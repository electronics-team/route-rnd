all:
	cd src/route-rnd && $(MAKE) all

clean:
	cd src/route-rnd && $(MAKE) clean
	-cd src_3rd/genht && $(MAKE) clean
	-cd src_3rd/genlist && $(MAKE) clean
	-cd src_3rd/genprique && $(MAKE) clean
	-cd src_3rd/genvector && $(MAKE) clean
	-cd src_3rd/libcdtr && $(MAKE) clean
	-cd src_3rd/libgrbs && $(MAKE) clean
	-cd src_3rd/libpsrand && $(MAKE) clean
	-cd src_3rd/libualloc && $(MAKE) clean
	-cd src_3rd/libusearch && $(MAKE) clean
	-cd src_3rd/libusteiner && $(MAKE) clean

distclean: clean
	cd src/route-rnd && $(MAKE) distclean
	-cd src_3rd/genht && $(MAKE) distclean
	-cd src_3rd/genlist && $(MAKE) distclean
	-cd src_3rd/genprique && $(MAKE) distclean
	-cd src_3rd/genvector && $(MAKE) distclean
	-cd src_3rd/libcdtr && $(MAKE) distclean
	-cd src_3rd/libgrbs && $(MAKE) distclean
	-cd src_3rd/libpsrand && $(MAKE) distclean
	-cd src_3rd/libualloc && $(MAKE) distclean
	-cd src_3rd/libusearch && $(MAKE) distclean
	-cd src_3rd/libusteiner && $(MAKE) distclean

install:
	cd src/route-rnd && $(MAKE) install
	cd doc && $(MAKE) install

linstall:
	cd src/route-rnd && $(MAKE) linstall
	cd doc && $(MAKE) linstall

uninstall:
	cd src/route-rnd && $(MAKE) uninstall
	cd doc && $(MAKE) uninstall
