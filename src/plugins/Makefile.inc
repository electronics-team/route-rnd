PLG=$(ROOT)/src/plugins/
THIRD=$(ROOT)/src_3rd

include $(PLG)/io_tedax/Makefile.inc
include $(PLG)/export_animator/Makefile.inc
include $(PLG)/export_svg/Makefile.inc
include $(PLG)/rt_horver/Makefile.inc
include $(PLG)/rt_topo/Makefile.inc
#include $(PLG)/rt_hace/Makefile.inc

BUILDIN_O = $(PLG)/buildin.o $(BUILDIN_IO_TEDAX) $(BUILDIN_EXPORT_ANIMATOR) $(BUILDIN_EXPORT_SVG) $(BUILDIN_RT_HORVER) $(BUILDIN_RT_TOPO) $(BUILDIN_RT_HACE)

$(PLG)/buildin.o: $(PLG)/buildin.c $(PLG)/buildin.h
