/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  animator export plugin - save the board (or a layer) in an animator(1) script
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "data.h"

#define ERROR "route-rnd animator error: "


typedef struct wr_ctx_s {
	rtrnd_t *ctx;
	FILE *f;
	char *fn;
	const char *clr;
} wr_ctx_t;

static void anim_wr_head(wr_ctx_t *wctx)
{
	fprintf(wctx->f, "macro board\n");
}

static void anim_wr_foot(wr_ctx_t *wctx)
{
	fprintf(wctx->f, "endmacro\n");
	fprintf(wctx->f, "viewport board\n");
	fprintf(wctx->f, "frame\n");
	fprintf(wctx->f, "invoke board\n");
	fprintf(wctx->f, "flush\n");
}

static void anim_color(wr_ctx_t *wctx, const char *clr)
{
	if ((wctx->clr != NULL) && ((wctx->clr == clr) || (strcmp(wctx->clr, clr) == 0)))
		return;
	fprintf(wctx->f, "color %s\n", clr);
	wctx->clr = clr;
}

static void anim_wr_via(wr_ctx_t *wctx, rtrnd_via_t *via)
{
	int verts = 10;
	if (via->dia > 2)
			verts = 20;

	anim_color(wctx, "#111111");
	fprintf(wctx->f, "fillcircle %f %f %f %d\n", via->x, -via->y, via->dia/2, verts);
}

static void anim_wr_layer(wr_ctx_t *wctx, rtrnd_layer_t *layer, int draw_vias)
{
	rtrnd_rtree_it_t it;
	rtrnd_any_obj_t *obj;
	rtp_vertex_t *v;
	int verts = 10;
	double r, vx, vy, nx, ny, l;

	fprintf(wctx->f, "! --- layer '%s' ---\n", layer->name);
	anim_color(wctx, layer->color);

	for(obj = rtrnd_rtree_all_first(&it, &layer->objs); obj != NULL; obj = rtrnd_rtree_all_next(&it)) {
		switch(obj->hdr.type) {
			case RTRND_LINE:
				if (obj->line.thickness > 2)
					verts = 20;
				r = obj->line.thickness/2;
				vx = obj->line.cline.p2.x - obj->line.cline.p1.x; vy = obj->line.cline.p2.y - obj->line.cline.p1.y;
				if ((vx != 0) || (vy != 0)) {
					l = sqrt(vx*vx + vy * vy);
					nx = -vy / l;
					ny = vx / l;
					fprintf(wctx->f, "poly %f %f %f %f  %f %f %f %f\n",
						obj->line.cline.p1.x + nx*r, -(obj->line.cline.p1.y + ny*r),
						obj->line.cline.p2.x + nx*r, -(obj->line.cline.p2.y + ny*r),
						obj->line.cline.p2.x - nx*r, -(obj->line.cline.p2.y - ny*r),
						obj->line.cline.p1.x - nx*r, -(obj->line.cline.p1.y - ny*r));
				}
				fprintf(wctx->f, "fillcircle %f %f %f %d\n", obj->line.cline.p1.x, -obj->line.cline.p1.y, r, verts);
				fprintf(wctx->f, "fillcircle %f %f %f %d\n", obj->line.cline.p2.x, -obj->line.cline.p2.y, r, verts);
				break;
			case RTRND_POLY:
				fprintf(wctx->f, "poly");
				for(v = gdl_first(&obj->poly.rtpoly.lst); v != NULL; v = gdl_next(&obj->poly.rtpoly.lst, v))
					fprintf(wctx->f, " %f %f", v->x, -v->y);
				fprintf(wctx->f, "\n");
				break;

			case RTRND_ARC:
			default: /* can't be on layer */
				break;
		}
	}

	if (draw_vias) {
		rtrnd_via_t *via;
		for(via = rtrnd_rtree_all_first(&it, &wctx->ctx->board->vias); via != NULL; via = rtrnd_rtree_all_next(&it))
			if (rtrnd_via_touches_layer(layer, via))
				anim_wr_via(wctx, via);
	}
}

static int anim_export(rtrnd_t *ctx, const char *basename, rtrnd_layer_t *layer, vtp0_t *annots)
{
	wr_ctx_t wctx;
	int res = 0, len = strlen(basename), n;

	wctx.fn = malloc(len + 32);
	memcpy(wctx.fn, basename, len);
	strcpy(wctx.fn + len, ".anim");
	wctx.clr = "";
	wctx.ctx = ctx;
	wctx.f = fopen(wctx.fn, "w");
	if (wctx.f == NULL) {
		fprintf(stderr, ERROR "can't open '%s' for write\n", wctx.fn);
		goto err;
	}

	anim_wr_head(&wctx);
	if (layer == NULL) {
		if ((ctx->board != NULL) && (ctx->board->layers.used > 0)) {
			rtrnd_via_t *via;
			rtrnd_rtree_it_t it;

			for(n = ctx->board->layers.used-1; n >=0 ; n--)
				anim_wr_layer(&wctx, ctx->board->layers.array[n], 0);
			for(via = rtrnd_rtree_all_first(&it, &ctx->board->vias); via != NULL; via = rtrnd_rtree_all_next(&it))
				anim_wr_via(&wctx, via);
		}
		else
			fprintf(wctx.f, "! --- (empty board - no layers!) ---\n");
	}
	else
		anim_wr_layer(&wctx, layer, 1);

	if (annots != NULL)
		for(n = 0; n < annots->used; n++)
			anim_wr_layer(&wctx, annots->array[n], 0);

	anim_wr_foot(&wctx);

	err:;
	free(wctx.fn);
	if (wctx.f != NULL)
		fclose(wctx.f);
	return res;
}


static const rtrnd_export_t exp_anim = {
	"animator",
	anim_export
};

void export_animator_init(void)
{
	vtp0_append(&rtrnd_all_export, (void *)&exp_anim);
}
