/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020,2021 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include <genvector/vtd0.h>
#include <libgrbs/route.h>

#include "geo.h"
#include "gengeo2d/cline.h"

#warning TODO: use libualloc for ctn
static int add_c_edge_cnt;
#define ADD_C_EDGE(p1, p2, immut) \
do { \
	e = cdt_insert_constrained_edge(cdt, p1, p2); \
	e->data = obj; \
	if (add_grbs) { \
		crbs_point_t *cp1 = p1->data, *cp2 = p2->data; \
		grbs_line_t *gl; \
		grbs_2net_t *tn = grbs_2net_new(&crbs->grbs, cop, clr); \
		crbs_2net_t *ctn = calloc(sizeof(crbs_2net_t), 1); \
		tn->user_data = ctn; \
		ctn->tn = NULL; \
		ctn->net = obj->hdr.net; \
		ctn->old = 1; \
		gl = grbs_line_realize(&crbs->grbs, tn, cp1->gpt, cp2->gpt); \
		gl->user_data = ctn; \
		gl->immutable = immut; \
		if (0) \
			printf("GT 2net_new _c_edge_%d %f %f from P%ld to P%ld\n", add_c_edge_cnt++, cop, clr, cp1->gpt->uid, cp2->gpt->uid); \
	} \
} while(0) \


static void long_constrained_edge(crbs_t *crbs, point_t *p1, point_t *p2, double maxlen2, double maxlen, double cop, double clr, int add_grbs, rtrnd_any_obj_t *obj, rtrnd_net_t *net)
{
	edge_t *e;
	double dx = p2->pos.x - p1->pos.x, dy = p2->pos.y - p1->pos.y;
	double len2 = dx*dx + dy*dy;
	cdt_t *cdt = &crbs->cdt;
	int immut = 1; /* because constrained in the cdt, we do not ever want these to move in grbs */

	if (len2 > maxlen2) {
		double len, x, y;
		long spn, n;
		point_t *lp, *p;

		len = sqrt(len2);
		spn = ceil(len/maxlen);
		dx /= ((double)spn+1);
		dy /= ((double)spn+1);
		x = p1->pos.x;
		y = p1->pos.y;
		lp = p1;

		/* start and internal points */
		for(n = 0; n < spn; n++) {
			x += dx;
			y += dy;
			p = crbs_make_point(crbs, x, y, cop, clr, NULL, net);
			if (p == NULL)
				continue;
			ADD_C_EDGE(lp, p, immut);
			lp = p;
		}

		/* end */
		ADD_C_EDGE(lp, p2, immut);
	}
	else
		ADD_C_EDGE(p1, p2, immut);
}

void rt_topo_crbs_cdt_init(rtrnd_t *ctx, crbs_t *crbs)
{
	cdt_t *cdt = &crbs->cdt;
	cdt_init(cdt, ctx->board->hdr.bbox.x1, ctx->board->hdr.bbox.y1, ctx->board->hdr.bbox.x2, ctx->board->hdr.bbox.y2);
}

void rt_topo_crbs_cdt_create_points(rtrnd_t *ctx, rtrnd_layer_t *ly, crbs_t *crbs)
{
	rtrnd_any_obj_t *obj;
	rtrnd_via_t *via;
	rtrnd_rtree_it_t it;
	point_t *p1, *p2;
	cdt_t *cdt = &crbs->cdt;
	rtp_vertex_t *v;
	long n;
	double maxl2, maxlen = (rt_topo_cfg.wire_thick + rt_topo_cfg.wire_clr/2) * 50; /* split long constrained edges so that no segment is longer than this value */
	double cop, clr;

	maxl2 = maxlen*maxlen;

	for(via = rtrnd_rtree_all_first(&it, &ctx->board->vias); via != NULL; via = rtrnd_rtree_all_next(&it))
		crbs_make_point(crbs, via->x, via->y, via->dia/2, via->clearance, via, via->hdr.net);

	for(obj = rtrnd_rtree_all_first(&it, &ly->objs); obj != NULL; obj = rtrnd_rtree_all_next(&it)) {
		switch(obj->hdr.type) {
			case RTRND_LINE:
				cop = obj->line.thickness/2;
				clr = obj->line.clearance;
				if (clr < 0) clr = 0;
				p1 = crbs_make_point(crbs, obj->line.cline.p1.x, obj->line.cline.p1.y, cop, clr, NULL, obj->hdr.net);
				if ((obj->line.cline.p1.x != obj->line.cline.p2.x) || (obj->line.cline.p1.y != obj->line.cline.p2.y)) {
					p2 = crbs_make_point(crbs, obj->line.cline.p2.x, obj->line.cline.p2.y, cop, clr, NULL, obj->hdr.net);
					long_constrained_edge(crbs, p1, p2, maxl2, maxlen, cop, clr, 1, obj, obj->hdr.net);
				}
				break;
			case RTRND_POLY:
				clr = 0/*obj->poly.clearance*/;
				v = gdl_last(&obj->poly.rtpoly.lst);
				p2 = crbs_make_point(crbs, v->x, v->y, 0, clr, NULL, obj->hdr.net);
				for(v = gdl_first(&obj->poly.rtpoly.lst); v != NULL; v = gdl_next(&obj->poly.rtpoly.lst, v)) {
					p1 = crbs_make_point(crbs, v->x, v->y, 0, clr, NULL, obj->hdr.net);
					long_constrained_edge(crbs, p1, p2, maxl2, maxlen, 0, clr, 1, obj, obj->hdr.net);
					p2 = p1;
				}
				break;
			default:
#warning handle all other types
				;
		}
	}
}

void rt_topo_crbs_cdt_create_edges(rtrnd_t *ctx, rtrnd_layer_t *ly, crbs_t *crbs)
{
	cdt_t *cdt = &crbs->cdt;
	long n;

	for(n = 0; n < cdt->edges.used; n++) {
		edge_t *e = cdt->edges.array[n];
#warning TODO: use libualloc here
		crbs_edge_t *ce = malloc(sizeof(crbs_edge_t));
		e->data = ce;
		memset(ce, 0, sizeof(crbs_edge_t));
		ce->ang[0] = atan2(e->endp[1]->pos.y - e->endp[0]->pos.y, e->endp[1]->pos.x - e->endp[0]->pos.x);
		ce->ang[1] = atan2(e->endp[0]->pos.y - e->endp[1]->pos.y, e->endp[0]->pos.x - e->endp[1]->pos.x);
		ce->edge = e;
	}
}

void rt_topo_crbs_cdt_draw(rtrnd_t *ctx, rtrnd_layer_t *ly_out, cdt_t *cdt)
{
	VTEDGE_FOREACH(edge, &cdt->edges)
		rtrnd_line_new(ly_out, NULL, NULL,
			edge->endp[0]->pos.x, edge->endp[0]->pos.y, edge->endp[1]->pos.x, edge->endp[1]->pos.y, \
			edge->is_constrained ? 0.1 : 0.01, 0);
	VTEDGE_FOREACH_END();

/* labels:*/
	VTPOINT_FOREACH(pt, &cdt->points)
		if (pt->data != NULL) {
			char tmp[64];
			sprintf(tmp, "P%ld", ((crbs_point_t *)pt->data)->gpt->uid);
			rtrnd_text_new(ly_out, pt->pos.x, pt->pos.y, tmp, 0.5);
		}
	VTPOINT_FOREACH_END();

	VTEDGE_FOREACH(edge, &cdt->edges) {
		if ((edge->data != NULL) && (((crbs_edge_t *)edge->data)->nets > 0)) {
			char tmp[64];
			double x = (edge->endp[0]->pos.x + edge->endp[1]->pos.x)/2, y = (edge->endp[0]->pos.y + edge->endp[1]->pos.y)/2;

			sprintf(tmp, "(%ld)", ((crbs_edge_t *)edge->data)->nets);
			rtrnd_text_new(ly_out, x, y, tmp, 0.5);
		}
	}
	VTEDGE_FOREACH_END();
}

static edge_t *crbs_cdt_get_edge(point_t *pt1, point_t *pt2)
{
	EDGELIST_FOREACH(e, (pt1->adj_edges)) {
		if ((e->endp[0] == pt2) || (e->endp[1] == pt2))
			return e;
	}
	EDGELIST_FOREACH_END();

	return NULL;
}

void rt_topo_crbs_cdt_inc_edge(crbs_t *crbs, grbs_addr_t *a1, grbs_addr_t *a2)
{
#if 0
	crbs_point_t *pt1, *pt2;
	edge_t *cdt_e;
	crbs_edge_t *edge;

	if ((a1->type & 0x0F) == ADDR_POINT)
		pt1 = a1->obj.pt->user_data;
	else
		pt1 = a1->obj.arc->parent_pt->user_data;

	if ((a2->type & 0x0F) == ADDR_POINT)
		pt2 = a2->obj.pt->user_data;
	else
		pt2 = a2->obj.arc->parent_pt->user_data;

	cdt_e = crbs_cdt_get_edge(pt1->cpt, pt2->cpt);

	assert(cdt_e != NULL);
	edge = cdt_e->data;
	edge->nets++;
	assert(edge->nets > 0); /* unlikely: overflow */
#endif
}

void rt_topo_crbs_cdt_dec_edge(crbs_t *crbs, grbs_arc_t *arc1, grbs_arc_t *arc2)
{
#if 0
	crbs_point_t *pt1 = arc1->parent_pt->user_data, *pt2 = arc2->parent_pt->user_data;
	edge_t *cdt_e = crbs_cdt_get_edge(pt1->cpt, pt2->cpt);
	crbs_edge_t *edge;

	assert(cdt_e != NULL);
	edge = cdt_e->data;
	assert(edge->nets > 0);
	edge->nets--;
#endif
}


void rt_topo_crbs_cdt_unref_tn(crbs_t *crbs, grbs_2net_t *tn)
{
	grbs_arc_t *prev = NULL, *a;
	for(a = gdl_first(&tn->arcs); a != NULL; prev = a, a = gdl_next(&tn->arcs, a)) {
		if (prev != NULL)
			rt_topo_crbs_cdt_dec_edge(crbs, prev, a);
	}
}
