/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include "data.h"

#include "laa.h"
#include "trbs.h"
#include "crbs.h"
#include "rt_topo.h"

rt_topo_cfg_t rt_topo_cfg;

static rtrnd_conf_t topo_cfg_desc_crbs[] = {
	RTRND_CONF_COORD("wire_thick",        0.20, 0.01, 10, "signal wire thickness", &rt_topo_cfg.wire_thick)
	RTRND_CONF_COORD("wire_clr",          0.20, 0.01, 10, "clearance around signal wire", &rt_topo_cfg.wire_clr)
	RTRND_CONF_COORD("via_dia",           1.2,  0.01, 10, "via copper ring outer diameter", &rt_topo_cfg.via_dia)
	RTRND_CONF_COORD("via_clr",           0.20, 0.01, 10, "clearance around via copper", &rt_topo_cfg.via_clr)
	RTRND_CONF_DOUBLE("beta",             1,   0, 100,    "via vs. wire length preference; high value = short wires, low value = less vias", &rt_topo_cfg.beta)
	RTRND_CONF_DOUBLE("max_hop_mult",     1,  0.1, 10000, "when to give up the search for a route on a layer; higher value means give up later", &rt_topo_cfg.max_hop_mult)
	RTRND_CONF_DOUBLE("junction_penalty", 1,  0.25, 1000, "higher value results in less junctions (Stenier points)", &rt_topo_cfg.junction_penalty)
	RTRND_CONF_DOUBLE("via_via_space",    1,  0.1,  10,   "higher value makes newly placed vias keep more space between each other so more wires can pass", &rt_topo_cfg.via_via_space)


/*	RTRND_CONF_BOOLEAN("octilinear",      0,              "draw 90 and 45 degree lines", &rt_topo_cfg.octilin)*/
	RTRND_CONF_TERMINATE
};

static rtrnd_conf_t topo_cfg_desc_trbs[] = {
	RTRND_CONF_COORD("wire_thick",        0.20, 0.01, 10, "signal wire thickness", &rt_topo_cfg.wire_thick)
	RTRND_CONF_COORD("wire_clr",          0.20, 0.01, 10, "clearance around signal wire", &rt_topo_cfg.wire_clr)
	RTRND_CONF_COORD("via_dia",           1.2,  0.01, 10, "via copper ring outer diameter", &rt_topo_cfg.via_dia)
	RTRND_CONF_COORD("via_clr",           0.20, 0.01, 10, "clearance around via copper", &rt_topo_cfg.via_clr)
	RTRND_CONF_DOUBLE("beta",             50,   0, 100,   "via vs. wire length preference; high value = short wires, low value = less vias", &rt_topo_cfg.beta)
	RTRND_CONF_DOUBLE("junction_penalty", 1,  0.25, 1000, "higher value results in less junctions (Stenier points)", &rt_topo_cfg.junction_penalty)
	RTRND_CONF_DOUBLE("via_via_space",    1,  0.1,  10,   "higher value makes newly placed vias keep more space between each other so more wires can pass", &rt_topo_cfg.via_via_space)

/*	RTRND_CONF_BOOLEAN("octilinear",      0,              "draw 90 and 45 degree lines", &rt_topo_cfg.octilin)*/
	RTRND_CONF_TERMINATE
};

static int route_topo(rtrnd_t *ctx, int (*rbs)(rtrnd_t *, rt_topo_laa2rbs_t *))
{
	double dx = ctx->board->hdr.bbox.x2 - ctx->board->hdr.bbox.x1, dy = ctx->board->hdr.bbox.y2 - ctx->board->hdr.bbox.y1;
	double diag = sqrt(dx*dx+dy*dy), alpha1;
	int res = 0, n;
	rt_topo_laa2rbs_t laa2rbs = {0};

	alpha1 = 0.12 * diag * rt_topo_cfg.beta / 100.0;
	rt_topo_cfg.alpha = alpha1/(1+alpha1);
	rt_topo_cfg.alpha2 = 1.0 - rt_topo_cfg.alpha;
	if (rt_topo_laa(ctx, &laa2rbs) != 0) {
		printf("Failed to solve the layer assignment\n");
		return -1;
	}

	res |= rbs(ctx, &laa2rbs);

	for(n = 0; n < ctx->board->layers.used; n++) {
		gdl_list_t *tnl = laa2rbs.ly2nets.array[n];
		rt_topo_2net_t *tn;
		while((tn = gdl_first(tnl)) != NULL) {
			gdl_remove(tnl, tn, link);
			free(tn);
		}
		free(tnl);
	}

	return res;
}

static int route_topo_trbs(rtrnd_t *ctx)
{
	return route_topo(ctx, rt_topo_trbs);
}

static int route_topo_crbs(rtrnd_t *ctx)
{
	return route_topo(ctx, rt_topo_crbs);
}


static const rtrnd_router_t rtr_topo_trbs = {
	"topo_trbs", "topological: LAA + triangulated rubber band sketch",
	topo_cfg_desc_trbs,
	route_topo_trbs
};

static const rtrnd_router_t rtr_topo_crbs = {
	"topo_crbs", "topological: LAA + geometric rubber band sketch",
	topo_cfg_desc_crbs,
	route_topo_crbs
};

void rt_topo_init(void)
{
	vtp0_append(&rtrnd_all_router, (void *)&rtr_topo_trbs);
	vtp0_append(&rtrnd_all_router, (void *)&rtr_topo_crbs);
}
