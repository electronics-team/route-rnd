/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* LAA step III: convert the solution to a per layer 2net format needed
   by the RBS code */

typedef struct { /* temp storage for ordering 2nets before creating them for the rbs */
	rt_topo_laa_2net_t *tn;
	int bi1, bi2; /* [bi1..bi2) is the section of the laa 2net that is becoming the rbs 2net */
	vtp0_t before;
	int is_before;
	gdl_elem_t link;
	unsigned inserted:1;
} laa2rbs_t;

static void print_coord(rtrnd_2branch_t *br)
{
	if (br->via != NULL)
		printf(" [%f %f]", br->via->x, br->via->y);
	else
		printf(" (%f %f)", br->x, br->y);
}

static void laa3_render_order(laa2rbs_t *l2r)
{
	rt_topo_laa_2net_t *tn = l2r->tn;
	int bi;

	for(bi = l2r->bi1; bi < l2r->bi2; bi++) {
		rtrnd_2branch_t *br = &tn->br.array[bi], *br2;
		rtrnd_crossing_t *cr;

		if (br->cridx < 0) continue;
		cr = &tn->cross.array[br->cridx];
		if ((cr->tn_detouring == tn) && (cr->cn_bridx >= 0) && (cr->cn != NULL)) {
			laa2rbs_t *o = br->ordinfo, *o2;
			br2 = &cr->cn->br.array[cr->cn_bridx];
			o2 = br2->ordinfo;
			printf(" ord: %p after %p!\n", o, o2);
			if (o2 != NULL) {
				vtp0_append(&o2->before, o);
				o->is_before++;
			}
		}
	}
}

static laa2rbs_t *l2r_alloc(uall_stacks_t *ordstk, rt_topo_laa_2net_t *tn, int bi1, int bi2)
{
	laa2rbs_t *l2r = uall_stacks_alloc(ordstk);
	int n;

	memset(l2r, 0, sizeof(laa2rbs_t));
	l2r->tn = tn;
	l2r->bi1 = bi1;
	l2r->bi2 = bi2;

	for(n = bi1; n < bi2; n++)
		tn->br.array[n].ordinfo = l2r;

	return l2r;
}

void laa3_render(laa3_t *laa3, rt_topo_laa2rbs_t *dst)
{
	rtrnd_t *ctx = laa3->ctx;
	htsp_entry_t *e;
	rt_topo_laa_2net_t *tn;
	laa2rbs_t *l2r, *ch;
	int bi, tni;
	long n, added, uid_2net = 0;
	vtp0_t ord = {0};
	uall_stacks_t ordstk = {0};
	uall_sysalloc_t sys = {0};
	gdl_list_t S = {0};

	sys.alloc     = uall_stdlib_alloc;
	sys.free      = uall_stdlib_free;
	sys.page_size = 4096;
	ordstk.sys = &sys;
	ordstk.elem_size = sizeof(laa2rbs_t);

	/* create a list of 2nets for each layer */
	for(n = 0; n < ctx->board->layers.used; n++)
		vtp0_append(&dst->ly2nets, calloc(sizeof(gdl_list_t), 1));

printf("** RENDER **\n");
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rt_topo_2nets_t *tns = NETDATA_LAA(net);
		for(tn = tns->head, tni=0; tn != NULL; tn = tn->next, tni++) {
			int currly = tn->asg[0], start = 0;
			for(bi = 1; bi < tn->br.used; bi++) {
				if (tn->asg[bi] != currly) {
printf(" net: %s:%d %d..%d bily: %d currly: %d", net->hdr.oid, tni, start, bi, tn->asg[bi], currly);
print_coord(&tn->br.array[start]);
print_coord(&tn->br.array[bi]);
					l2r = l2r_alloc(&ordstk, tn, start, bi);
					vtp0_append(&ord, l2r);
printf(" -> %p\n", l2r);
					currly = tn->asg[bi];
					start = bi;
				}
			}
			if (start != bi-1) {
printf(" net: %s:%d %d..%d", net->hdr.oid, tni, start, bi-1);
print_coord(&tn->br.array[start]);
print_coord(&tn->br.array[bi-1]);
				l2r = l2r_alloc(&ordstk, tn, start, bi-1);
				vtp0_append(&ord, l2r);
printf(" -> %p\n", l2r);
			}
		}
	}

	/* build the ordering graph */
	for(n = 0; n < ord.used; n++)
		laa3_render_order(ord.array[n]);


printf("Final order on %ld nets:\n", ord.used);

	/* Kahn's algorithm for topological sorting */
	for(n = 0; n < ord.used; n++) {
		l2r = ord.array[n];
		if (l2r->is_before == 0)
			gdl_insert(&S, l2r, link);
	}

	add_more:;
	while((l2r = gdl_first(&S)) != NULL) {
		rtrnd_2branch_t *br1 = &l2r->tn->br.array[l2r->bi1], *br2 = &l2r->tn->br.array[l2r->bi2];
		long ly = l2r->tn->asg[l2r->bi1];
		gdl_list_t *lst = dst->ly2nets.array[ly];
		rt_topo_2net_t *res2net;

		gdl_remove(&S, l2r, link);

printf(" %p ly=%ld: %s %f;%f %f;%f\n", l2r, ly, br1->parent->net->hdr.oid, br1->x, br1->y, br2->x, br2->y);
		if (!l2r->inserted) {
			res2net = calloc(sizeof(rt_topo_2net_t), 1);
			res2net->x[0] = br1->x; res2net->y[0] = br1->y;
			res2net->x[1] = br2->x; res2net->y[1] = br2->y;
			res2net->net = br1->parent->net;
			res2net->uid = uid_2net++;
			gdl_append(lst, res2net, link);
			l2r->inserted = 1;
printf("Insert: %p\n", l2r);
		}

		for(n = l2r->before.used - 1; n >= 0; n--) {
			ch = l2r->before.array[n];
			assert(ch->is_before > 0);
			ch->is_before--;
			if (ch->is_before == 0) {
				if (ch->link.parent == NULL)
					gdl_insert(&S, ch, link);
			}
		}
		l2r->before.used = 0;
	}


	added = 0;
	for(n = 0; n < ord.used; n++) { /* append remainings at the end */
		l2r = ord.array[n];
		if (!l2r->inserted) {
			gdl_append(&S, l2r, link);
printf("Append: %p\n", l2r);
			added++;
		}
	}
	if (added)
		goto add_more;


	/* free temporary data */
	for(n = 0; n < ord.used; n++) {
		l2r = ord.array[n];
		vtp0_uninit(&l2r->before);
	}
	vtp0_uninit(&ord);
	uall_stacks_clean(&ordstk);

	printf(" Resulting ordered nets: (%ld); per layer:\n", gdl_length(&S));
	for(n = 0; n < 2; n++) {
		gdl_list_t *lst = dst->ly2nets.array[n];
		printf("  [%ld] %ld\n", n, lst == NULL ? 0 : gdl_length(lst));
	}
}

