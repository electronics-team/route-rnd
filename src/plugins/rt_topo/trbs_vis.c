/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

static int trbs_vis_trace = 1;

#if 0
#	define tprintf printf
#else
	static void tprintf(const char *fmt, ...) {}
#endif

static edge_t *trbs_pt_opposite_edge(trbs_t *trbs, triangle_t *t, point_t *src)
{
	int n;

	/* find an edge that doesn't have src as endpoint - in a triangle, that
	   must be the opposite */
	for(n = 0; n < 3; n++)
		if ((t->e[n]->endp[0] != src) && (t->e[n]->endp[1] != src))
			return t->e[n];

	fprintf(stderr, "trbs_pt_opposite_edge(): invalid triangle!\n");
	abort();
}

/* returns direction (+1=start->end or -1=end->start) that corresponds to
   CCW walk in a triangle on a specific edge */
static int trbs_edge_dir_ccw(trbs_t *trbs, triangle_t *t, edge_t *e)
{
	int n, si = -1, ei = -1;
	for(n = 0; n < 3; n++) {
		if (t->p[n] == e->endp[0]) si = n;
		if (t->p[n] == e->endp[1]) ei = n;
	}

	/* broken triangle detection */
	assert(si != -1);
	assert(ei != -1);

	if (ei > si) {
		if ((ei-si) > 1)
			return +1; /* ei=3, si=1: overflow */
		return -1;
	}

	/* ei < si */
	if ((si - ei) > 1)
		return -1; /* si=3, ei=1: overflow */
	return +1;
}

/* Convert a line's dx/dy into a fake angle value between 0 and 8; the fake
   angle is suitable for determining the order of lines by angle (but not
   the exact angles) */
static double line_fake_angle(double dx, double dy)
{
	int sx, sy;

	/* special cases for axis aligned lines (quicker) */
	if (dx == 0) {
		if (dy == 0)
			return 0;
		return dy > 0 ? 2 : 6;
	}
	else if (dy == 0)
		return dx > 0 ? 0 : 4;


	/* arbitrary angle cases, handled by sign of dx and dy */
	sx = (dx >= 0);
	sy = (dy >= 0);

	if (sx && sy) { /* base is 0 */
		if (dx > dy)
			return 0 + dy / dx;
		return (0 + 2 - (dx/dy));
	}
	else if (!sx && sy) { /* base is 2 */
		dx = -dx;
		if (dx > dy)
			return 2 + 2 - (dy/dx);
		return (2 + (dx/dy));
	}
	else if (!sx && !sy) { /* base is 4 */
		if (dx < dy)
			return 4 + (dy/dx);
		return (4 + 2 - (dx/dy));
	}
	else /*if (sx && !sy)*/ { /* base is 6 */
		dy = -dy;
		if (dx > dy)
			return 6 + 2 - (dy/dx);
		return (6 + (dx/dy));
	}
}

/* Return the next (dir>0) or previous (dir<0) crosing from cr */
#define CR_STEP(cr, dir)   (((dir) > 0) ? (cr)->link_edge.next : (cr)->link_edge.prev)

static void trbs_print_triangle(triangle_t *t)
{
	int n;

	for(n = 0; n < 3; n++)
		tprintf(" P%ld", ((trbs_point_t *)t->p[n]->data)->uid);
}

/* Returns the insertion point next to the sentinel cr on the edge of t
   that differs from src_edge but ends in the same endpoint pt */
static trbs_cross_t *trbs_vis_get_pt_insertion(trbs_t *trbs, triangle_t *t, point_t *pt, edge_t *src_edge, int srcdir)
{
	edge_t *edge = NULL;
	trbs_edge_t *tedge;
	int n;
	point_t *shared;

	if (trbs_vis_trace) {
		tprintf("  triangle: ");
		trbs_print_triangle(t);
		tprintf("\n");
	}

	/* point is not in triangle means the path is leaving the triangle
	   through this crossing to reach another point */
	if ((t->p[0] != pt) && (t->p[1] != pt) && (t->p[2] != pt)) {
		if (trbs_vis_trace) tprintf("  null1\n");
		return NULL;
	}

	/* the path line cuts the triangle in two halves; to determine in which
	   half to search (which is our target edge), we need to find the point
	   that is shared between the source edge and the target edge */
	if (srcdir > 0)
		shared = src_edge->endp[0];
	else
		shared = src_edge->endp[1];

	/* find target edge */
	for(n = 0; n < 3; n++) {
		if (t->e[n] == src_edge)
			continue;
		if ((t->e[n]->endp[0] == shared) || (t->e[n]->endp[1] == shared)) { /* in our half-triangle */
			if ((t->e[n]->endp[0] == pt) || (t->e[n]->endp[1] == pt)) { /* edge ends in path end */
				edge = t->e[n];
				break;
			}
		}
	}

	if (edge == NULL) {
		fprintf(stderr, "trbs_vis_get_pt_insertion(): broken triangle\n");
		return NULL;
	}

	tedge = edge->data;

	/* pt is starting point of edge: insertion point is after the start sentinel */
	if (edge->endp[0] == pt) {
		if (trbs_vis_trace) tprintf("  first\n");
		return gdl_first(&tedge->crosses);
	}

	/* else pt is the ending point of edge: insertion point is the point before
	   the end sentinel */
	if (trbs_vis_trace) tprintf("  last\n");
	return gdl_prev(&tedge->crosses, gdl_last(&tedge->crosses));
}

/* Returns 1 if cr is within t, 0 otherwise */
static int trbs_vis_cr_in_trianlge(trbs_t *trbs, triangle_t *t, trbs_cross_t *cr)
{
	return (cr->edge->adj_t[0] == t) || (cr->edge->adj_t[1] == t);
}

/* if there is a path line from point pt within triangle t, jump along that */
static trbs_cross_t *trbs_vis_jump_line_from_pt(trbs_t *trbs, triangle_t *t, point_t *pt, trbs_cross_t *crside)
{
	edge_t *side, *ope = trbs_pt_opposite_edge(trbs, t, pt);
	trbs_edge_t *tope = ope->data;
	trbs_cross_t *cr;

	if (trbs_vis_trace) {
		trbs_cross_t *cr1 = gdl_first(&tope->crosses), *cr2 = gdl_last(&tope->crosses);
		tprintf("trbs_vis_jump_line_from_pt crside=%ld ope: %ld..%ld pt=P%ld\n", crside->uid, cr1->uid, cr2->uid, ((trbs_point_t *)pt->data)->uid);
	}

	/* check the shared point between the opposite (target) side and the
	   side we are coming from; iterate over crossing and return the first whose
	   path ends in pt */
	side = crside->edge;
	if ((ope->endp[0] == side->endp[0]) || (ope->endp[0] == side->endp[1])) {
		if (trbs_vis_trace) tprintf(" endp[0]\n");
		/* shared point is ope->endp[0], search from the sentinel forward */
		for(cr = gdl_first(&tope->crosses); cr != NULL; cr = gdl_next(&tope->crosses, cr)) {
			if (trbs_vis_trace) tprintf("  cr=%ld\n", cr->uid);
			if (cr->twonet == NULL)
				continue;
			if (trbs_vis_trace) tprintf("   %p %p == %p\n", cr->twonet->start, cr->twonet->end, pt);
			if ((cr->twonet->start == pt) || (cr->twonet->end == pt)) {
				trbs->collision = cr->twonet;
				return gdl_prev(&tope->crosses, cr);
			}
		}
	}
	else if ((ope->endp[1] == side->endp[0]) || (ope->endp[1] == side->endp[1])) {
		if (trbs_vis_trace) tprintf(" endp[1]\n");
		/* shared point is ope->endp[1], search from the sentinel backward */
		for(cr = gdl_last(&tope->crosses); cr != NULL; cr = gdl_prev(&tope->crosses, cr)) {
			if (trbs_vis_trace) tprintf("  cr=%ld\n", cr->uid);
			if (cr->twonet == NULL)
				continue;
			if (trbs_vis_trace) tprintf("   %p %p == %p\n", cr->twonet->start, cr->twonet->end, pt);
			if ((cr->twonet->start == pt) || (cr->twonet->end == pt)) {
				trbs->collision = cr->twonet;
				return cr;
			}
		}
	}
	else
		fprintf(stderr, "trbs_vis_jump_line_from_pt(): broken triangle\n");

	if (trbs_vis_trace) tprintf("  (line_from_pt not found)\n");
	return NULL;
}

/* jump from cr1 to cr2 over a path line within a triangle; whether we land
   before or after cr2 depends on the shared endpoint of their edges and from
   which direction cr1 is reached from */
static trbs_cross_t *trbs_vis_jump_path_line(trbs_t *trbs, triangle_t *t, trbs_cross_t *cr1, trbs_cross_t *cr2, int cr1dir)
{
	point_t *shared;
	int cr1o, cr2o, cr2dir;
	trbs_edge_t *tedge2 = cr2->edge->data;

	if ((cr1->edge->endp[0] == cr2->edge->endp[0]) || (cr1->edge->endp[0] == cr2->edge->endp[1])) {
		shared = cr1->edge->endp[0];
		cr1o = -1;
	}
	else if ((cr1->edge->endp[1] == cr2->edge->endp[0]) || (cr1->edge->endp[1] == cr2->edge->endp[1])) {
		shared = cr1->edge->endp[1];
		cr1o = +1;
	}
	else {
		fprintf(stderr, "trbs_vis_jump_path_line: broken triangle\n");
		return NULL;
	}

	cr2o = (shared == cr2->edge->endp[0]) ? -1 : +1;

	/* compare edge orientations toward the shared point: */
	if (cr1o == cr2o) {
		/* if they are the same, both pointing toward the shared point,
		   invert the direction to step in the same dir */
		cr2dir = -cr1dir;
	}
	else {
		cr2dir = cr1dir;
	}

	if (cr2dir > 0)
		return cr2; /* after cr2 */

	return gdl_prev(&tedge2->crosses, cr2); /* before cr2 */
}

static int is_edge_blocked(trbs_edge_t *e, rtrnd_net_t *net)
{
	if (!e->blocked)
		return 0;

	/* blocked, but by the same net - we may cross; this is how we can escape
	   from a via-in-poly starting point through the constrained edge of the poly */
	if ((e->obj != NULL) && (net != NULL) && (e->obj->hdr.net == net))
		return 0;

	return 1;
}

/* Jump to the next edge within a triangle in CCW: start tracing from after
   src_aft and return the crossing which the tracing lands after on the next
   edge */
static trbs_cross_t *trbs_vis_jump_edge(trbs_t *trbs, triangle_t *t, trbs_cross_t *src_aft)
{
	edge_t *srce = src_aft->edge, *dste = NULL; /* src is where we started from, dst is the next edge, CCW */
	trbs_cross_t *cr, *cr2;
	int dsti, srcdir, dstdir;
	point_t *corner;


	srcdir = trbs_edge_dir_ccw(trbs, t, srce);
	if (srcdir > 0)
		corner = srce->endp[1];
	else
		corner = srce->endp[0];

	/* determine destination edge */
	for(dsti = 0; dsti < 3; dsti++) {
		if (t->e[dsti] == srce) continue;
		if ((t->e[dsti]->endp[0] == corner) || (t->e[dsti]->endp[1] == corner)) {
			dste = t->e[dsti];
			break;
		}
	}
	assert(dste != NULL);
	dstdir = trbs_edge_dir_ccw(trbs, t, dste);

	if (trbs_vis_trace) {
		trbs_edge_t *tsrce = srce->data, *tdste = dste->data;
		trbs_cross_t *s0 = gdl_first(&tsrce->crosses), *s1 = gdl_last(&tsrce->crosses);
		trbs_cross_t *d0 = gdl_first(&tdste->crosses), *d1 = gdl_last(&tdste->crosses);
		tprintf("jump_edge from %ld in trinagle", src_aft->uid);
		trbs_print_triangle(t);
		tprintf(" srcedge=P%ld..P%ld (%ld..%ld) srcdir=%d   dstdege=P%ld..P%ld (%ld..%ld) dstdir=%d\n",
			((trbs_point_t *)srce->endp[0]->data)->uid, ((trbs_point_t *)srce->endp[1]->data)->uid, s0->uid, s1->uid, srcdir,
			((trbs_point_t *)dste->endp[0]->data)->uid, ((trbs_point_t *)dste->endp[1]->data)->uid, d0->uid, d1->uid, dstdir);
	}


	if ((srcdir < 0) && (src_aft->twonet != NULL)) { /* we are after a path crossing and we are required to step back */
		/* we'd be stepping across a path crossing - instead, jump along the line */
		cr = src_aft;
	}
	else {
		/* step one on the source edge */
		cr = CR_STEP(src_aft, srcdir);

		if (cr == NULL) /* we are at the sentinel */
			cr = src_aft;
	}

	if (trbs_vis_trace) tprintf(" step: %ld->%ld sentinel=%d srcdir=%d\n", src_aft->uid, cr->uid, (cr->twonet == NULL), srcdir);


	/* if we reached the sentinel, that means we reached the shared point;
	   walk around this "corner" of the triangle: return the region "after"
	   the sentinel on dste */
	if (cr->twonet == NULL) {
		trbs_edge_t *e = dste->data;
		assert(e != NULL);

		/* this point may be our target point */
		if (corner == trbs->target) {
			if (trbs_vis_trace) tprintf(" -> target corner! (P%ld)\n", ((trbs_point_t *)corner->data)->uid);
			return (trbs_cross_t *)trbs->target; /* caller checks for this ptr explicitly */
		}

		if (dstdir > 0) {
			/* landed after the first point on src edge: */

			/* check if a path line ends in this point, through our triangle - if so
			   make a jump through that line */
tprintf("endpA cr=%ld [P%ld .. P%ld corner=P%ld]: ", cr->uid,
	((trbs_point_t *)cr->edge->endp[0]->data)->uid,
	((trbs_point_t *)cr->edge->endp[1]->data)->uid,
	((trbs_point_t *)corner->data)->uid);
			cr = trbs_vis_jump_line_from_pt(trbs, t, corner, cr);
			if (cr != NULL) {
				if (trbs_vis_trace) tprintf(" -> %ld\n", cr->uid);
				return cr;
			}

			/* or return the first sentinel cr (after which our landing region is) */
			cr = gdl_first(&e->crosses);
			assert(cr != NULL);
			if (trbs_vis_trace) tprintf(" -> %ld (first)\n", cr->uid);
			return cr;
		}

		/* landed before the last point on dste: */

		/* check if a path line ends in this point, through our triangle - if so
		   make a jump through that line */
tprintf("endpB cr=%ld [P%ld .. P%ld corner=P%ld]: ",
	cr->uid,
	((trbs_point_t *)cr->edge->endp[0]->data)->uid,
	((trbs_point_t *)cr->edge->endp[1]->data)->uid,
	((trbs_point_t *)corner->data)->uid);
		cr = trbs_vis_jump_line_from_pt(trbs, t, corner, cr);
		if (cr != NULL) {
			if (trbs_vis_trace) tprintf(" -> %ld\n", cr->uid);
			return cr;
		}

		/* or count back one crossing from the last sentinel so cr is the crossing
		   after which our landing region is, right before the ending sentinel */
		cr = gdl_last(&e->crosses);
		assert(cr != NULL);
		assert(cr->link_edge.prev != NULL);
		cr = cr->link_edge.prev;
		if (trbs_vis_trace) {
			if (cr != NULL)
				tprintf(" -> %ld (last)\n", cr->uid);
			else
				tprintf(" -> NULL (last)\n");
		}
		return cr;
	}


	/* else we have reached an existing path line crossing srce - need to use
	   that to jump on the next edge, which may be diffreent from dste */
	trbs->collision = cr->twonet;

	/* check if next point is in the same triangle */
	cr2 = gdl_next(&cr->twonet->route, cr);
	if (cr2 == NULL) {
		/* going into a triangle point at the end */
		if (trbs_vis_trace) tprintf(" INS1 at %ld\n", cr->uid);
		cr2 = trbs_vis_get_pt_insertion(trbs, t, cr->twonet->end, cr->edge, srcdir);
		if (cr2 != NULL) {
			if (trbs_vis_trace) tprintf("  -> %ld\n", cr2->uid);
			return cr2;
		}
	}
	else {
		/* next cr - check if within the same triangle */
		if (trbs_vis_trace) tprintf(" cr2a=%ld (in triangle %d)\n", cr2->uid, trbs_vis_cr_in_trianlge(trbs, t, cr2));
		if (trbs_vis_cr_in_trianlge(trbs, t, cr2))
			return trbs_vis_jump_path_line(trbs, t, cr, cr2, srcdir);
	}

	/* else check if previus point is in the same triangle */
	cr2 = gdl_prev(&cr->twonet->route, cr);
	if (cr2 == NULL) {
		/* going into a triangle point at the start */
		if (trbs_vis_trace) tprintf(" INS2 at %ld\n", cr->uid);
		cr2 = trbs_vis_get_pt_insertion(trbs, t, cr->twonet->start, cr->edge, srcdir);
		if (cr2 != NULL) {
			if (trbs_vis_trace) tprintf("  -> %ld\n", cr2->uid);
			return cr2;
		}
	}
	else {
		/* prev cr - check if within the same triangle */
		if (trbs_vis_trace) tprintf(" cr2b=%ld (in triangle %d)\n", cr2->uid, trbs_vis_cr_in_trianlge(trbs, t, cr2));
		if (trbs_vis_cr_in_trianlge(trbs, t, cr2))
			return trbs_vis_jump_path_line(trbs, t, cr, cr2, srcdir);
	}

	/* path crosses triangle edge but does not enter the triangle: this happens
	   when it is moving along the edge (conencting two points directly) */
	return NULL;
}

/* Returns insert-after-crossing on dst if dst is visible from src;
   returns NULL if not visible. */
static trbs_cross_t *trbs_is_visible_from_edge(trbs_t *trbs, triangle_t *t, edge_t *dst, trbs_cross_t *src_aft)
{
	int timeout;
	trbs_cross_t *cr = src_aft;

	/* go around in CCW to find our target edge */
	for(timeout = 0; timeout < 3; timeout++) {
		cr = trbs_vis_jump_edge(trbs, t, cr);
		if (cr == (trbs_cross_t *)trbs->target)
			return cr;

#if 0
		if (cr->twonet == NULL) { /* hit a sentinel, check if it reached the end point */
			int is_last = (cr->link_edge.next == NULL);
			point_t *pt = is_last ? cr->edge->endp[1] : cr->edge->endp[0];
			if (pt == trbs->target)
				return trbs->target;
		}
#endif

		if (cr->edge == dst)
			return cr; /* positive: found the next edge region */
		if (cr->edge == src_aft->edge)
			return NULL; /* negative: traced back to our own edge */
	}

	/* We should never get here: a triangle has 3 edges, so in 3 jumps we
	   either reached our dst or reached back on src or dst is not in the
	   triangle or the triangle is broken. */
	fprintf(stderr, "trbs_is_visible_from_edge(): broken triangle or dst query\n");
	abort();
	return NULL; /* suppress warning */
}

/* Returns insert-after-crossing index on dst if dst is visible from src;
   returns NULL if not visible */
static trbs_cross_t *trbs_is_visible_from_pt(trbs_t *trbs, triangle_t *t, edge_t *dst, point_t *src)
{
	edge_t *se = NULL, *te[3];
	double dx[3], dy[3];
	int n, m;
	trbs_cross_t *snt;
	trbs_edge_t *ste;

#warning TODO: according to Wojciech edges are CCW ordered so we do not need this search
	/* find edge 'CCW' from the point */
	for(n = m = 0; n < 3; n++) {
		if (t->e[n]->endp[0] == src) {
			dx[m] = t->e[n]->endp[1]->pos.x - t->e[n]->endp[0]->pos.x;
			dy[m] = t->e[n]->endp[1]->pos.y - t->e[n]->endp[0]->pos.y;
			te[m++] = t->e[n];
		}
		else if (t->e[n]->endp[1] == src) {
			dx[m] = t->e[n]->endp[0]->pos.x - t->e[n]->endp[1]->pos.x;
			dy[m] = t->e[n]->endp[0]->pos.y - t->e[n]->endp[1]->pos.y;
			te[m++] = t->e[n];
		}
	}
	assert(m == 2);

	/* se is the one that is 'CCW' from the point */
	se = ((line_fake_angle(dx[0], dy[0]) - line_fake_angle(dx[1], dy[1])) < 0) ? te[0] : te[1];
	ste = se->data;

	snt = (se->endp[0] == src) ? gdl_first(&ste->crosses) : gdl_last(&ste->crosses);

	/* start searching from the starting sentinel of the edge */
	return trbs_is_visible_from_edge(trbs, t, dst, snt);
}


void trbs_next_edge_from_point(trbs_t *trbs, vtp0_t *dst, point_t *from)
{
	trianglelist_node_t *tn;

	for(tn = from->adj_triangles; tn != NULL; tn = tn->next) {
		triangle_t *t = tn->item;
		edge_t *e = trbs_pt_opposite_edge(trbs, t, from);
		trbs_cross_t *cr;
		trbs_edge_t *tedge = e->data;

		cr = gdl_first(&tedge->crosses);
		if (trbs_vis_trace) tprintf("\n?vis_from_pt P%ld: edge=%ld\n", ((trbs_point_t *)from->data)->uid, cr->uid);

		if ((e != NULL) && ((cr = trbs_is_visible_from_pt(trbs, t, e, from)) != NULL)) {
			/* don't allow jumping on blocked edge */
			if (!is_edge_blocked(tedge, trbs->routing_net)) {
				if (trbs_vis_trace) tprintf("!vis_from_pt P%ld: %ld\n", ((trbs_point_t *)from->data)->uid, cr->uid);
				vtp0_append(dst, cr);
			}
		}
	}
}

void trbs_next_edge_from_edge(trbs_t *trbs, vtp0_t *dst, triangle_t *t, trbs_cross_t *from_aft, double thickness)
{
	int n;
	trbs_cross_t *cr = from_aft;

	for(n = 0; n < 3; n++) {
		trbs_edge_t *te;

		/* determine the next crossing */
		cr = trbs_vis_jump_edge(trbs, t, cr);

		if (cr == (trbs_cross_t *)trbs->target) {
			vtp0_append(dst, cr);
			break;
		}

		/* if arrived back to the starting edge, we have mapped all possible
		   landing zones (on all visible target edges) */
		if ((cr == NULL) || (cr->edge == from_aft->edge))
			break;

		te = cr->edge->data;

		/* don't allow jumping on blocked edge */
		if (is_edge_blocked(te, trbs->routing_net)) {
			tprintf("  skip: blocked\n");
			continue;
		}

		if (te->cap < thickness) {
			tprintf("  skip: cap %f < %f\n", te->cap, thickness);
			continue;
		}

		if (trbs_vis_trace)  {
			tprintf("edge2edge %ld=>%ld in triangle", from_aft->uid, cr->uid);
			trbs_print_triangle(t);
			tprintf("\n");
		}
		vtp0_append(dst, cr);
	}
}

