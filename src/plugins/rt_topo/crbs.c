/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2021 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include <genlist/gendlist.h>

#include "data.h"
#include "io.h"
#include "route_res.h"

#include <libgrbs/debug.h>
#include <gengeo2d/cline.h>

#include "crbs.h"

grbs_2net_t tn_unknown;

static void crbs_coll_report_arc_cb(grbs_t *grbs, grbs_2net_t *tn, grbs_2net_t *coll_tn, grbs_arc_t *coll_arc)
{
/*	crbs_t *crbs = grbs->user_data;*/
	crbs_2net_t *coll_ctn = coll_tn->user_data;
/*	crbs_2net_t *ctn = tn->user_data;*/

	if (coll_tn != &tn_unknown)
		coll_ctn->coll = 1;
}

static void crbs_coll_report_line_cb(grbs_t *grbs, grbs_2net_t *tn, grbs_2net_t *coll_tn, grbs_line_t *coll_line)
{
/*	crbs_t *crbs = grbs->user_data;*/
	crbs_2net_t *coll_ctn = coll_tn->user_data;
/*	crbs_2net_t *ctn = tn->user_data;*/

	if (coll_tn != &tn_unknown)
		coll_ctn->coll = 1;
}

static void crbs_coll_report_check_cb(grbs_t *grbs, grbs_2net_t *tn, grbs_2net_t *coll_tn)
{
/*	crbs_t *crbs = grbs->user_data;*/
	crbs_2net_t *coll_ctn = coll_tn->user_data;
/*	crbs_2net_t *ctn = tn->user_data;*/

	if (coll_tn != &tn_unknown)
		coll_ctn->coll = 1;
}


static int crbs_coll_ingore_tn_line_cb(grbs_t *grbs, grbs_2net_t *tn, grbs_line_t *l)
{
	crbs_2net_t *lctn = l->user_data;
	crbs_2net_t *tctn = tn->user_data;

	assert(tctn != NULL);

	if (lctn == NULL)
		return 0;

	return lctn->net == tctn->net;
}

static int crbs_coll_ingore_tn_point_cb(grbs_t *grbs, grbs_2net_t *tn, grbs_point_t *pt)
{
	crbs_point_t *p = pt->user_data;
	crbs_2net_t *tctn = tn->user_data;

	assert(tctn != NULL);

	if (p == NULL)
		return 0;

	return p->net == tctn->net;
}


crbs_point_t *crbs_point_new(crbs_t *crbs)
{
	crbs_point_t *p = calloc(sizeof(crbs_point_t), 1);
#warning TODO: do this allocation using ualloc stacks
	return p;
}

#include <genht/hash.h>
GRBS_ADDR_HASH(htad_hash);

point_t *crbs_make_point(crbs_t *crbs, double x, double y, double cop, double clr, rtrnd_via_t *via, rtrnd_net_t *net)
{
	grbs_point_t *gpt;
	point_t *cpt;
	crbs_point_t *p;

	cpt = cdt_insert_point(&crbs->cdt, x, y);
	if (cpt->data != NULL) { /* already initialized */
		int grew;
		
		p = cpt->data;
		grew = (cop > p->gpt->copper) || (clr > p->gpt->clearance);

		if (grew) {
			grbs_point_unreg(&crbs->grbs, p->gpt);

			if (cop > p->gpt->copper)
				p->gpt->copper = cop;
			if (clr > p->gpt->clearance)
				p->gpt->clearance = clr;

			grbs_point_reg(&crbs->grbs, p->gpt);

			printf("GT point_chg P%ld  %f %f\n", p->gpt->uid, p->gpt->copper, p->gpt->clearance);
		}
		return cpt;
	}

	p = crbs_point_new(crbs);
	cpt->data = p;
	gpt = grbs_point_new(&crbs->grbs, x, y, cop, clr);
	gpt->user_data = p;

	printf("GT point_new P%ld  %f %f  %f %f\n", gpt->uid, x, y, cop, clr);

	p->cpt = cpt;
	p->gpt = gpt;
	p->obj = (rtrnd_any_obj_t *)via;
	p->net = net;

	return cpt;
}

/* Return an existing point with the same net near x;y (max distance
   is dist) or create a new point and return that if there was no match near */
point_t *crbs_make_point_near(crbs_t *crbs, double x, double y, double cop, double clr, rtrnd_via_t *via, rtrnd_net_t *net, double dist)
{
	grbs_rtree_box_t bbox;
	grbs_point_t *pt;
	grbs_rtree_it_t it;
	double dp2 = dist/2, dist2 = dist*dist, dx, dy;

	bbox.x1 = x - dp2;
	bbox.y1 = y - dp2;
	bbox.x2 = x + dp2;
	bbox.y2 = y + dp2;

	for(pt = grbs_rtree_first(&it, &crbs->grbs.point_tree, &bbox); pt != NULL; pt = grbs_rtree_next(&it)) {
		crbs_point_t *cpt = pt->user_data;
		if (cpt->net != net) continue;
		dx = pt->x - x;
		dy = pt->y - y;
		if ((dx*dx+dy*dy) > dist2) continue;
		return cpt->cpt;
	}

	/* nothing found near */
	return crbs_make_point(crbs, x, y, cop, clr, via, net);
}

void crbs_auto_created_arc_cb(grbs_t *grbs, grbs_2net_t *tn, grbs_arc_t *a)
{
	a->user_data = tn->user_data;
}

static void crbs_init(crbs_t *crbs, rtrnd_t *ctx)
{

	grbs_init(&crbs->grbs);
	crbs->ctx = ctx;
	crbs->grbs.coll_report_arc_cb = crbs_coll_report_arc_cb;
	crbs->grbs.coll_report_line_cb = crbs_coll_report_line_cb;
	crbs->grbs.coll_report_check_cb = crbs_coll_report_check_cb;
	/* Note: crbs->grbs.coll_report_pt_cb was not called back in 2021 either; may be a bug */
	crbs->grbs.coll_ingore_tn_line= crbs_coll_ingore_tn_line_cb;
	crbs->grbs.coll_ingore_tn_point = crbs_coll_ingore_tn_point_cb;
	crbs->grbs.auto_created_arc = crbs_auto_created_arc_cb;

	crbs->grbs.user_data = crbs;
	htad_init(&crbs->addrs, htad_hash, grbs_addr_hash_keyeq);

}

static void crbs_clean(crbs_t *crbs)
{
}

static void crbs_uninit(crbs_t *crbs)
{
}


static long crbs_dist_heur(double x1, double y1, double x2, double y2)
{
	double dx = x2 - x1, dy = y2 - y1, d = dx*dx+dy*dy;

	return d == 0 ? 0 : floor(sqrt(d * DIST_HEUR_MULT));
}



#include "crbs_cdt.c"
#include "crbs_route.c"


int rt_topo_crbs(rtrnd_t *ctx, rt_topo_laa2rbs_t *src)
{
	int n, res = 0;

	/* route each layer */
	for(n = 0; n < ctx->board->layers.used; n++)
		res |= rt_topo_crbs_layer(ctx, ctx->board->layers.array[n], src->ly2nets.array[n]);

	crbs_draw_vias(ctx);

	return res;
}


static int crbs_grbs_draw(crbs_t *crbs, char *fn)
{
	FILE *f = fopen(fn, "w");

	if (f == NULL)
		return -1;

	grbs_draw_begin(&crbs->grbs, f);
	fprintf(f, "<!-- DUMP:\n");
	grbs_dump_points(&crbs->grbs, f);
	grbs_dump_wires(&crbs->grbs, f);
	fprintf(f, "-->\n");
	grbs_draw_points(&crbs->grbs, f);
	grbs_draw_wires(&crbs->grbs, f);
	grbs_draw_end(&crbs->grbs, f);

	fclose(f);

	return 0;
}

static int crbs_grbs_dump(crbs_t *crbs, char *fn)
{
	FILE *f = fopen(fn, "w");

	if (f == NULL)
		return -1;

	grbs_dump_points(&crbs->grbs, f);
	grbs_dump_wires(&crbs->grbs, f);

	fclose(f);

	return 0;
}

void crbs_draw_routes(crbs_t *crbs, rtrnd_layer_t *ly_out, rtrnd_layer_t *ly_drw)
{
	grbs_line_t *l;
	grbs_arc_t *a;
	rtrnd_any_obj_t *o;
	crbs_2net_t *ctn;

	for(l = gdl_first(&crbs->grbs.all_lines); l != NULL; l = gdl_next(&crbs->grbs.all_lines, l)) {
		ctn = l->user_data;
		if ((ctn != NULL) && ctn->old) continue;
		rtrnd_line_new(ly_drw, NULL, NULL, l->x1, l->y1, l->x2, l->y2, 0.2, 0);
		if (ly_out != NULL) {
			o = (rtrnd_any_obj_t *)rtrnd_line_new(ly_out, NULL, (ctn == NULL ? NULL : ctn->net), l->x1, l->y1, l->x2, l->y2, rt_topo_cfg.wire_thick, 0);
			rtrnd_res_add(crbs->ctx, o);
		}
	}

	for(a = gdl_first(&crbs->grbs.all_arcs); a != NULL; a = gdl_next(&crbs->grbs.all_arcs, a)) {
		if (!a->in_use) continue;
		if ((a->r == 0) || (a->da == 0)) continue;

		ctn = a->user_data;
		if ((ctn == NULL) || ctn->old) continue;

		rtrnd_arc_new(ly_drw, NULL, NULL, a->parent_pt->x, a->parent_pt->y, a->r, a->sa, a->da, 0.2, 0);
		if (ly_out != NULL) {
			o = (rtrnd_any_obj_t *)rtrnd_arc_new(ly_out, NULL, (ctn == NULL ? NULL : ctn->net), a->parent_pt->x, a->parent_pt->y, a->r, a->sa, a->da, rt_topo_cfg.wire_thick, 0);
			rtrnd_res_add(crbs->ctx, o);
		}
	}
}

void crbs_draw_vias(rtrnd_t *ctx)
{
	rtrnd_any_obj_t *v;
	rtrnd_rtree_it_t it;

	for(v = rtrnd_rtree_all_first(&it, &ctx->board->vias); v != NULL; v = rtrnd_rtree_all_next(&it)) {
		if (v->hdr.created)
			rtrnd_res_add(ctx, v);
	}
}
