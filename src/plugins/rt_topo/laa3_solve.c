/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* LAA step III: find the optimal resolution of crossings by activating
   vias and detorus */

/* bloat up via bboxes so via-via placement leaves enough room for a trace; also
   make sure vias are not placed closer than this initially */
#define VIA_SPACING_BLOAT ((rt_topo_cfg.wire_thick + rt_topo_cfg.wire_clr) * rt_topo_cfg.via_via_space)


static double laa_cost_2net_update(rt_topo_laa_2net_t *tn)
{
	double cost = 0;
	int ci;

	for(ci = 0; ci < tn->cross.used; ci++) {
		rtrnd_crossing_t *cr = &tn->cross.array[ci];
		cost += cr->detcost;
	}

	tn->cost_detour = cost;
	return cost;
}


/* Wire length (detour) cost, already multipled by 1-alpha */
static detour_t laa_cost_det = {0};
static double laa_cost_2net_calc_cross(laa3_t *laa3, rt_topo_laa_2net_t *tn, int ci, int update)
{
	rtrnd_crossing_t *cr = &tn->cross.array[ci];
	double c = laa_cross_cost(laa3, tn, cr, &laa_cost_det) * rt_topo_cfg.alpha2;
	if (update) {
		cr->detcost = c;
		cr->tn_detouring = laa_cost_det.tn_detouring;
	}
	return c;
}

/* Total via cost, already multiplied by alpha */
static double laa_cost_2net_calc_via_layer(laa3_t *laa3, rt_topo_laa_2net_t *tn, int update)
{
	int n;
	double cost = 0;
	for(n = 1; n < tn->br.used; n++) {
		if (tn->asg[n-1] != tn->asg[n])
			cost += rt_topo_cfg.alpha;
	}
	if (update)
		tn->cost_vialayer = cost;
	return cost;
}

static void laa3_solve_init(laa3_t *laa3)
{
	double cost = 0;
	htsp_entry_t *e;
	rtrnd_t *ctx = laa3->ctx;

	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rt_topo_2nets_t *tns = NETDATA_LAA(net);
		rt_topo_laa_2net_t *tn;

		for(tn = tns->head; tn != NULL; tn = tn->next) {
			int ci;
			for(ci = 0; ci < tn->cross.used; ci++)
				laa_cost_2net_calc_cross(laa3, tn, ci, 1);
			cost += laa_cost_2net_update(tn);
		}
	}

	printf("initial board cost: %f\n", cost);
}

/* Return 1 if layer assignment along a tn has 2 vias too close */
static int laa3_vias_too_close(rt_topo_laa_2net_t *tn, double min_dist2)
{
	double lastx, lasty;
	long bi;

	/* printf("VIAS-too-close: %f;%f to %f;%f:\n", tn->br.array[0].x, tn->br.array[0].y,  tn->br.array[tn->br.used-1].x, tn->br.array[tn->br.used-1].y); */

	lastx = tn->br.array[0].x;
	lasty = tn->br.array[0].y;
	for(bi = 1; bi < tn->br.used; bi++) {
		if (tn->asg[bi] != tn->asg[bi-1]) {
			double dx = tn->br.array[bi].x - lastx;
			double dy = tn->br.array[bi].y - lasty;
			double dist2 = dx*dx + dy*dy;

			/* printf(" dist2=%f (< %f) at %f;%f\n", dist2, min_dist2, tn->br.array[bi].x, tn->br.array[bi].y); */
			if (dist2 < min_dist2)
				return 1;
			lastx = tn->br.array[bi].x;
			lasty = tn->br.array[bi].y;
		}
	}

	return 0;
}

static double laa3_solve_best(laa3_t *laa3)
{
	htsp_entry_t *e;
	rtrnd_t *ctx = laa3->ctx;
	double best_gain = 0, gain, best_detcost, detcost, best_vialayercost, vialayercost;
	rt_topo_laa_2net_t *tn, *best_tn = NULL;
	int lid, ci, best_ci = -1, best_lid = -1, orig_lid;
	double min_dist2 = VIA_SPACING_BLOAT + rt_topo_cfg.via_dia + rt_topo_cfg.via_clr;

	min_dist2 = min_dist2 * min_dist2;

	/* take each crossing... */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rt_topo_2nets_t *tns = NETDATA_LAA(net);
		for(tn = tns->head; tn != NULL; tn = tn->next) {
			for(ci = 0; ci < tn->cross.used; ci++) {
				rtrnd_crossing_t *cr = &tn->cross.array[ci];
				rtrnd_2branch_t *br = &tn->br.array[cr->bridx];
				long lbit = 1;

				if (cr->detcost/2 > best_gain) {
					/* ... and try relocating the segment to different layers and see the cost */
					orig_lid = tn->asg[cr->bridx]; /* remember original assignment */
					for(lid = 0; (lid < ctx->board->layers.used); lid++, lbit <<= 1) {
						if (lid == orig_lid) continue;
						if (!(br->pt_layers & lbit)) continue;
						tn->asg[cr->bridx] = lid; /* temporarily change the assingment so costs can be recalculated */
						detcost = laa_cost_2net_calc_cross(laa3, tn, ci, 0);
						vialayercost = laa_cost_2net_calc_via_layer(laa3, tn, 0);
						gain = (cr->detcost + tn->cost_vialayer) - (detcost + vialayercost);
						if (laa3_vias_too_close(tn, min_dist2)) gain = 0;
						if (gain > best_gain) {
							best_gain = gain;
							best_tn = tn;
							best_ci = ci;
							best_lid = lid;
							best_detcost = detcost;
							best_vialayercost = vialayercost;
						}
					}
					tn->asg[cr->bridx] = orig_lid; /* restore original settings for now */
				}
			}
		}
	}

	printf("laa3 solver gain: %f", best_gain);
	if (best_gain > 0) {
		rtrnd_crossing_t *cr = &best_tn->cross.array[best_ci];

		printf(" by moving tn %p seg %d from layer %d to %d\n", best_tn, best_ci, best_tn->asg[cr->bridx], best_lid);
		best_tn->asg[cr->bridx] = best_lid;
		cr->detcost = best_detcost;
		best_tn->cost_vialayer = best_vialayercost;

		/* need to update the other side of the crossing; via/layer cost doesn't need update as it didn't change */
		laa_cost_2net_calc_cross(laa3, cr->cn, cr->cn_cridx, 1);

/* can not do this check: it may be that some crossing happens to be on the
   same coord as an unrelated pin; common when the board uses grid and well
   aligned pins */
#ifdef DOUBLE_CHECK_CROSSING
		{
		int cridx2;
		for(cridx2 = 0; cridx2 < cr->cn->cross.used; cridx2++) {
			rtrnd_crossing_t *cr2 = &cr->cn->cross.array[cridx2];
			if ((fabs(cr2->x-cr->x) < 0.0001) && (fabs(cr2->y-cr->y) < 0.0001)) {
				if (cr->cn_cr != cr2)
					printf("  update2 mismatch %d %d at {%f %f} vs {%f %f} (%p == %p)\n", cr->cn_bridx, cr2->bridx, cr->x, cr->y, cr2->x, cr2->y, cr->cn_cr, cr2);
				assert(cr->cn_cr == cr2);
			}
		}
		}
#endif

	}
	printf("\n");
	return best_gain;
}

static int found_stop_cb(rtrnd_find_t *ctx, rtrnd_any_obj_t *new_obj, rtrnd_any_obj_t *arrived_from)
{
	if (arrived_from == NULL)
		return 0;
printf("found %s %p\n", new_obj->hdr.oid, arrived_from);
	return 1;
}

static rtrnd_via_t *laa3_find_via(laa3_t *laa3, vtp0_t *vias, double x, double y, rtrnd_net_t *net)
{
	long n;
	for(n = 0; n < vias->used; n++) {
		rtrnd_via_t *via = vias->array[n];
		if ((via->x == x) && (via->y == y) && (via->hdr.net == net))
			return via;
	}
	return NULL;
}

static rtrnd_via_t *laa3_board_via_at(rtrnd_t *ctx, double x, double y)
{
	rtrnd_rtree_it_t it;
	rtrnd_rtree_box_t bbox;

	bbox.x1 = x - 0.001; bbox.y1 = y - 0.001;
	bbox.x2 = x + 0.001; bbox.y2 = y + 0.001;

	return rtrnd_rtree_first(&it, &ctx->board->vias, &bbox);
}

static void laa3_check_steiner_via(laa3_t *laa3, vtp0_t *vias, rt_topo_laa_2net_t *tn1, int i1, rt_topo_laa_2net_t *tn2, int i2, double x, double y)
{
	int fx1, fx2;
	int ly1, ly2;
	rtrnd_via_t *via;

	fx1 = (i1 == 1) ? tn1->p1fx : tn1->p2fx;
	fx2 = (i2 == 1) ? tn2->p1fx : tn2->p2fx;

	/* don't do anything if this endpoint is fixed (terminal) */
	if (fx1 || fx2)
		return;

	ly1 = (i1 == 1) ? tn1->asg[0] : tn1->asg[LAA_MAX(tn1->br.used-1, 0)];
	ly2 = (i2 == 1) ? tn2->asg[0] : tn2->asg[LAA_MAX(tn2->br.used-1, 0)];

	/* don't add via if there's no layer switch */
	if (ly1 == ly2)
		return;

	/* add via if there's no via there already */
	via = laa3_find_via(laa3, vias, x, y, tn1->net);
	if (via == NULL) {
		char oid[128];
		printf("STEINER VIA: %f %f  fx %d %d  ly %d %d\n", x, y, fx1, fx2, ly1, ly2);

		sprintf(oid, "rt_topo_sv_%ld", vias->used);
		via = rtrnd_via_alloc(oid, x, y, rt_topo_cfg.via_dia, rt_topo_cfg.via_clr);
		via->hdr.net = tn1->net;
		vtp0_append(vias, via);
	}

	/* update two-net branch via bindings */
	if (i1 == 1)
		tn1->br.array[0].via = via;
	else
		tn1->br.array[LAA_MAX(tn1->br.used-1, 0)].via = via;
	if (i2 == 1)
		tn2->br.array[0].via = via;
	else
		tn2->br.array[LAA_MAX(tn2->br.used-1, 0)].via = via;
}

static void laa3_via_rtree_reg(laa3_t *laa3, rtrnd_rtree_t *rtv, rtrnd_via_t *via)
{
	rtrnd_rtree_insert(rtv, via, &via->hdr.bbox);
}

static void laa3_via_rtree_unreg(laa3_t *laa3, rtrnd_rtree_t *rtv, rtrnd_via_t *via)
{
	rtrnd_rtree_delete(rtv, via, &via->hdr.bbox);
}

static int laa3_via_rtree_find(laa3_t *laa3, rtrnd_rtree_t *rtv, rtrnd_via_t *via)
{
	rtrnd_via_t *v;
	rtrnd_rtree_it_t it;

	for(v = rtrnd_rtree_first(&it, rtv, &via->hdr.bbox); v != NULL; v = rtrnd_rtree_next(&it))
		if (v != via)
			return 2;

	return 0;
}


static void laa3_via_rtree_init(laa3_t *laa3, rtrnd_rtree_t *rtv, vtp0_t *vias)
{
/*	rtrnd_t *ctx = laa3->ctx;*/
	long n;
	double bloat = VIA_SPACING_BLOAT;


	rtrnd_rtree_init(rtv);
	for(n = 0; n < vias->used; n++) {
		rtrnd_via_t *via = vias->array[n];
		via->hdr.bbox.x1 -= bloat; via->hdr.bbox.y1 -= bloat;
		via->hdr.bbox.x2 += bloat; via->hdr.bbox.y2 += bloat;
		laa3_via_rtree_reg(laa3, rtv, via);
	}
}

static void laa3_via_rtree_uninit(laa3_t *laa3, rtrnd_rtree_t *rtv, vtp0_t *vias)
{
	rtrnd_rtree_uninit(rtv);
}


int laa3_try_place_vias(laa3_t *laa3, vtp0_t *vias)
{
	rtrnd_t *ctx = laa3->ctx;
	rtrnd_via_t *via;
	htsp_entry_t *e;
	rt_topo_laa_2net_t *tn, *tn1, *tn2;
	int bi, limit;
	rtrnd_rtree_t rtv;

	/* create vias on steiner points if there's a layer switch */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rt_topo_2nets_t *tns = NETDATA_LAA(net);
		for(tn1 = tns->head; tn1 != NULL; tn1 = tn1->next) {
			for(tn2 = tn1->next; tn2 != NULL; tn2 = tn2->next) {
				if ((tn1->x1 == tn2->x1) && (tn1->y1 == tn2->y1))
					laa3_check_steiner_via(laa3, vias, tn1, 1, tn2, 1, tn1->x1, tn1->y1);
				else if ((tn1->x1 == tn2->x2) && (tn1->y1 == tn2->y2))
					laa3_check_steiner_via(laa3, vias, tn1, 1, tn2, 2, tn1->x1, tn1->y1);
				else if ((tn1->x2 == tn2->x2) && (tn1->y2 == tn2->y2))
					laa3_check_steiner_via(laa3, vias, tn1, 2, tn2, 2, tn1->x2, tn1->y2);
				else if ((tn1->x2 == tn2->x1) && (tn1->y2 == tn2->y1))
					laa3_check_steiner_via(laa3, vias, tn1, 2, tn2, 1, tn1->x2, tn1->y2);
			}
		}
	}


	/* create vias where a branch switched layer */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rt_topo_2nets_t *tns = NETDATA_LAA(net);
		for(tn = tns->head; tn != NULL; tn = tn->next) {
			for(bi = 1; bi < tn->br.used; bi++) {
				if (tn->asg[bi] != tn->asg[bi-1]) {
					char oid[128];
					rtrnd_via_t *ev;

					sprintf(oid, "rt_topo_v_%ld", vias->used);
					ev = laa3_board_via_at(ctx, tn->br.array[bi].x, tn->br.array[bi].y);
					if (ev != NULL) {
						if (ev->hdr.net == net)
							continue; /* we already have a via at the right place */
						/* via, but wrong net; go on placing a new one (on the same spot,
						   the solver will move it away due to collision) */
					}
					via = laa3_find_via(laa3, vias, tn->br.array[bi].x, tn->br.array[bi].y, net);
					printf(" VIA at %f;%f: %p\n", tn->br.array[bi].x, tn->br.array[bi].y, via);
					if (via == NULL) {
						via = rtrnd_via_alloc(oid, tn->br.array[bi].x, tn->br.array[bi].y, rt_topo_cfg.via_dia, rt_topo_cfg.via_clr);
						via->hdr.net = net;
						vtp0_append(vias, via);
					}
					tn->br.array[bi].via = via;
				}
			}
		}
	}

	/* look at all two-net start and end point and see if it falls on a via
	   (and then bind it) */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rt_topo_2nets_t *tns = NETDATA_LAA(net);
		for(tn = tns->head; tn != NULL; tn = tn->next) {
			if (tn->br.array[0].via == NULL) {
				via = laa3_find_via(laa3, vias, tn->br.array[0].x, tn->br.array[0].y, net);
				if (via != NULL)
					tn->br.array[0].via = via;
			}
			bi = tn->br.used-1;
			if (tn->br.array[bi].via == NULL) {
				via = laa3_find_via(laa3, vias, tn->br.array[bi].x, tn->br.array[bi].y, net);
				if (via != NULL)
					tn->br.array[bi].via = via;
			}
		}
	}

	laa3_via_rtree_init(laa3, &rtv, vias);

	/* resolve via geo collisions by randomly tuning via positions */
	for(limit = 10240; limit > 0; limit--) {
		int collided = 0;
		long n;

		for(n = 0; n < vias->used; n++) {
			rtrnd_via_t *via = vias->array[n];
			rtrnd_find_t fctx = {0};
			double dx, dy;
			int find_inited;

			fctx.nfound = laa3_via_rtree_find(laa3, &rtv, via);

			if (fctx.nfound == 0) {
				fctx.found_cb = found_stop_cb;
				fctx.bloat = rt_topo_cfg.via_clr;
				find_inited = 1;
				rtrnd_find_from_obj(&fctx, ctx->board, (rtrnd_any_obj_t *)via);
			}
			else
				find_inited = 0;

			if (fctx.nfound > 1) {
				static int mtw_inited;
				static psr_mtw_t mtw;
/*printf("via coll at %f;%f\n", via->x, via->y);*/
				if (!mtw_inited) {
					psr_mtw_init(&mtw, 37);
					mtw_inited = 1;
				}
				laa3_via_rtree_unreg(laa3, &rtv, via);
				dx = psr_mtw_rand01(&mtw) * 0.2 - 0.1;
				dy = psr_mtw_rand01(&mtw) * 0.2 - 0.1;
				via->x += dx;
				via->y += dy;
				via->hdr.bbox.x1 += dx;
				via->hdr.bbox.y1 += dy;
				via->hdr.bbox.x2 += dx;
				via->hdr.bbox.y2 += dy;
				laa3_via_rtree_reg(laa3, &rtv, via);
				collided = 1;
			}

			if (find_inited)
				rtrnd_find_free(&fctx);
		}
		if (!collided)
			goto done;  /* no collision => done */
	}
printf("Failed to resolve via sites\n");
	laa3_via_rtree_uninit(laa3, &rtv, vias);
	return -1;

	done:

	laa3_via_rtree_uninit(laa3, &rtv, vias);

	/* when ends are tied to vias make sure their coords are updated to via moves */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rt_topo_2nets_t *tns = NETDATA_LAA(net);
		for(tn = tns->head; tn != NULL; tn = tn->next) {
			long idx;
			for(idx = 0; idx < tn->br.used; idx++) {
				via = tn->br.array[idx].via;
				if (via != NULL) {
printf("via end adjust2: %f;%f -> %f;%f\n", tn->x2, tn->y2, via->x, via->y);
					tn->x2 = tn->br.array[idx].x = via->x;
					tn->y2 = tn->br.array[idx].y = via->y;
				}
			}
		}
	}
	return 0;
}

void laa3_realize_vias(laa3_t *laa3, vtp0_t *vias)
{
	rtrnd_t *ctx = laa3->ctx;
	long n;
	for(n = 0; n < vias->used; n++) {
		rtrnd_via_t *via = vias->array[n];
		rtrnd_net_t *net = via->hdr.net;
		via->hdr.net = NULL;
		via->hdr.created = 1;
		rtrnd_via_reg(ctx->board, net, via);
	}
}

static int laa3_solve(laa3_t *laa3)
{
	vtp0_t vias = {0};

	laa3_solve_init(laa3);
	while(laa3_solve_best(laa3) > 0) ;
	if (laa3_try_place_vias(laa3, &vias) != 0)
		return -1;
	laa3_realize_vias(laa3, &vias);
	return 0;
}
