/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* LAA step II: place via sites on 2nets, building the assignment graph; adds
   potential via sites between crossings. Included from laa.c */

RTRND_INLINE double pt2offs(rt_topo_laa_2net_t *tn, double x, double y)
{
	g2d_vect_t pt;
	g2d_cline_t cl;

	pt.x = x; pt.y = y;
	cl.p1.x = tn->x1; cl.p1.y = tn->y1;
	cl.p2.x = tn->x2; cl.p2.y = tn->y2;
	return g2d_offs_cline_pt(&cl, pt);
}

static rtrnd_2branch_t *br_append_point(rt_topo_laa_2net_t *tn, vt2br_t *br, double x, double y, int fixed, long pt_layers, int avoid_overlap)
{
	rtrnd_2branch_t *p;

	/* do not add overlapping points (zero length branches) */
	if (avoid_overlap && (br->used > 0)) {
		p = &br->array[br->used-1];
		if ((fabs(p->x-x) < 0.00001) && (fabs(p->y-y) < 0.00001))
			return p;
	}


	p = vt2br_alloc_append(br, 1);
	p->parent = tn;
	p->offs = pt2offs(tn, x, y);
	p->x = x; p->y = y;
	p->coord_fixed = fixed;
	p->pt_layers = pt_layers;
	p->edge_ly_fixed = -1; /* assume free-to-allocate outgoing edge */
	p->cridx = -1;
	return p;
}

static int cmp_crossing(const void *a, const void *b)
{
	const rtrnd_crossing_t *c1 = a, *c2 = b;

	if (c1->offs > c2->offs) return 1;
	return -1;
}

/* Look for 2net crossings to create potential via sites in 2nets */
static void laa_2net_vias(rtrnd_t *ctx, rtrnd_net_t *net, rtrnd_layer_t *ly, rtrnd_rtree_t *r2net)
{
	rt_topo_2nets_t *tns = NETDATA_LAA(net);
	rt_topo_laa_2net_t *tn, *cn;
	vtcr_t cr = {0}; /* crossings */
	long n, lall = layer_bits_all(ctx);

printf("2nets for %s:\n", net->hdr.oid);
	for(tn = tns->head; tn != NULL; tn = tn->next) {
		rtrnd_rtree_it_t it;
		g2d_cline_t l1;
		l1.p1.x = tn->x1; l1.p1.y = tn->y1; l1.p2.x = tn->x2; l1.p2.y = tn->y2;

		rtrnd_line_new(ly, NULL, NULL, tn->x1, tn->y1, tn->x2, tn->y2, tn->efx ? 0.01 : 0.1, 0);
printf(" [%d:%02lx] %.3f;%.3f %.3f;%.3f\n", tn->efx, tn->ely, tn->x1, tn->y1, tn->x2, tn->y2);
printf("	%d:%02lx %d:%02lx Crossings for %p:\n", tn->p1fx, tn->p1ly, tn->p2fx, tn->p2ly, tn);

		/* find all crossings (in random order) and build co, a list of offsets on tn/l1 */
		cr.used = 0;
		for(cn = rtrnd_rtree_first(&it, r2net, &tn->bbox); cn != NULL; cn = rtrnd_rtree_next(&it)) {
			g2d_cline_t l2;
			g2d_vect_t ip[2];
			g2d_offs_t offs[2];
			int iscs;
			rtrnd_crossing_t *cp;

			if (cn->net == tn->net)
				continue;
			l2.p1.x = cn->x1; l2.p1.y = cn->y1; l2.p2.x = cn->x2; l2.p2.y = cn->y2;

			iscs = g2d_iscp_cline_cline(&l1, &l2, ip, offs);
			switch(iscs) {
				case 0: break; /* no crossing, just bboxes got close */
				case 1: /* 2net crossing in a single point (common case) */
					printf("		%p at %.02f;%.02f (single): %.03f\n", cn, ip[0].x, ip[0].y, offs[0]);
					cp = vtcr_alloc_append(&cr, 1);
					cp->offs = offs[0];
					cp->x = ip[0].x; cp->y = ip[0].y;
					cp->cn = cn;
					cp->bridx = -1;
					break;
				case 2: /* 2net crossing in an overlap (rare case); place a single via in the middle */
					printf("		%p at %.02f;%.02f (overlap): %03f\n", cn, (ip[0].x+ip[1].x)/2.0, (ip[0].y+ip[1].y)/2.0, (offs[0]+offs[1])/2.0);
					cp = vtcr_alloc_append(&cr, 1);
					cp->offs = (offs[0]+offs[1])/2.0;
					cp->x = (ip[0].x+ip[1].x)/2.0; cp->y = (ip[0].y+ip[1].y)/2.0;
					cp->cn = cn;
					cp->bridx = -1;
					break;
			}
		}

		/* sort offsets so it's easier to calculate midpoints for via sites */
		qsort(cr.array, cr.used, sizeof(rtrnd_crossing_t), cmp_crossing);

		/* calculate potential via sites */
		br_append_point(tn, &tn->br, tn->x1, tn->y1, tn->p1fx, tn->p1ly, 0); /* append start */
		if (tn->efx) {
			tn->br.array[0].edge_ly_fixed = tn->ely; /* for fixed/static 2net edges, the outgoing edge of the branch is also fixed and is the same */
			for(n = 0; n < cr.used; n++)
				cr.array[n].bridx = tn->br.used;
		}

		/* build a list of potential via sites, if possible */
		if ((cr.used > 0) && !tn->efx) {
			g2d_cvect_t last, curr;
			/* if starting point does not reach every layer, need to add a via site, just in case */
			if (tn->p1ly != lall) {
				g2d_cvect_t cross = g2d__cline_offs(&l1, cr.array[0].offs);
				last.x = (tn->x1 + cross.x)/2;
				last.y = (tn->y1 + cross.y)/2;
				br_append_point(tn, &tn->br, last.x, last.y, 0, lall, 0);
			}

			/* cross-index the crossing on the outgoing of the first segment */
			n = 0;
			cr.array[n].bridx = tn->br.used-1;
			tn->br.array[tn->br.used-1].cridx = n;

			/* sites in between 2 crossings */
			for(n = 0; n < cr.used; n++) {
				curr = g2d__cline_offs(&l1, cr.array[n].offs);
				if (n > 0) {
					br_append_point(tn, &tn->br, (last.x + curr.x)/2, (last.y + curr.y)/2, 0, lall, 0);
					/* cross-index the crossing on the outgoing of theis segment */
					cr.array[n].bridx = tn->br.used-1;
					tn->br.array[tn->br.used-1].cridx = n;
				}
				last = curr;
			}
			/* if ending point does not reach every layer, need to add a via site, just in case */
			if (tn->p2ly != lall)
				br_append_point(tn, &tn->br, (tn->x2 + last.x)/2, (tn->y2 + last.y)/2, 0, lall, 1);
		}
		else if ((cr.used == 0) && !tn->efx) {
			/* special case: there are only a starting and an ending terminal and no
			   crossings; if neither terminals are 100% free to choose their layer,
			   better add a via so jumping layer is possible. Typical example:
			   a 2net ending in 2 SMD pads on opposit sides. */
			if ((tn->p1ly != lall) && (tn->p2ly != lall))
				br_append_point(tn, &tn->br, (tn->x1 + tn->x2)/2, (tn->y1 + tn->y2)/2, 0, lall, 1);
		}
		br_append_point(tn, &tn->br, tn->x2, tn->y2, tn->p2fx, tn->p2ly, 1); /* append end */

		printf("	via sites:");
		for(n = 0; n < tn->br.used; n++) {
			double x = tn->br.array[n].x, y = tn->br.array[n].y, size = 0.2;
			printf("   %.3f %.3f", x, y);
			if (tn->br.array[n].pt_layers == lall) size = 0.4;
			if (tn->br.array[n].coord_fixed) {
				rtrnd_line_new(ly, NULL, NULL, x-size, y, x+size, y, 0.07, 0);
				rtrnd_line_new(ly, NULL, NULL, x, y-size, x, y+size, 0.07, 0);
			}
			else {
				rtrnd_line_new(ly, NULL, NULL, x-size, y-size, x+size, y+size, 0.07, 0);
				rtrnd_line_new(ly, NULL, NULL, x-size, y+size, x+size, y-size, 0.07, 0);
			}
		}
		printf("\n");

		memcpy(&tn->cross, &cr, sizeof(cr));
		memset(&cr, 0, sizeof(cr));
	}
	vtcr_uninit(&cr);
}

/* assuming all branches are created, fill in br->cn_bridx */
static void laa_2net_cn_bridx(rtrnd_t *ctx, rtrnd_net_t *net)
{
	rt_topo_2nets_t *tns = NETDATA_LAA(net);
	rt_topo_laa_2net_t *tn;

	printf("cn_bridx %s:\n", net->hdr.oid);
	for(tn = tns->head; tn != NULL; tn = tn->next) {
		long n, i;
		for(n = 0; n < tn->cross.used; n++) {
			double offs = pt2offs(tn->cross.array[n].cn, tn->cross.array[n].x, tn->cross.array[n].y);

			if (tn->cross.array[n].cn->br.used <= 1)
				continue;

			if (offs < 1.0) {
				tn->cross.array[n].cn_bridx = -1;
				for(i = 1; i < tn->cross.array[n].cn->br.used; i++) {
					if (offs < tn->cross.array[n].cn->br.array[i].offs) {
						tn->cross.array[n].cn_bridx = i-1;
						break;
					}
				}
			}
			else
				tn->cross.array[n].cn_bridx = tn->cross.array[n].cn->br.used - 2;
			printf(" offs2=%f -> %d (self: %f)\n", offs, tn->cross.array[n].cn_bridx, tn->cross.array[n].offs);
			assert(tn->cross.array[n].cn_bridx < (tn->cross.array[n].cn->br.used-1));
			assert(tn->cross.array[n].cn_bridx != -1);
			assert(tn->cross.array[n].bridx >= 0);
			{ /* look up the pair of this crossing */
				tn->cross.array[n].cn_cr = NULL;
				for(i = 0; i < tn->cross.array[n].cn->cross.used; i++) {
					rtrnd_crossing_t *cr2 = &tn->cross.array[n].cn->cross.array[i];
					if (cr2->bridx == tn->cross.array[n].cn_bridx) {
						tn->cross.array[n].cn_cr = cr2;
						tn->cross.array[n].cn_cridx = i;
						break;
					}
				}
				assert(tn->cross.array[n].cn_cr != NULL);
			}
		}
	}
}

/* Main entry point, deals with the whole board */
static void laa_2vias(rtrnd_t *ctx, rtrnd_layer_t *ly, rtrnd_rtree_t *r2net)
{
	htsp_entry_t *e;

	/* create via sites on each 2net */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e))
		laa_2net_vias(ctx, e->value, ly, r2net);

	/* fill in cn_bridx */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e))
		laa_2net_cn_bridx(ctx, e->value);

}
