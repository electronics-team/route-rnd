#ifndef VT2BR_H
#define VT2BR_H

#include <stdlib.h>
#include <string.h>
#include "data.h"

typedef struct rtrnd_2branch_s rtrnd_2branch_t;

struct rtrnd_2branch_s {
	struct rt_topo_laa_2net_s *parent;
	double offs;               /* offset 0..1 from the the 2net's x1;y1 */
	double x, y;               /* starting point coords */
	long pt_layers;            /* which layers the starting point may access */
	unsigned coord_fixed:1;    /* can't be moved (terminal) */
	char edge_ly_fixed;        /* if >= 0, outgoing edge is fixed to a specific layer */
	int cridx;                 /* if >= 0, index into the crossing vector for the crossing that happens on the outgoing edge of this branch (there can be only one crossing) */

	/* temporary fields used for laa3 */
	rtrnd_2branch_t *found_next;
	rtrnd_via_t *via;          /* via placed in that point, or NULL; when via placed, real coords for RBE is x;y of the via, not the x;y of the branch! */
	void *ordinfo;             /* temp for laa2rbs ordering */
	unsigned found:1;
};

/* An actual assingment is a char[] where each byte corresponds to one
   rtrnd_2branch_t and determines which layer the next edge leaves on;
   -1 means free */

/* Elem=rtrnd_2branch_t; init=0
   Long int vector, all newly allocated bytes are set to 0 */

/* all public symbols are wrapped in GVT() - see vt_t(7) */
#define GVT(x) vt2br_ ## x

/* Array elem type - see vt_t(7) */
#define GVT_ELEM_TYPE rtrnd_2branch_t

/* Type that represents array lengths - see vt_t(7) */
#define GVT_SIZE_TYPE size_t

/* Below this length, always double allocation size when the array grows */
#define GVT_DOUBLING_THRS 128

/* Initial array size when the first element is written */
#define GVT_START_SIZE 8

/* Optional terminator; when present, it is always appended at the end - see
   vt_term(7)*/
/* #define GVT_TERM '\0' */

/* Optional prefix for function definitions (e.g. static inline) */
#define GVT_FUNC

/* Enable this to set all new bytes ever allocated to this value - see
   vt_set_new_bytes_to(7) */
#define GVT_SET_NEW_BYTES_TO 0

/* Enable GVT_INIT_ELEM_FUNC and an user configured function is called
   for each new element allocated (even between used and alloced).
   See vt_init_elem(7) */
/*#define GVT_INIT_ELEM_FUNC*/

/* Enable GVT_ELEM_CONSTRUCTOR and an user configured function is called
   for each element that is getting within the range of ->used.
   See vt_construction(7) */
/*#define GVT_ELEM_CONSTRUCTOR */

/* Enable GVT_ELEM_DESTRUCTOR and an user configured function is called
   for each element that was once constructed and now getting beyong ->used.
   See vt_construction(7) */
/*#define GVT_ELEM_DESTRUCTOR */

/* Enable GVT_ELEM_COPY and an user configured function is called
   for copying elements into the array.
   See vt_construction(7) */
/*#define GVT_ELEM_COPY */

/* Optional extra fields in the vector struct - see vt_user_fields(7) */
/* #define GVT_USER_FIELDS int foo; char bar[12]; */

/* An extra no_realloc field; when it is set to non-zero by the user, no
   realloc() is called (any attempt to grow the array fails) */
/* #define GVT_OPTIONAL_NO_REALLOC */

/* Include the actual header implementation */
#include <genvector/genvector_impl.h>

/* Memory allocator - see vt_allocation(7) */
#define GVT_REALLOC(vect, ptr, size)  realloc(ptr, size)
#define GVT_FREE(vect, ptr)           free(ptr)

/* clean up #defines */
#include <genvector/genvector_undef.h>

#endif
