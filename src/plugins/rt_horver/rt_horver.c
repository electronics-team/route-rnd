/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: simple grid based horizontal/vertical routing for 2 layer boards
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "data.h"
#include "util_grid.h"
#include "rt_horver.h"
#include "escape.h"
#include "optimize.h"
#include "bus.h"
#include "route_res.h"
#include "conf.h"


#define ERROR "route-rnd rt-horver error: "

/* settings */
double wire_thick, wire_clr, via_dia, via_clr;
int disable_dir[4];
static g2d_box_t brdbox;

static rtrnd_conf_t horver_cfg_desc[] = {
	RTRND_CONF_COORD("wire_thick",        0.25, 0.01, 10, "signal wire thickness", &wire_thick)
	RTRND_CONF_COORD("wire_clr",          0.25, 0.01, 10, "clearance around signal wire", &wire_clr)
	RTRND_CONF_COORD("via_dia",           1.6,  0.01, 10, "via copper ring outer diameter", &via_dia)
	RTRND_CONF_COORD("via_clr",           0.25, 0.01, 10, "clearance around via copper", &via_clr)
	RTRND_CONF_BOOLEAN("disable_left",    0,              "do not use the left side for buses", &disable_dir[0])
	RTRND_CONF_BOOLEAN("disable_right",   0,              "do not use the right side for buses", &disable_dir[1])
	RTRND_CONF_BOOLEAN("disable_top",     0,              "do not use the top side for buses", &disable_dir[2])
	RTRND_CONF_BOOLEAN("disable_bottom",  0,              "do not use the bottom side for buses", &disable_dir[3])
	RTRND_CONF_TERMINATE
};

/* states */
horver_t hvctx_ = {0}, *hvctx = &hvctx_;

double wirebox_minor_end(int is_major_x, int dirpol)
{
	if (is_major_x) {
		assert(dirpol != 0);
		if (dirpol > 0)
			return hvctx->wirebox.p2.y;
		return hvctx->wirebox.p1.y;
	}
	if (dirpol > 0)
		return hvctx->wirebox.p2.x;
	return hvctx->wirebox.p1.x;
}

/* figure best wire grid so wires can pass between pins, but don't let vias overlap */
static double adjust_grid_to_wire(double term_spacing, double via_spacing_max)
{
	double gap, num_wires, wspc;

	if (term_spacing < 0.01)
		return wire_thick+wire_clr;

	if (via_clr > wire_clr)
		gap = term_spacing - via_dia - via_clr*2;
	else
		gap = term_spacing - via_dia - wire_clr*2;

	num_wires = (gap + wire_clr) / (wire_thick + wire_clr) + 1;
	if (num_wires < 1) {
		rtrnd_error("horver wire grid: can't pass a wire between two terminals; make sure your terminals are aligned to a grid!");
		return wire_thick + wire_clr;
	}

	wspc = term_spacing / floor(num_wires);
/*printf("GAP: %f %f nw=%f -> %f\n", term_spacing, gap, num_wires, wspc);*/

	if (wspc < via_spacing_max) { /* but don't let wire-via get too close because they need to pass at buses */
		num_wires = floor(term_spacing / via_spacing_max);
		if (num_wires < 1)
			num_wires = 1;
		wspc = term_spacing / num_wires;
	}
	return wspc;
}

static void create_bus_(rtrnd_t *ctx, horver_t *hvctx, int grididx, int is_major_x, int dirpol, double spacing, int len)
{
	escape_dir_t dir = ESCAPE_DIR(is_major_x, dirpol);
	double first;
	long ridx;
	rtrnd_raline_t *rl;

	if (dirpol < 0)
		ridx = 0;
	else
		ridx = hvctx->grid[grididx].len - 1;

	rl = &hvctx->grid[grididx].raline[ridx];
	if (dirpol < 0)
		first = rl->major - spacing;
	else
		first = rl->major + spacing;

	bus_init(&hvctx->bus[dir], dirpol, first, spacing, len, rl->minor.array[0] - spacing, rl->minor.array[1] + spacing);
	bus_draw_grid(&hvctx->bus[dir], hvctx->ly_busgrid, !is_major_x);
}

static void do_bus(rtrnd_t *ctx, horver_t *hvctx, rtrnd_layer_t *ly)
{
	htsp_entry_t *e;

	printf("do_bus:\n");
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		escape_dir_t dir = ESCAPE_DIR(NETDATA_IS_MAJOR_X(net), NETDATA_DIR(net));
		rtrnd_netseg_t *ns;
		rtrnd_raline_t *busline;
		double ma_from, ma_to, ma, tune_via;
		int esced = 0;

		if (net->segments.length < 2) continue;

		/* calculate bus line span */
		printf(" net=%s\n", net->hdr.oid);
		for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
			escape_seg_t *eseg = NSDATA_ESEG(ns);
			if (eseg->chosen == -1) continue; /* did not find an escape */
			ma = eseg->poss[eseg->chosen].rl->major;
printf("  seg: %s %f\n", ns->hdr.oid, ma);
			if (esced) {
				if (ma < ma_from) ma_from = ma;
				if (ma > ma_to) ma_to = ma;
			}
			else
				ma_from = ma_to = ma;
			esced++;
		}
		if (esced < 2) continue; /* 0 or 1 segment escaped, no point in a bus */
		tune_via = via_dia/2 + via_clr;
		busline = bus_reserve(&hvctx->bus[dir], ma_from - tune_via, ma_to + tune_via);
		printf("  span=%f..%f on %f net is_major_x=%d net dir=%d\n", ma_from, ma_to, busline->major, (int)NETDATA_IS_MAJOR_X(net), (int)NETDATA_DIR(net));

		/* place the bus line */
		if (!NETDATA_IS_MAJOR_X(net))
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[!NETDATA_IS_MAJOR_X(net)], ly, net, busline->major, ma_from, busline->major, ma_to, wire_thick, wire_clr, 0.2, 0);
		else
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[!NETDATA_IS_MAJOR_X(net)], ly, net, ma_from, busline->major, ma_to, busline->major, wire_thick, wire_clr, 0.2, 0);

		/* adjust escape lines */
		for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
			double vx, vy;
			escape_seg_t *eseg = NSDATA_ESEG(ns);
			if (eseg->chosen == -1) continue; /* did not find an escape */
			eseg->poss[eseg->chosen].bus_minor = busline->major;
			escape_draw(ctx, hvctx, NETDATA_IS_MAJOR_X(net), NETDATA_DIR(net), ns, eseg->poss[eseg->chosen].rl);

			/* place the vias */
			if (NETDATA_IS_MAJOR_X(net)) {
				vy = eseg->poss[eseg->chosen].bus_minor;
				vx = eseg->poss[eseg->chosen].rl->major;
			}
			else {
				vx = eseg->poss[eseg->chosen].bus_minor;
				vy = eseg->poss[eseg->chosen].rl->major;
			}

			rtrnd_draw_res_via(ctx, hvctx->ly_copper[!NETDATA_IS_MAJOR_X(net)], net, vx, vy, via_dia, via_clr, 0.5, 0);
		}

	}
}

static void create_bus(rtrnd_t *ctx, horver_t *hvctx)
{
	double spacing = (wire_thick + via_dia)/2 + (via_clr > wire_clr ? via_clr : wire_clr);
#warning TODO: number of netsegs/2 oslt
	int len = 10;
	create_bus_(ctx, hvctx, 1, 0, -1, spacing, len);
	create_bus_(ctx, hvctx, 1, 0, +1, spacing, len);
	create_bus_(ctx, hvctx, 0, 1, -1, spacing, len);
	create_bus_(ctx, hvctx, 0, 1, +1, spacing, len);
}

static void destroy_bus(rtrnd_t *ctx, horver_t *hvctx)
{
	int n;
	for(n = 0; n < 4; n++)
		bus_uninit(&hvctx->bus[n]);
}

static int shuffle_collision(rtrnd_t *ctx, horver_t *hvctx)
{
	htsp_entry_t *e;
	long todo = 0;
	unsigned disable_bits = 0;
	int n;

	for(n = 0; n < 4; n++)
		if (disable_dir[n])
			disable_bits |= 1 << n;

	printf("Shuffle:\n");
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;

		if ((net->segments.length < 2) || (!NETDATA_FAILED(net)))
			continue; /* deal with routable, filed nets */

		if ((NETDATA_FAILDIRS(net) | disable_bits) != 0x0F) {
			NETDATA_FAILED_CLR(net);
			todo++;
			printf(" shuffling %s: %x %x\n", net->hdr.oid, (unsigned)NETDATA_FAILDIRS(net), disable_bits);
		}
	}

	if (todo == 0) {
		printf("shuffled all nets in all possible ways - there's no chance for any progress\n");
		return -1;
	}

	escape_uninit(ctx, hvctx);
	return 0;
}

static rtrnd_layer_t *find_layer(rtrnd_t *ctx, rtrnd_layer_loc_t loc)
{
	int n;
	for(n = 0; n < ctx->board->layers.used; n++) {
		rtrnd_layer_t *ly = ctx->board->layers.array[n];
		if (ly->loc == loc)
			return ly;
	}
	return NULL;
}

/* return the relevant box edge closer to the target bus in 1d, depending on
   direction polarity */
static double box_edge_by_pol(double small, double big, int dirpol)
{
	return (dirpol < 0) ? small : big;
}

long horver_map_obj(horver_map_cb_t cb, void *cbctx, rtrnd_ragrid_t *grid, rtrnd_any_obj_t *o, int is_major_x, int dirpol, int dry)
{
	double ma_from, ma_to, mi, mi2;
	rtrnd_raline_t *rl, *rlmax;
	int first;
	long cnt = 0, r;

	/* figure the (ascending) range of majors that spans the bbox of the
	   object, +1 major on each side */
	ma_from = is_major_x ? o->hdr.bbox.x1 : o->hdr.bbox.y1 ;
	ma_to = is_major_x ? o->hdr.bbox.x2 : o->hdr.bbox.y2;
	mi = is_major_x ? box_edge_by_pol(o->hdr.bbox.y1, o->hdr.bbox.y2, dirpol) : box_edge_by_pol(o->hdr.bbox.x1, o->hdr.bbox.x2, dirpol);

	rl = rtrnd_grid_find_major_before(grid, ma_from);
	if (rl == NULL)
		return -1;

	mi2 = wirebox_minor_end(is_major_x, dirpol);
	if (!dry)
		printf("  obj: ma:%f..%f\n", ma_from, ma_to);

	/* check each major within the range of majors for possible escape in
	   the specified direction */
	for(rlmax = &grid->raline[grid->len], first = 1; rl < rlmax; rl++, first = 0) {
		int within, ma_obj_found;
		double o_from, o_to, ma_obj, mi1;

		/* ignore the cutout on the raline if it was made by this specific object */
		within = (rtrnd_raline_obj_mask_size_at(rl, is_major_x, wire_thick, wire_clr, o, &o_from, &o_to) == 0);
		if (within)
			mi1 = (dirpol < 0) ? o_from : o_to;
		else
			mi1 = mi;

		/* determine the object-side end of the dog-leg */
		ma_obj_found = 0;
		if (first) { /* one grid before the object starts; dogleg steps one up */
			if (rl+1 < rlmax) {
				rtrnd_raline_obj_mask_size_at(rl+1, is_major_x, 0, 0, o, &o_from, &o_to);
				ma_obj = rl[1].major;
				ma_obj_found = 1;
			}
		}
		else if (rl->major >= ma_to) { /* one grid after the object ends; dogleg steps one down */
			if (rl-1 >= grid->raline) {
				rtrnd_raline_obj_mask_size_at(rl-1, is_major_x, 0, 0, o, &o_from, &o_to);
				ma_obj = rl[-1].major;
				ma_obj_found = 1;
			}
		}
		if (!ma_obj_found) { /* dogleg is on a mid point or was unable to step up/down */
			rtrnd_raline_obj_mask_size_at(rl, is_major_x, 0, 0, o, &o_from, &o_to);
			ma_obj = rl->major;
		}

		/* now we have the mid_point, the object anchor point at the end of the
		   dogleg; store it */
/*		printf("   at ma=%f:  obj_ma=%f  obj_mi=%f mi1=%f mi2=%f\n", rl->major, ma_obj, (o_from+o_to)/2, mi1, mi2);*/
		r = cb(cbctx, rl, is_major_x, dirpol, mi1, mi2,   ma_obj, (o_from+o_to)/2, dry);
		if (r > 0) cnt += r;
		if (!first && !within) break; /* this was a line effectively beyond the object's major */
		if (rl->major > ma_to) break;
	}

	return cnt;
}


static int route_horver(rtrnd_t *ctx)
{
	double x_orig, x_spacing, y_orig, y_spacing, via_spacing;
	long bad;

	if (rtrnd_grid_detect_terms(ctx->board, &x_orig, &x_spacing, &y_orig, &y_spacing) != 0) {
		fprintf(stderr, ERROR "failed to determine terminal grid\n");
		return -1;
	}

/*	printf("terminal grid 1: %f:%f %f:%f\n", x_spacing, x_orig, y_spacing, y_orig);*/

	via_spacing = via_dia/2 + wire_thick/2 + (via_clr > wire_clr ? via_clr : wire_clr);
	x_spacing = adjust_grid_to_wire(x_spacing, via_spacing);
	y_spacing = adjust_grid_to_wire(y_spacing, via_spacing);

/*	printf("terminal grid 2: %f:%f %f:%f\n", x_spacing, x_orig, y_spacing, y_orig);*/

	hvctx->ly_wiregrid = rtrnd_annot_new(ctx, "wiregrid");
	strcpy(hvctx->ly_wiregrid->color, "#333333");
	hvctx->ly_busgrid = rtrnd_annot_new(ctx, "busgrid");
	strcpy(hvctx->ly_busgrid->color, "#111199");
	hvctx->ly_escape[0] = rtrnd_annot_new(ctx, "escape");
	strcpy(hvctx->ly_escape[0]->color, "#AAAA00");
	hvctx->ly_escape[1] = rtrnd_annot_new(ctx, "escape");
	strcpy(hvctx->ly_escape[1]->color, "#AA00AA");

	rtrnd_board_bbox(&brdbox, ctx->board);
	hvctx->wirebox = brdbox;

	/* origin will shift it at most 1 grid up, make sure there are enough grids on the top */
	hvctx->wirebox.p1.x -= x_spacing;
	hvctx->wirebox.p1.y -= x_spacing;
	hvctx->wirebox.p2.x += x_spacing;
	hvctx->wirebox.p2.y += y_spacing;

	rtrnd_ragrid_init(&hvctx->grid[0], hvctx->wirebox.p1.y, hvctx->wirebox.p2.y, y_orig, y_spacing, hvctx->wirebox.p1.x, hvctx->wirebox.p2.x);
	rtrnd_grid_mask_objs(&hvctx->grid[0], 0, ctx->board, ctx->board->layers.array[0], wire_thick, wire_clr);
	rtrnd_ragrid_draw(&hvctx->grid[0], hvctx->ly_wiregrid, 0);
	hvctx->ly_copper[0] = find_layer(ctx, RTRND_LLOC_TOP);
	if (hvctx->ly_copper[0] == NULL) {
		fprintf(stderr, ERROR "No top copper layer available, can not route\n");
		return -1;
	}


	rtrnd_ragrid_init(&hvctx->grid[1], hvctx->wirebox.p1.x, hvctx->wirebox.p2.x, x_orig, x_spacing, hvctx->wirebox.p1.y, hvctx->wirebox.p2.y);
	rtrnd_grid_mask_objs(&hvctx->grid[1], 1, ctx->board, ctx->board->layers.array[1], wire_thick, wire_clr);
	rtrnd_ragrid_draw(&hvctx->grid[1], hvctx->ly_wiregrid, 1);
	hvctx->ly_copper[1] = find_layer(ctx, RTRND_LLOC_BOTTOM);
	if (hvctx->ly_copper[1] == NULL) {
		fprintf(stderr, ERROR "No bottom copper layer available, can not route\n");
		return -1;
	}


	create_bus(ctx, hvctx);

	opt1_noescape(ctx, hvctx);


	for(;;) {
		bad = escape(ctx, hvctx);
		if (bad == 0)
			break;

		printf("*** Net segs failed to escape: %ld\n", bad);
		if (shuffle_collision(ctx, hvctx) != 0) {
			printf("*** Failed to shuffle collisions, unrouted net segs remain\n");
			break;
		}
	}

	do_bus(ctx, hvctx, hvctx->ly_busgrid);
	escape_uninit(ctx, hvctx);
	destroy_bus(ctx, hvctx);

	rtrnd_ragrid_uninit(&hvctx->grid[0]);
	rtrnd_ragrid_uninit(&hvctx->grid[1]);
	return 0;
}

static const rtrnd_router_t rt_horver = {
	"horver", "2 layer, grid based, horizontal/vertical escape to orthogonal buses on the sides",
	horver_cfg_desc,
	route_horver
};

void rt_horver_init(void)
{
	vtp0_append(&rtrnd_all_router, (void *)&rt_horver);
}
