/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  tedax IO plugin - load and save files in the tEDAx format
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <genvector/vts0.h>
#include <genvector/vtd0.h>
#include <genvector/gds_char.h>
#include <genht/htsp.h>
#include <genht/htss.h>
#include <genht/hash.h>
#include <genht/ht_utils.h>
#include "data.h"
#include "parse.h"
#include "compat_misc.h"
#include "conf.h"

#define ERROR "route-rnd tEDAx error: "

typedef struct {
	rtrnd_t *ctx;
	FILE *f;
	char line[520], *argv[16];

	htsp_t stackups, layers, polylines;
	char *stackup_name;
} load_ctx_t;

#define TEDAX_GETLINE(lctx) \
	tedax_getline(lctx->f, lctx->line, sizeof(lctx->line), lctx->argv, (sizeof(lctx->argv)/sizeof(lctx->argv[0])))

#define setup_loader(ht, type, locvar, blockname, name,    namedup) \
do { \
	locvar = htsp_get(ht, name); \
	if (locvar != NULL) { \
		fprintf(stderr, ERROR "multiple " blockname " with the same name: %s\n", name); \
		return -1; \
	} \
	locvar = calloc(sizeof(type), 1); \
	htsp_set(ht, (char *)(namedup = rnd_strdup(name)), locvar); \
} while(0)

typedef struct {
	vts0_t layers;
	gds_t layer_locs;
	htss_t colors; /* layer name -> color string */
} stackup_t;

int tedax_load_stackup(load_ctx_t *lctx, const char *name)
{
	int argc;
	stackup_t *stackup;
	const char *namedup;

	setup_loader(&lctx->stackups, stackup_t, stackup, "stackup", name, namedup);
	htss_init(&stackup->colors, strhash, strkeyeq);

	while((argc = TEDAX_GETLINE(lctx)) >= 0) {
		if ((argc > 3) && (strcmp(lctx->argv[0], "layer") == 0) && (strcmp(lctx->argv[3], "copper") == 0)) {
			vts0_append(&stackup->layers, rnd_strdup(lctx->argv[1]));
			gds_append(&stackup->layer_locs, lctx->argv[2][0]);
		}
		if ((argc > 3) && (strcmp(lctx->argv[0], "lprop") == 0) && (strcmp(lctx->argv[2], "display-color") == 0)) {
			const char *clr = htss_get(&stackup->colors, lctx->argv[1]);
			if (clr == NULL) {
				char *s = lctx->argv[3];
				int len, valid = 1;
				if (*s != '#') valid = 0;
				for(s++, len = 0; *s != '\0'; s++,len++) {
					if (len >= 6) {
						valid = 0;
						break;
					}
					if (isdigit(*s) || ((*s >= 'a') && (*s <= 'f')) || ((*s >= 'A') && (*s <= 'F')))
						continue;
					valid = 0;
					break;
				}
				if (valid)
					htss_set(&stackup->colors, rnd_strdup(lctx->argv[1]), rnd_strdup(lctx->argv[3]));
				else
					fprintf(stderr, ERROR "invalid color '%s' ignored for layer %s in layerstack %s\n", lctx->argv[3], lctx->argv[1], namedup);
			}
			else
				fprintf(stderr, ERROR "duplicate color ignored for layer %s in layerstack %s\n", lctx->argv[1], namedup);
		}
		else if ((argc == 2) && (strcmp(lctx->argv[0], "end") == 0) && (strcmp(lctx->argv[1], "stackup") == 0))
			return 0;
	}
	fprintf(stderr, ERROR "unterminated stackup\n");
	return -1;
}

int tedax_load_polyline(load_ctx_t *lctx, const char *name)
{
	char *dname;
	vtd0_t *v;
	int argc;
	
	v = htsp_get(&lctx->polylines, name);
	if (v != NULL) {
		fprintf(stderr, ERROR "Duplicate polyline: '%s'\n", name);
		return -1;
	}
	
	v = calloc(sizeof(vtd0_t), 1);
	dname = rnd_strdup(name);
	htsp_set(&lctx->polylines, dname, v);

	while((argc = TEDAX_GETLINE(lctx)) >= 0) {
		if ((argc > 2) && (strcmp(lctx->argv[0], "v") == 0)) {
			char *end;
			double x, y;
			x = strtod(lctx->argv[1], &end);
			if (*end != '\0') {
				fprintf(stderr, ERROR "Invalid polyline x coord: '%s' in polyline '%s'\n", lctx->argv[1], dname);
				return -1;
			}
			y = strtod(lctx->argv[2], &end);
			if (*end != '\0') {
				fprintf(stderr, ERROR "Invalid polyline y coord: '%s' in polyline '%s'\n", lctx->argv[2], dname);
				return -1;
			}
			vtd0_append(v, x);
			vtd0_append(v, y);
		}
		else if ((argc == 2) && (strcmp(lctx->argv[0], "end") == 0) && (strcmp(lctx->argv[1], "polyline") == 0)) {
			if (v->used < 6) {
				fprintf(stderr, ERROR "Invalid polyline '%s': too few corners (%ld/2)\n", dname, v->used);
				return -1;
			}
			return 0;
		}
	}

	fprintf(stderr, ERROR "unterminated polyline '%s'\n", dname);
	return -1;
}


#define conv_coord(dst, src) \
do { \
	char *end; \
	dst = strtod(lctx->argv[src], &end); \
	if (*end != '\0') { \
		fprintf(stderr, ERROR "invalid coord '%s'\n", lctx->argv[src]); \
		return -1; \
	} \
} while(0)

#define conv_net(dst, src) \
do { \
	const char *netname = lctx->argv[src]; \
	if ((netname[0] == '-') && (netname[1] == '\0')) dst = NULL; \
	else { \
		dst = htsp_get(&lctx->ctx->board->nets, netname); \
		if (dst == NULL) { \
			dst = calloc(sizeof(rtrnd_net_t), 1); \
			dst->hdr.oid = rnd_strdup(netname); \
			htsp_set(&lctx->ctx->board->nets, dst->hdr.oid, dst); \
		} \
	} \
} while(0)

#define conv_constraints(dst, src) \
do { \
	const char *s; \
	for(s = lctx->argv[src]; *s != '\0'; s++) { \
		switch(*s) { \
			case 'd': dst->hdr.cnst_del = 1; break; \
			case 'm': dst->hdr.cnst_move = 1; break; \
			case 't': dst->hdr.terminal = 1; break; \
		} \
	} \
} while(0)

int tedax_load_layernet(load_ctx_t *lctx, const char *name)
{
	int argc;
	rtrnd_any_obj_t *obj;
	rtrnd_layer_t *layer;
	rtrnd_net_t *net;
	const char *namedup;

	setup_loader(&lctx->layers, rtrnd_layer_t, layer, "layernet", name, namedup);
	rtrnd_layer_init(layer, name);

	while((argc = TEDAX_GETLINE(lctx)) >= 0) {
		if ((argc > 9) && (strcmp(lctx->argv[0], "line") == 0)) {
			double x1, y1, x2, y2, thick, clr;

			conv_net(net, 2);
			conv_coord(x1, 4); conv_coord(y1, 5);
			conv_coord(x2, 6); conv_coord(y2, 7);
			conv_coord(thick, 8); conv_coord(clr, 9);

			obj = (rtrnd_any_obj_t *)rtrnd_line_new(layer, lctx->argv[1], net, x1, y1, x2, y2, thick, clr);

			conv_constraints(obj, 3);
		}
		else if ((argc > 10) && (strcmp(lctx->argv[0], "arc") == 0)) {
			double cx, cy, r, sa, da, thick, clr;

			conv_net(net, 2);
			conv_coord(cx, 4); conv_coord(cy, 5);
			conv_coord(r, 6);
			conv_coord(sa, 7); conv_coord(da, 8);
			conv_coord(thick, 9); conv_coord(clr, 10);

			sa = RTRND_DEG2RAD(180.0-sa);
			da = -RTRND_DEG2RAD(da);

			obj = (rtrnd_any_obj_t *)rtrnd_arc_new(layer, lctx->argv[1], net, cx, cy, r, sa, da, thick, clr);
		}
		else if ((argc > 3) && (strcmp(lctx->argv[0], "poly") == 0)) {
			double ox, oy;
			vtd0_t *v;

			v = htsp_get(&lctx->polylines, lctx->argv[4]);
			if (v == NULL) {
				fprintf(stderr, ERROR "invalid layernet poly %s: no such polyline defined\n", lctx->line);
				return -1;
			}

			conv_net(net, 2);
			conv_coord(ox, 5); conv_coord(oy, 6);

			obj = (rtrnd_any_obj_t *)rtrnd_poly_new_from_xys(layer, lctx->argv[1], net, v->array, v->used, ox, oy);

			conv_constraints(obj, 3);
		}
		else if ((argc == 2) && (strcmp(lctx->argv[0], "end") == 0) && (strcmp(lctx->argv[1], "layernet") == 0)) {
			return 0;
		}
		else {
			fprintf(stderr, ERROR "invalid layernet line '%s' in layernet '%s'\n", lctx->line, namedup);
			return -1;
		}
	}
	fprintf(stderr, ERROR "unterminated stackup %s\n", namedup);
	return -1;
}

int tedax_load_via(load_ctx_t *lctx)
{
	
	double x, y, dia, clr;
	rtrnd_any_obj_t *obj;
	rtrnd_net_t *net;

	conv_net(net, 2);
	conv_coord(x, 4); conv_coord(y, 5);
	conv_coord(dia, 6); conv_coord(clr, 7);

#warning	TODO: Ignores bbvia aspects

	obj = (rtrnd_any_obj_t *)rtrnd_via_new(lctx->ctx->board, lctx->argv[1], net, x, y, dia, clr);

	conv_constraints(obj, 3);
	return 0;
}


int tedax_load_route_req(load_ctx_t *lctx, const char *name)
{
	int argc;

	lctx->ctx->name = rnd_strdup(name);

	while((argc = TEDAX_GETLINE(lctx)) >= 0) {
		if ((argc > 1) && (strcmp(lctx->argv[0], "stackup") == 0)) {
			lctx->stackup_name = rnd_strdup(lctx->argv[1]);
		}
		else if (strcmp(lctx->argv[0], "via") == 0) {
			if (tedax_load_via(lctx) != 0)
				return -1;
		}
		else if ((argc > 0) && (strcmp(lctx->argv[0], "route_all") == 0)) {
		/* TODO */
		}
		else if ((argc > 0) && (strcmp(lctx->argv[0], "conf") == 0)) {
			if (rtrnd_conf_set(lctx->ctx->rt->conf, lctx->argv[1], lctx->argv[2]) != 0)
				fprintf(stderr, ERROR "invalid conf setting for '%s' (ignored)\n", lctx->argv[1]);
		}
		else if ((argc == 2) && (strcmp(lctx->argv[0], "end") == 0) && (strcmp(lctx->argv[1], "route_req") == 0)) {
			return 0;
		}
		else {
			fprintf(stderr, ERROR "invalid route_req line %s\n", lctx->line);
			return -1;
		}
	}

	return -1;
}

static int tedax_apply_stackup(load_ctx_t *lctx)
{
	stackup_t *istack;
	long n;

	printf("apply stackup %s\n", lctx->stackup_name);
	if (lctx->stackup_name == NULL) {
		fprintf(stderr, ERROR "invalid route_req: no stackup named\n");
		return -1;
	}

	istack = htsp_get(&lctx->stackups, lctx->stackup_name);
	if (istack == NULL) {
		fprintf(stderr, ERROR "invalid route_req: stackup '%s' is not defined\n", lctx->stackup_name);
		return -1;
	}


	for(n = 0; n < istack->layers.used; n++) {
		const char *layername = istack->layers.array[n], *clr;
		char loc = istack->layer_locs.array[n];
		htsp_entry_t *e = htsp_popentry(&lctx->layers, layername);
		rtrnd_layer_t *layer;
		if (e == NULL) {
			fprintf(stderr, ERROR "invalid stackup '%s': refers to non-existent copper layernet '%s'\n", lctx->stackup_name, layername);
			return -1;
		}
		free(e->key);
		layer = e->value;
		vtp0_append(&lctx->ctx->board->layers, layer);
		clr = htss_get(&istack->colors, layername);
		if (clr != NULL)
			strcpy(layer->color, clr);
		else
			strcpy(layer->color, "#990000");
		switch(loc) {
			case 't': layer->loc = RTRND_LLOC_TOP; break;
			case 'i': layer->loc = RTRND_LLOC_INTERN; break;
			case 'b': layer->loc = RTRND_LLOC_BOTTOM; break;
		}
	}

	return 0;
}

static int tedax_load(rtrnd_t *ctx, FILE *f)
{
	load_ctx_t lctx;
	int argc, res = 0;

	memset(&lctx, 0, sizeof(lctx));
	lctx.ctx = ctx;
	lctx.f = f;

	lctx.ctx->board = rtrnd_board_new();

	htsp_init(&lctx.stackups, strhash, strkeyeq);
	htsp_init(&lctx.layers, strhash, strkeyeq);
	htsp_init(&lctx.polylines, strhash, strkeyeq);

	while((argc = tedax_getline(f, lctx.line, sizeof(lctx.line), lctx.argv, sizeof(lctx.argv)/sizeof(lctx.argv[0]))) >= 0) {
		if (strcmp(lctx.argv[0], "begin") == 0) {
			if ((strcmp(lctx.argv[1], "stackup") == 0) && (tedax_load_stackup(&lctx, lctx.argv[3]) != 0)) {
				res = -1;
				break;
			}
			if ((strcmp(lctx.argv[1], "layernet") == 0) && (tedax_load_layernet(&lctx, lctx.argv[3]) != 0)) {
				res = -1;
				break;
			}
			if ((strcmp(lctx.argv[1], "polyline") == 0) && (tedax_load_polyline(&lctx, lctx.argv[3]) != 0)) {
				res = -1;
				break;
			}
			if ((strcmp(lctx.argv[1], "route_req") == 0) && (tedax_load_route_req(&lctx, lctx.argv[3]) != 0)) {
				res = -1;
				break;
			}
		}
	}

	if (res == 0)
		res = tedax_apply_stackup(&lctx);

	genht_uninit_deep(htsp, &lctx.stackups, { /* remove layers of unused stacks */
		int n;
		stackup_t *stackup = htent->value;
		for(n = 0; n < stackup->layers.used; n++)
			free(stackup->layers.array[n]);
		vts0_uninit(&stackup->layers);
		gds_uninit(&stackup->layer_locs);
		genht_uninit_deep(htss, &stackup->colors, { /* remove layer colors */
			free(htent->key);
			free(htent->value);
		});
		free(stackup);
		free(htent->key);
	});
	free(lctx.stackup_name);

	genht_uninit_deep(htsp, &lctx.polylines, {
		vtd0_t *v = htent->value;
		vtd0_uninit(v);
		free(v);
		free(htent->key);
	});

	genht_uninit_deep(htsp, &lctx.layers, {
		free(htent->key);
		free(htent->value);
	});

	return res;
}

static int tedax_save_begin(rtrnd_t *ctx, FILE *f)
{
	fprintf(f, "tEDAx v1\n");
	fprintf(f, "begin route_res v1 ");
	tedax_fprint_escape(f, ctx->name);
	fprintf(f, "\n");
	return 0;
}

static int tedax_save_add(rtrnd_t *ctx, FILE *f, const rtrnd_any_obj_t *obj)
{
	switch(obj->hdr.type) {
		case RTRND_LINE:
			fprintf(f, " add ");
			tedax_fprint_escape(f, obj->hdr.parent->layer.name);
			fprintf(f, " line ");
			tedax_fprint_escape(f, obj->hdr.oid);
			fprintf(f, " ");
			tedax_fprint_escape(f, obj->hdr.net == NULL ? "-" : obj->hdr.net->hdr.oid);
			fprintf(f, " %f %f %f %f  %f %f\n",
				obj->line.cline.p1.x, obj->line.cline.p1.y,
				obj->line.cline.p2.x, obj->line.cline.p2.y,
				obj->line.thickness, obj->line.clearance);
			return 0;

		case RTRND_VIA:
			fprintf(f, " add - via ");
			tedax_fprint_escape(f, obj->hdr.oid);
			fprintf(f, " ");
			tedax_fprint_escape(f, obj->hdr.net == NULL ? "-" : obj->hdr.net->hdr.oid);
			fprintf(f, " %f %f  %f %f\n",
				obj->via.x, obj->via.y,
				obj->via.dia, obj->via.clearance);

			return 0;

		case RTRND_ARC:
			fprintf(f, " add ");
			tedax_fprint_escape(f, obj->hdr.parent->layer.name);
			fprintf(f, " arc ");
			tedax_fprint_escape(f, obj->hdr.oid);
			fprintf(f, " ");
			tedax_fprint_escape(f, obj->hdr.net == NULL ? "-" : obj->hdr.net->hdr.oid);
			fprintf(f, " %f %f  %f %f %f  %f %f  %f %f  %f %f\n",
				obj->arc.carc.c.x, obj->arc.carc.c.y, obj->arc.carc.r,
				180.0-RTRND_RAD2DEG(obj->arc.carc.start), -RTRND_RAD2DEG(obj->arc.carc.delta),
				obj->arc.thickness, obj->arc.clearance,
				obj->arc.carc.c.x + cos(obj->arc.carc.start) * obj->arc.carc.r, obj->arc.carc.c.y + sin(obj->arc.carc.start) * obj->arc.carc.r,
				obj->arc.carc.c.x + cos(obj->arc.carc.start + obj->arc.carc.delta) * obj->arc.carc.r, obj->arc.carc.c.y + sin(obj->arc.carc.start + obj->arc.carc.delta) * obj->arc.carc.r);

			break;

		case RTRND_POLY:
#warning TODO
			break;

		case RTRND_BOARD:
		case RTRND_LAYER:
		case RTRND_NET:
		case RTRND_NETSEG:
			break;
	}
	return -1;
}

int tedax_save_log(rtrnd_t *ctx, FILE *f, char level, char *msg, int len)
{
	switch(level) {
		case 'I': case 'W': case 'E':
			if (len > 500)
				strcpy(msg+496, "...");
			fprintf(f, " log %c ", level);
			tedax_fprint_escape(f, msg);
			fprintf(f, " ");
			break;
	}
	return 0;
}

static int tedax_save_confkey(rtrnd_t *ctx, FILE *f, const rtrnd_conf_t *item)
{
	fprintf(f, " confkey ");
	tedax_fprint_escape(f, item->name);
	fprintf(f, " %s", rtrnd_conf_type2name(item->type));

	/* print default:min:max, per type */
	switch(item->type) {
		case RTRND_CT_BOOLEAN:
			fprintf(f, " %d", (int)item->defval.dval);
			break;
		case RTRND_CT_INTEGER:
			fprintf(f, " %ld", (long)item->defval.dval);
			if (item->min != RTRND_CONF_NOVAL) {
				fprintf(f, ":%ld", (long)item->min);
				if (item->max != RTRND_CONF_NOVAL)
					fprintf(f, ":%ld", (long)item->max);
			}
			break;
		case RTRND_CT_DOUBLE:
		case RTRND_CT_COORD:
			fprintf(f, " %f", item->defval.dval);
			if (item->min != RTRND_CONF_NOVAL) {
				fprintf(f, ":%f", item->min);
				if (item->max != RTRND_CONF_NOVAL)
					fprintf(f, ":%f", item->max);
			}
			break;
		case RTRND_CT_STRING:
			fprintf(f, " ");
			tedax_fprint_escape(f, item->defval.sval);
			break;
		case RTRND_CT_TERMINATOR:
		default:
			fprintf(f, " -");
			break;
	}

	/* print desc" */
	fprintf(f, " ");
	tedax_fprint_escape(f, item->desc);
	fprintf(f, "\n");

	return 0;
}

static int tedax_save_end(rtrnd_t *ctx, FILE *f)
{
	fprintf(f, "end route_res\n");
	return 0;
}


static const rtrnd_io_t io_tedax = {
	"tEDAx",
	rtrnd_tedax_test_parse,
	tedax_load,
	tedax_save_begin,
	tedax_save_add,
	tedax_save_log,
	tedax_save_confkey,
	tedax_save_end
};

void io_tedax_init(void)
{
	vtp0_append(&rtrnd_all_io, (void *)&io_tedax);
}
