/*
 *                            COPYRIGHT
 *
 *  pcb-rnd, interactive printed circuit board design
 *  (this file is based on PCB, interactive printed circuit board design)
 *  Copyright (C) 1994,1995,1996 Thomas Nau
 *  Copyright (C) 1998,1999,2000,2001 harry eaton
 *
 *  this file, autoroute.c, was written and is
 *  Copyright (c) 2001 C. Scott Ananian
 *  Copyright (c) 2006 harry eaton
 *  Copyright (c) 2009 harry eaton
 *
 *  Updated for pcb-rnd for subcircuits, padstacks and netlist
 *  Copyright (c) 2018,2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 *
 *
 *  Old contact info:
 *  harry eaton, 6697 Buttonhole Ct, Columbia, MD 21044 USA
 *  haceaton@aplcomm.jhuapl.edu
 *
 */

/*
 *-------------------------------------------------------------------
 * This file implements a rectangle-expansion router, based on
 * "A Method for Gridless Routing of Printed Circuit Boards" by
 * A. C. Finch, K. J. Mackenzie, G. J. Balsdon, and G. Symonds in the
 * 1985 Proceedings of the 22nd ACM/IEEE Design Automation Conference.
 * This reference is available from the ACM Digital Library at
 * http://www.acm.org/dl for those with institutional or personal
 * access to it.  It's also available from your local engineering
 * library.
 *
 * The code is much closer to what is described in the paper now,
 * in that expansion areas can grow from corners and in all directions
 * at once. Previously, these were emulated with discrete boxes moving
 * in the cardinal directions. With the new method, there are fewer but
 * larger expansion boxes that one might do a better job of routing in.
 *--------------------------------------------------------------------
 */
#define NET_HEAP 1
#include "config.h"

#include <assert.h>
#include <setjmp.h>

#include "data.h"
#include "heap.h"
#include "rnd_rtree.h"
#include "route_res.h"
#include "mtspace.h"
#include "vector.h"
#include "util_rat.h"
#include <genvector/vtp0.h>

#define ERROR   "route-rnd rt-hace error: "
#define WARNING "route-rnd rt-hace warning: "
#define INFO    "route-rnd rt-hace info: "

#define MAPPED(obj)           ((obj)->hdr.rt_data.l[0])

/* #defines to enable some debugging output */
/*
#define ROUTE_VERBOSE
*/

/*
#define ROUTE_DEBUG
//#define DEBUG_SHOW_ROUTE_BOXES
#define DEBUG_SHOW_EXPANSION_BOXES
//#define DEBUG_SHOW_EDGES
//#define DEBUG_SHOW_VIA_BOXES
#define DEBUG_SHOW_TARGETS
#define DEBUG_SHOW_SOURCES
//#define DEBUG_SHOW_ZIGZAG
*/

#warning TODO: check/set up these:
#define PCB_MAX_LAYERGRP 128
#define RND_MAX_COORD HUGE_VAL
#define RTRND_VOID 0
static rtrnd_board_t *PCB;

static double wire_thick, wire_clr, via_dia, via_clr;
static rtrnd_conf_t hace_cfg_desc[] = {
	RTRND_CONF_COORD("wire_thick",        0.25, 0.01, 10, "signal wire thickness", &wire_thick)
	RTRND_CONF_COORD("wire_clr",          0.25, 0.01, 10, "clearance around signal wire", &wire_clr)
	RTRND_CONF_COORD("via_dia",           1.6,  0.01, 10, "via copper ring outer diameter", &via_dia)
	RTRND_CONF_COORD("via_clr",           0.25, 0.01, 10, "clearance around via copper", &via_clr)
	RTRND_CONF_TERMINATE
};


typedef long rnd_layergrp_id_t;

typedef enum {
	RND_NORTH = 0, RND_EAST = 1, RND_SOUTH = 2, RND_WEST = 3,
	RND_NE = 4, RND_SE = 5, RND_SW = 6, RND_NW = 7, RND_ANY_DIR = 8
} rnd_direction_t;

static rnd_direction_t directionIncrement(rnd_direction_t dir)
{
	switch (dir) {
	case RND_NORTH:
		dir = RND_EAST;
		break;
	case RND_EAST:
		dir = RND_SOUTH;
		break;
	case RND_SOUTH:
		dir = RND_WEST;
		break;
	case RND_WEST:
		dir = RND_NE;
		break;
	case RND_NE:
		dir = RND_SE;
		break;
	case RND_SE:
		dir = RND_SW;
		break;
	case RND_SW:
		dir = RND_NW;
		break;
	case RND_NW:
		dir = RND_ANY_DIR;
		break;
	case RND_ANY_DIR:
		dir = RND_NORTH;
		break;
	}
	return dir;
}

#ifdef ROUTE_DEBUG
rnd_hid_t *ddraw = NULL;
static rnd_hid_gc_t ar_gc = 0;
#endif

#define EXPENSIVE 3e28
/* round up "half" thicknesses */
#define HALF_THICK(x) (((x)+1)/2)
/* a styles maximum bloat is its clearance plus the larger of its via radius
 * or line half-thickness. */
#define BLOAT(style)\
	((style)->Clearance + HALF_THICK((style)->Thickness))
/* conflict penalty is less for traces laid down during previous pass than
 * it is for traces already laid down in this pass. */
#define CONFLICT_LEVEL(rb)\
	(((rb)->flags.is_odd==AutoRouteParameters.is_odd) ?\
	 HI_CONFLICT : LO_CONFLICT )
#define CONFLICT_PENALTY(rb)\
	((CONFLICT_LEVEL(rb)==HI_CONFLICT ? \
	 AutoRouteParameters.ConflictPenalty : \
	 CONFLICT_LEVEL(rb)==LO_CONFLICT ? \
	 AutoRouteParameters.LastConflictPenalty : 1) * (rb)->pass)

#define _NORTH 1
#define _EAST 2
#define _SOUTH 4
#define _WEST 8

#define PCB_END_LOOP  }} while (0)

#define LIST_LOOP(init, which, x) do {\
     routebox_t *__next_one__ = (init);\
   x = NULL;\
   if (!__next_one__)\
     assert(__next_one__);\
   else\
   while (!x  || __next_one__ != (init)) {\
     x = __next_one__;\
     /* save next one first in case the command modifies or frees it */\
     __next_one__ = x->which.next

#define FOREACH_SUBNET(net, p) do {\
  routebox_t *_pp_;\
  /* iterate through *distinct* subnets */\
  LIST_LOOP(net, same_net, p); \
  if (!p->flags.subnet_processed) {\
    LIST_LOOP(p, same_subnet, _pp_);\
    _pp_->flags.subnet_processed=1;\
    PCB_END_LOOP
#define END_FOREACH(net, p) \
  }; \
  PCB_END_LOOP;\
  /* reset subnet_processed flags */\
  LIST_LOOP(net, same_net, p); \
  p->flags.subnet_processed=0;\
  PCB_END_LOOP;\
} while (0)
#define SWAP(t, f, s) do { t a=s; s=f; f=a; } while (0)
/* notes:
 * all rectangles are assumed to be closed on the top and left and
 * open on the bottom and right.   That is, they include their top-left
 * corner but don't include their bottom and right edges.
 *
 * expansion regions are always half-closed.  This means that when
 * tracing paths, you must steer clear of the bottom and right edges.,
 * because these are not actually in the allowed box.
 *
 * All routeboxes *except* EXPANSION_AREAS now have their "box" bloated by
 * their particular required clearance. This simplifies the tree searching.
 * the "sbox" contains the unbloated box.
 */

/* enumerated type for conflict levels */
typedef enum { NO_CONFLICT = 0, LO_CONFLICT = 1, HI_CONFLICT = 2 } conflict_t;

typedef struct routebox_s routebox_t;

typedef struct routebox_list_s {
	routebox_t *next, *prev;
} routebox_list_t;

typedef enum etype { TERM, VIA, VIA_SHADOW, LINE, OTHER, EXPANSION_AREA, PLANE, THERMAL } etype;

typedef struct {
	double Thickness, Clearance, Diameter;
} pcb_route_style_t;

struct routebox_s {
	rtrnd_rtree_box_t box, sbox;
	struct {
		double x1, y1, x2, y2;
	} line; /* exact coords of the line we are going to draw if type is line; reverse engineering these from the bounding box using halfthick and other hacks lead to rounding errors, a few LSB flicker in coords, e.g. breaking rubber band */
	union {
		rtrnd_via_t *via;
		rtrnd_any_obj_t *term;
		routebox_t *via_shadow;	/* points to the via in r-tree which
																	 * points to the rtrnd_via_t in the PCB. */
		rtrnd_line_t *line;
		void *generic;							/* 'other' is polygon, arc */
		routebox_t *expansion_area;	/* previous expansion area in search */
	} parent;
	unsigned short group;
	unsigned short layer;
	rtrnd_netseg_t *ns;
	etype type;
	struct {
		unsigned nonstraight:1;
		unsigned fixed:1;
		/* for searches */
		unsigned source:1;
		unsigned target:1;
		/* rects on same net as source and target don't need clearance areas */
		unsigned nobloat:1;
		/* mark circular pins, so that we be sure to connect them up properly */
		unsigned circular:1;
		/* we sometimes create routeboxen that don't actually belong to a
		 * r-tree yet -- make sure refcount of homelesss is set properly */
		unsigned homeless:1;
		/* was this nonfixed obstacle generated on an odd or even pass? */
		unsigned is_odd:1;
		/* fixed route boxes that have already been "routed through" in this
		 * search have their "touched" flag set. */
		unsigned touched:1;
		/* this is a status bit for iterating through *different* subnets */
		unsigned subnet_processed:1;
		/* some expansion_areas represent via candidates */
		unsigned is_via:1;
		/* mark non-straight lines which go from bottom-left to upper-right,
		 * instead of from upper-left to bottom-right. */
		unsigned bl_to_ur:1;
		/* mark polygons which are "transparent" for via-placement; that is,
		 * vias through the polygon will automatically be given a clearance
		 * and will not electrically connect to the polygon. */
		unsigned clear_poly:1;
		/* this marks "conflicting" routes that must be torn up to obtain
		 * a correct routing.  This flag allows us to return a correct routing
		 * even if the user cancels auto-route after a non-final pass. */
		unsigned is_bad:1;
		/* for assertion that 'box' is never changed after creation */
		unsigned inited:1;
		/* indicate this expansion ares is a thermal between the pin and plane */
		unsigned is_thermal;
	} flags;
	/* indicate the direction an expansion box came from */
	rnd_heap_cost_t cost;
	rnd_cheap_point_t cost_point;
	/* reference count for homeless routeboxes; free when refcount==0 */
	int refcount;
	/* when routing with conflicts, we keep a record of what we're
	 * conflicting with.
	 */
	vector_t *conflicts_with;
	/* route style of the net associated with this routebox */
	pcb_route_style_t  *style;
	/* congestion values for the edges of an expansion box */
	unsigned char n, e, s, w;
	/* what pass this this track was laid down on */
	unsigned char pass;
	/* the direction this came from, if any */
	rnd_direction_t came_from;
	/* circular lists with connectivity information. */
	routebox_list_t same_net, same_subnet, original_subnet, different_net;
};

typedef struct routedata_s {
	rtrnd_t *ctx;

	/* one rtree per layer *group */
	rtrnd_rtree_t *layergrouptree[PCB_MAX_LAYERGRP];	/* no silkscreen layers here =) */
	rtrnd_layer_t *annot[PCB_MAX_LAYERGRP];
	rtrnd_layer_t *annot_via, *annot_rats;
	/* root pointer into connectivity information */
	routebox_t *first_net;
	/* default routing style */
	pcb_route_style_t defaultstyle;
	/* what is the maximum bloat (clearance+line half-width or
	 * clearance+via_radius) for any style we've seen? */
	double max_bloat;
	double max_keep;
	mtspace_t *mtspace;
	rtrnd_ratsnest_t nest;
} routedata_t;

typedef struct edge_struct_s {
	routebox_t *rb;								/* path expansion edges are real routeboxen. */
	rnd_cheap_point_t cost_point;
	rnd_heap_cost_t pcb_cost_to_point;					/* from source */
	rnd_heap_cost_t cost;									/* cached edge cost */
	routebox_t *minpcb_cost_target;		/* minimum cost from cost_point to any target */
	vetting_t *work;							/* for via search edges */
	rnd_direction_t expand_dir;
	struct {
		/* this indicates that this 'edge' is a via candidate. */
		unsigned is_via:1;
		/* record "conflict level" of via candidates, in case we need to split
		 * them later. */
		unsigned via_conflict_level:2; /* conflict_t */
		/* when "routing with conflicts", sometimes edge is interior. */
		unsigned is_interior:1;
		/* this is a fake edge used to defer searching for via spaces */
		unsigned via_search:1;
		/* this is a via edge in a plane where the cost point moves for free */
		unsigned in_plane:1;
	} flags;
} edge_t;

static struct AutoRouteParameters_s {
	/* net style parameters */
	pcb_route_style_t *style;
	/* the present bloat */
	double bloat;
	/* cost parameters */
	rnd_heap_cost_t ViaCost,								/* additional "length" cost for using a via */
	  LastConflictPenalty,				/* length mult. for routing over last pass' trace */
	  ConflictPenalty,						/* length multiplier for routing over another trace */
	  JogPenalty,									/* additional "length" cost for changing direction */
	  CongestionPenalty,					/* (rational) length multiplier for routing in */
	  NewLayerPenalty,						/* penalty for routing on a previously unused layer */
	  MinPenalty;									/* smallest Direction Penalty */
	/* maximum conflict incidence before calling it "no path found" */
	int hi_conflict;
	/* are vias allowed? */
	rtrnd_bool_t use_vias;
	/* is this an odd or even pass? */
	rtrnd_bool_t is_odd;
	/* permit conflicts? */
	rtrnd_bool_t with_conflicts;
	/* is this a final "smoothing" pass? */
	rtrnd_bool_t is_smoothing;
	/* rip up nets regardless of conflicts? */
	rtrnd_bool_t rip_always;
	rtrnd_bool_t last_smooth;
	unsigned char pass;
} AutoRouteParameters;

typedef struct routeone_state_s {
	/* heap of all candidate expansion edges */
	rnd_heap_t *workheap;
	/* information about the best path found so far. */
	routebox_t *best_path, *best_target;
	rnd_heap_cost_t best_cost;
} routeone_state_t;


static routebox_t *CreateExpansionArea(const rtrnd_rtree_box_t * area, long group,
																			 routebox_t * parent, rtrnd_bool_t relax_edge_requirements, edge_t * edge);

static rnd_heap_cost_t edge_cost(const edge_t * e, const rnd_heap_cost_t too_big);
static void best_path_candidate(routeone_state_t *s, edge_t * e, routebox_t * best_target);

static rtrnd_rtree_box_t edge_to_box(const routebox_t * rb, rnd_direction_t expand_dir);

static void add_or_destroy_edge(routeone_state_t *s, edge_t * e);

static void
RD_DrawThermal(routedata_t * rd, double X, double Y, long group, long layer, routebox_t * subnet, rtrnd_bool_t is_bad);
static void ResetSubnet(routebox_t * net);
#ifdef ROUTE_DEBUG
static int showboxen = -2;
static int aabort = 0;
static void showroutebox(routebox_t * rb);
#endif

/* group number of groups that hold surface mount pads */
static rnd_layergrp_id_t front, back;
static rtrnd_bool_t usedGroup[PCB_MAX_LAYERGRP];
static int x_cost[PCB_MAX_LAYERGRP], y_cost[PCB_MAX_LAYERGRP];
static rtrnd_bool_t is_layer_group_active[PCB_MAX_LAYERGRP];
static int ro = 0;
static int smoothes = 1;
static int passes = 12;
static int routing_layers = 0;
static float total_wire_length = 0;
static int total_via_count = 0;

RTRND_INLINE long pcb_max_group(rtrnd_board_t *pcb)
{
	return pcb->layers.used;
}

/* assertion helper for routeboxen */
#ifndef NDEBUG
static int __routepcb_box_is_good(routebox_t * rb)
{
	assert(rb && (rb->group < pcb_max_group(PCB)) &&
				 (rb->box.x1 <= rb->box.x2) && (rb->box.y1 <= rb->box.y2) &&
				 (rb->flags.homeless ?
					(rb->box.x1 != rb->box.x2) || (rb->box.y1 != rb->box.y2) : (rb->box.x1 != rb->box.x2) && (rb->box.y1 != rb->box.y2)));
	assert((rb->flags.source ? rb->flags.nobloat : 1) &&
				 (rb->flags.target ? rb->flags.nobloat : 1) &&
				 (rb->flags.homeless ? !rb->flags.touched : rb->refcount == 0) && (rb->flags.touched ? rb->type != EXPANSION_AREA : 1));
	assert((rb->flags.is_odd ? (!rb->flags.fixed) &&
					(rb->type == VIA || rb->type == VIA_SHADOW || rb->type == LINE || rb->type == PLANE) : 1));
	assert(rb->flags.clear_poly ? ((rb->type == OTHER || rb->type == PLANE) && rb->flags.fixed && !rb->flags.homeless) : 1);
	assert(rb->flags.inited);
/* run through conflict list showing none are homeless, targets or sources */
	if (rb->conflicts_with) {
		int i;
		for (i = 0; i < vector_size(rb->conflicts_with); i++) {
			routebox_t *c = vector_element(rb->conflicts_with, i);
			assert(!c->flags.homeless && !c->flags.source && !c->flags.target && !c->flags.fixed);
		}
	}
	assert(rb->style != NULL && rb->style != NULL);
	assert(rb->type == EXPANSION_AREA
				 || (rb->same_net.next && rb->same_net.prev && rb->same_subnet.next
						 && rb->same_subnet.prev && rb->original_subnet.next
						 && rb->original_subnet.prev && rb->different_net.next && rb->different_net.prev));
	return 1;
}

static int __edge_is_good(edge_t * e)
{
	assert(e && e->rb && __routepcb_box_is_good(e->rb));
	assert((e->rb->flags.homeless ? e->rb->refcount > 0 : 1));
	assert((0 <= e->expand_dir) && (e->expand_dir < 9)
				 && (e->flags.is_interior ? (e->expand_dir == RND_ANY_DIR && e->rb->conflicts_with) : 1));
	assert((e->flags.is_via ? e->rb->flags.is_via : 1)
				 && (e->flags.via_conflict_level >= 0 && e->flags.via_conflict_level <= 2)
				 && (e->flags.via_conflict_level != 0 ? e->flags.is_via : 1));
	assert((e->pcb_cost_to_point >= 0) && e->cost >= 0);
	return 1;
}

int no_planes(const rtrnd_rtree_box_t * b, void *cl)
{
	routebox_t *rb = (routebox_t *) b;
	if (rb->type == PLANE)
		return 0;
	return 1;
}
#endif /* !NDEBUG */

/*---------------------------------------------------------------------
 * route utility functions.
 */

enum boxlist { NET, SUBNET, ORIGINAL, DIFFERENT_NET };
static routebox_list_t *__select_list(routebox_t * r, enum boxlist which)
{
	assert(r);
	switch (which) {
	default:
		assert(0);
	case NET:
		return &(r->same_net);
	case SUBNET:
		return &(r->same_subnet);
	case ORIGINAL:
		return &(r->original_subnet);
	case DIFFERENT_NET:
		return &(r->different_net);
	}
}

static void InitLists(routebox_t * r)
{
	static enum boxlist all[] = { NET, SUBNET, ORIGINAL, DIFFERENT_NET }
	, *p;
	for (p = all; p < all + (sizeof(all) / sizeof(*p)); p++) {
		routebox_list_t *rl = __select_list(r, *p);
		rl->prev = rl->next = r;
	}
}

static void MergeNets(routebox_t * a, routebox_t * b, enum boxlist which)
{
	routebox_list_t *al, *bl, *anl, *bnl;
	routebox_t *an, *bn;
	assert(a && b);
	assert(a != b);
	assert(a->type != EXPANSION_AREA);
	assert(b->type != EXPANSION_AREA);
	al = __select_list(a, which);
	bl = __select_list(b, which);
	assert(al && bl);
	an = al->next;
	bn = bl->next;
	assert(an && bn);
	anl = __select_list(an, which);
	bnl = __select_list(bn, which);
	assert(anl && bnl);
	bl->next = an;
	anl->prev = b;
	al->next = bn;
	bnl->prev = a;
}

static void RemoveFromNet(routebox_t * a, enum boxlist which)
{
	routebox_list_t *al, *anl, *apl;
	routebox_t *an, *ap;
	assert(a);
	al = __select_list(a, which);
	assert(al);
	an = al->next;
	ap = al->prev;
	if (an == a || ap == a)
		return;											/* not on any list */
	assert(an && ap);
	anl = __select_list(an, which);
	apl = __select_list(ap, which);
	assert(anl && apl);
	anl->prev = ap;
	apl->next = an;
	al->next = al->prev = a;
	return;
}

static void init_const_box(routebox_t * rb, double X1, double Y1, double X2, double Y2, double clearance)
{
	rtrnd_rtree_box_t *bp = (rtrnd_rtree_box_t *) & rb->box;	/* note discarding const! */
	assert(!rb->flags.inited);
	assert(X1 <= X2 && Y1 <= Y2);
	assert(clearance >= 0);
	bp->x1 = X1 - clearance;
	bp->y1 = Y1 - clearance;
	bp->x2 = X2 + clearance;
	bp->y2 = Y2 + clearance;
	bp = (rtrnd_rtree_box_t *) & rb->sbox;
	bp->x1 = X1;
	bp->y1 = Y1;
	bp->x2 = X2;
	bp->y2 = Y2;
	if (bp->x1 == bp->x2) bp->x2 += DELTA;
	if (bp->y1 == bp->y2) bp->y2 += DELTA;
	rb->flags.inited = 1;
}

static inline rtrnd_rtree_box_t shrink_routebox(const routebox_t * rb)
{
	return rb->sbox;
}

static inline rnd_heap_cost_t box_area(const rtrnd_rtree_box_t b)
{
	rnd_heap_cost_t ans = b.x2 - b.x1;
	return ans * (b.y2 - b.y1);
}

static inline rnd_cheap_point_t closest_point_in_routebox(const rnd_cheap_point_t * from, const routebox_t * rb)
{
	return rnd_closest_cheap_point_in_box(from, &rb->sbox);
}

static inline rtrnd_bool_t point_in_shrunk_box(const routebox_t * box, double X, double Y)
{
	rtrnd_rtree_box_t b = shrink_routebox(box);
	return rnd_point_in_box(&b, X, Y);
}

static int layer_id(rtrnd_layer_t *layer)
{
	long n;
	for(n = 0; n < PCB->layers.used; n++)
		if (PCB->layers.array[n] == layer)
			return n;
	return -1;
}

RTRND_INLINE double obj_clearance_at(rtrnd_board_t *board, rtrnd_any_obj_t *obj, rtrnd_layer_t *layer)
{
	switch(obj->hdr.type) {
		case RTRND_LINE: return obj->line.clearance;
		case RTRND_ARC:  return obj->arc.clearance;
		case RTRND_POLY: return 0;
		case RTRND_VIA:
#warning TODO: bbvia
			return obj->via.clearance;
		default:
			return 0;
	}
}

/*---------------------------------------------------------------------
 * routedata initialization functions.
 */
static routebox_t *AddTerm_(vtp0_t layergroupboxes[], rtrnd_any_obj_t *term, pcb_route_style_t *style, rtrnd_layer_t *layer)
{
	routebox_t **rbpp;
	int layergroup = -1;
	double clr;

	if ((layer->loc != RTRND_LLOC_TOP) && (layer->loc != RTRND_LLOC_BOTTOM)) {
		if (term->hdr.type != RTRND_VIA) {
			/*fprintf(stderr, WARNING "trying to route to SMD pad on internal layer");*/
			/* silenlty ignore internal pads of a padstack */
			return NULL;
		}
	}
	
	layergroup = layer_id(layer);
	assert(layergroup >= 0);

	rbpp = (routebox_t **)vtp0_alloc_append(&layergroupboxes[layergroup], 1);
	assert(rbpp);
	*rbpp = (routebox_t *)calloc(sizeof(**rbpp), 1);
	assert(*rbpp);
	(*rbpp)->group = layergroup;
	clr = obj_clearance_at(PCB, term, layer);
	init_const_box(*rbpp,
								 /*X1 */ term->hdr.bbox.x1,
								 /*Y1 */ term->hdr.bbox.y1,
								 /*X2 */ term->hdr.bbox.x2,
								 /*Y2 */ term->hdr.bbox.y2,
								 style->Clearance);
	/* kludge for non-manhattan pads (which are not allowed at present) */
#warning old-TODO term
/*
	if (pad->cline.p1.x != pad->cline.p2.x && pad->cline.p1.y != pad->cline.p2.y)
		(*rbpp)->flags.nonstraight = 1;
*/
	(*rbpp)->flags.nonstraight = 0; /* otherwise the autorouter just ignores it */
	/* set aux. properties */
	(*rbpp)->type = TERM;
	(*rbpp)->parent.term = term;
	(*rbpp)->flags.fixed = 1;
	(*rbpp)->came_from = RND_ANY_DIR;
	(*rbpp)->style = style;
	/* circular lists */
	InitLists(*rbpp);
	return *rbpp;
}

static routebox_t *AddTerm(vtp0_t layergroupboxes[], rtrnd_any_obj_t *term, pcb_route_style_t *style)
{
	assert(term->hdr.parent->hdr.type == RTRND_LAYER);
	return AddTerm_(layergroupboxes, term, style, &term->hdr.parent->layer);
}

static routebox_t *AddVia(vtp0_t layergroupboxes[], rtrnd_via_t *ps, pcb_route_style_t *style)
{
	int i;
	routebox_t *r, *last = NULL;

	for (i = 0; i < PCB->layers.used; i++) {
		rtrnd_layer_t *ly = PCB->layers.array[i];
		if (rtrnd_via_touches_layer(ly, ps)) {
			r = AddTerm_(layergroupboxes, (rtrnd_any_obj_t *)ps, style, ly);
			if (r != NULL) {
				if (last != NULL) {
					MergeNets(r, last, NET);
					MergeNets(r, last, SUBNET);
					MergeNets(r, last, ORIGINAL);
				}
				last = r;
			}
		}
	}

	return last;
}


static routebox_t *AddLine(vtp0_t layergroupboxes[], int layergroup, rtrnd_line_t *line,
													 rtrnd_line_t *ptr, pcb_route_style_t * style)
{
	routebox_t **rbpp;
	assert(layergroupboxes && line);
	assert(0 <= layergroup && layergroup < pcb_max_group(PCB));

	rbpp = (routebox_t **) vtp0_alloc_append(&layergroupboxes[layergroup], 1);
	*rbpp = (routebox_t *) malloc(sizeof(**rbpp));
	memset(*rbpp, 0, sizeof(**rbpp));
	(*rbpp)->group = layergroup;
	init_const_box(*rbpp,
								 /*X1 */ RND_MIN(line->cline.p1.x,
														 line->cline.p2.x) - HALF_THICK(line->thickness),
								 /*Y1 */ RND_MIN(line->cline.p1.y,
														 line->cline.p2.y) - HALF_THICK(line->thickness),
								 /*X2 */ RND_MAX(line->cline.p1.x,
														 line->cline.p2.x) + HALF_THICK(line->thickness),
								 /*Y2 */ RND_MAX(line->cline.p1.y,
														 line->cline.p2.y) + HALF_THICK(line->thickness), style->Clearance);
	/* kludge for non-manhattan lines */
	if (line->cline.p1.x != line->cline.p2.x && line->cline.p1.y != line->cline.p2.y) {
		(*rbpp)->flags.nonstraight = 1;
		(*rbpp)->flags.bl_to_ur =
			(RND_MIN(line->cline.p1.x, line->cline.p2.x) == line->cline.p1.x) != (RND_MIN(line->cline.p1.y, line->cline.p2.y) == line->cline.p1.y);
#if defined(ROUTE_DEBUG) && defined(DEBUG_SHOW_ZIGZAG)
		showroutebox(*rbpp);
#endif
	}
	/* set aux. properties */
	(*rbpp)->type = LINE;
	(*rbpp)->line.x1 = line->cline.p1.x;
	(*rbpp)->line.y1 = line->cline.p1.y;
	(*rbpp)->line.x2 = line->cline.p2.x;
	(*rbpp)->line.y2 = line->cline.p2.y;
	(*rbpp)->parent.line = ptr;
	(*rbpp)->flags.fixed = 1;
	(*rbpp)->came_from = RND_ANY_DIR;
	(*rbpp)->style = style;
	/* circular lists */
	InitLists(*rbpp);
	return *rbpp;
}

static routebox_t *AddIrregularObstacle(vtp0_t layergroupboxes[],
																				double X1, double Y1,
																				double X2, double Y2, long layergroup, void *parent, pcb_route_style_t * style)
{
	routebox_t **rbpp;
	double keep = style->Clearance;
	assert(layergroupboxes && parent);
	assert(X1 <= X2 && Y1 <= Y2);
	assert(0 <= layergroup && layergroup < PCB->layers.used);

	rbpp = (routebox_t **) vtp0_alloc_append(&layergroupboxes[layergroup], 1);
	*rbpp = (routebox_t *) malloc(sizeof(**rbpp));
	memset(*rbpp, 0, sizeof(**rbpp));
	(*rbpp)->group = layergroup;
	init_const_box(*rbpp, X1, Y1, X2, Y2, keep);
	(*rbpp)->flags.nonstraight = 1;
	(*rbpp)->type = OTHER;
	(*rbpp)->parent.generic = parent;
	(*rbpp)->flags.fixed = 1;
	(*rbpp)->style = style;
	/* circular lists */
	InitLists(*rbpp);
	return *rbpp;
}

static routebox_t *AddPolygon(vtp0_t layergroupboxes[], long layer, rtrnd_poly_t *polygon, pcb_route_style_t * style)
{
	int is_not_rectangle = 1;
	rnd_layergrp_id_t layergroup = layer;
	routebox_t *rb;
	assert(0 <= layergroup && layergroup < pcb_max_group(PCB));
	rb = AddIrregularObstacle(layergroupboxes,
														polygon->hdr.bbox.x1,
														polygon->hdr.bbox.y1,
														polygon->hdr.bbox.x2, polygon->hdr.bbox.y2, layergroup, polygon, style);
	if (polygon->rtpoly.lst.length == 4) { /* common case: check if a 4-corner polygon is an axis aligned rectangle */
		double X[4], Y[4];
		int n;
		rtp_vertex_t *v;
		for(n = 0, v = gdl_first(&polygon->rtpoly.lst); v != NULL; v = v->link.next, n++) {
			X[n] = v->x;
			Y[n] = v->y;
		}
		if ((X[0] == X[1] || Y[0] == Y[1]) &&
		    (X[1] == X[2] || Y[1] == Y[2]) &&
		    (X[2] == X[3] || Y[2] == Y[3]) &&
		    (X[3] == X[0] || Y[3] == Y[0]))
			is_not_rectangle = 0;
	}
	rb->flags.nonstraight = is_not_rectangle;
	rb->layer = layer;
	rb->came_from = RND_ANY_DIR;
#warning TODO: at the moment any poly is a clearing poly
	/*if (PCB_FLAG_TEST(PCB_FLAG_CLEARPOLY, polygon))*/ {
		rb->flags.clear_poly = 1;
		if (!is_not_rectangle)
			rb->type = PLANE;
	}
	return rb;
}

static routebox_t *AddArc(vtp0_t layergroupboxes[], long layergroup, rtrnd_arc_t *arc, pcb_route_style_t * style)
{
	return AddIrregularObstacle(layergroupboxes,
															arc->hdr.bbox.x1, arc->hdr.bbox.y1,
															arc->hdr.bbox.x2, arc->hdr.bbox.y2, layergroup, arc, style);
}

struct rb_info {
	rtrnd_rtree_box_t query;
	routebox_t *winner;
	jmp_buf env;
};

static rnd_r_dir_t __found_one_on_lg(const rtrnd_rtree_box_t * box, void *cl)
{
	struct rb_info *inf = (struct rb_info *) cl;
	routebox_t *rb = (routebox_t *) box;
	rtrnd_rtree_box_t sb;

	if (rb->flags.nonstraight)
		return RND_R_DIR_NOT_FOUND;
	sb = rnd_shrink_box(&rb->box, rb->style->Clearance);
	if (inf->query.x1 >= sb.x2 || inf->query.x2 <= sb.x1 || inf->query.y1 >= sb.y2 || inf->query.y2 <= sb.y1)
		return RND_R_DIR_NOT_FOUND;
	inf->winner = rb;
	if (rb->type == PLANE)
		return RND_R_DIR_FOUND_CONTINUE;										/* keep looking for something smaller if a plane was found */
	longjmp(inf->env, 1);
	return RND_R_DIR_NOT_FOUND;
}

static routebox_t *FindRouteBoxOnLayerGroup(routedata_t * rd, double X, double Y, long layergroup)
{
	struct rb_info info;
	info.winner = NULL;
	info.query = rnd_point_box(X, Y);
	if (setjmp(info.env) == 0)
		rnd_r_search(rd->layergrouptree[layergroup], &info.query, NULL, __found_one_on_lg, &info, NULL);
	return info.winner;
}

#ifdef ROUTE_DEBUG_VERBOSE
static void DumpRouteBox(routebox_t * rb)
{
	fprintf(stderr, INFO "RB: %f;%f-%%f;%f l%d; ", rb->box.x1, rb->box.y1, rb->box.x2, rb->box.y2, (int) rb->group);
	switch (rb->type) {
	case TERM:
		fprintf(stderr, INFO "TERM[%s] ", rb->parent.term->term);
		break;
	case LINE:
		fprintf(stderr, INFO "LINE ");
		break;
	case OTHER:
		fprintf(stderr, INFO "OTHER ");
		break;
	case EXPANSION_AREA:
		fprintf(stderr, INFO "EXPAREA ");
		break;
	default:
		fprintf(stderr, INFO "UNKNOWN ");
		break;
	}
	if (rb->flags.nonstraight)
		fprintf(stderr, INFO "(nonstraight) ");
	if (rb->flags.fixed)
		fprintf(stderr, INFO "(fixed) ");
	if (rb->flags.source)
		fprintf(stderr, INFO "(source) ");
	if (rb->flags.target)
		fprintf(stderr, INFO "(target) ");
	if (rb->flags.homeless)
		fprintf(stderr, INFO "(homeless) ");
	printf("\n");
}
#endif

static routebox_t *crd_add_line(routedata_t *rd, vtp0_t *layergroupboxes, rnd_layergrp_id_t group, rtrnd_any_obj_t *obj, routebox_t **last_in_net, routebox_t **last_in_subnet)
{
	routebox_t *rb = NULL;
	rtrnd_line_t *line = (rtrnd_line_t *) obj;

	/* dice up non-straight lines into many tiny obstacles */
	if (line->cline.p1.x != line->cline.p2.x && line->cline.p1.y != line->cline.p2.y) {
		rtrnd_line_t fake_line = *line;
		double dx = (line->cline.p2.x - line->cline.p1.x);
		double dy = (line->cline.p2.y - line->cline.p1.y);
		int segs = RND_MAX(fabs(dx), fabs(dy)) / (4 * BLOAT(&rd->defaultstyle) + 1);
		int qq;
		segs = RND_CLAMP(segs, 1, 32); /* don't go too crazy */
		dx /= segs;
		dy /= segs;
		for (qq = 0; qq < segs - 1; qq++) {
			fake_line.cline.p2.x = fake_line.cline.p1.x + dx;
			fake_line.cline.p2.y = fake_line.cline.p1.y + dy;
			if (fake_line.cline.p2.x == line->cline.p2.x && fake_line.cline.p2.y == line->cline.p2.y)
				break;
			rb = AddLine(layergroupboxes, group, &fake_line, line, &rd->defaultstyle);
			if (*last_in_subnet && rb != *last_in_subnet)
				MergeNets(*last_in_subnet, rb, ORIGINAL);
			if (*last_in_net && rb != *last_in_net)
				MergeNets(*last_in_net, rb, NET);
			*last_in_subnet = *last_in_net = rb;
			fake_line.cline.p1 = fake_line.cline.p2;
		}
		fake_line.cline.p2 = line->cline.p2;
		rb = AddLine(layergroupboxes, group, &fake_line, line, &rd->defaultstyle);
	}
	else
		rb = AddLine(layergroupboxes, group, line, line, &rd->defaultstyle);

	return rb;
}

static routebox_t *crd_add_misc(routedata_t *rd, vtp0_t *layergroupboxes, rtrnd_any_obj_t *obj)
{
	routebox_t *rb = NULL;

	switch (obj->hdr.type) {
	case RTRND_VIA:
		rb = AddVia(layergroupboxes, (rtrnd_via_t *)obj, &rd->defaultstyle);
		break;
	case RTRND_POLY:
		{
			rtrnd_poly_t *poly = (rtrnd_poly_t *)obj;
			rtrnd_layer_t *layer = &obj->hdr.parent->layer;
			if (poly->hdr.terminal)
				rb = AddTerm(layergroupboxes, obj, &rd->defaultstyle);
			else
				rb = AddPolygon(layergroupboxes, layer_id(layer), poly, &rd->defaultstyle);
		}
		break;
	case RTRND_LINE:
	case RTRND_ARC:
		if (obj->hdr.terminal)
			rb = AddTerm(layergroupboxes, obj, &rd->defaultstyle);
		break;

	case RTRND_BOARD:
	case RTRND_LAYER:
	case RTRND_NET:
	case RTRND_NETSEG:
		break; /* don't care about these */
	}
	return rb;
}

static void CreateRouteData_obstacles(routedata_t *rd, vtp0_t *layergroupboxes)
{
	rtrnd_board_t *board = rd->ctx->board;
	rtrnd_netseg_t *ns;

	for(ns = gdl_first(&board->orphaned_segments); ns != NULL; ns = gdl_next(&board->orphaned_segments, ns)) {
		rtrnd_any_obj_t *obj;
		for(obj = gdl_first(&ns->objs); obj != NULL; obj = gdl_next(&ns->objs, obj)) {
			if (MAPPED(obj)) continue;
			switch(obj->hdr.type) {
				case RTRND_LINE:
#warning TODO: split up non-axis-aligned lines
					AddLine(layergroupboxes, layer_id(&obj->hdr.parent->layer), &obj->line, &obj->line, &rd->defaultstyle);
					break;
				case RTRND_POLY:
					AddPolygon(layergroupboxes, layer_id(&obj->hdr.parent->layer), &obj->poly, &rd->defaultstyle);
					break;
				case RTRND_ARC:
#warning TODO: split up to smaller sections
					AddArc(layergroupboxes, layer_id(&obj->hdr.parent->layer), &obj->arc, &rd->defaultstyle);
					break;
				case RTRND_VIA:
					AddVia(layergroupboxes, &obj->via, &rd->defaultstyle);
					break;
				default:;
			}
		}
	}
}


static void CreateRouteData_subnet(routedata_t *rd, vtp0_t *layergroupboxes, rtrnd_net_t *net, routebox_t **last_in_net)
{
	routebox_t *last_in_subnet = NULL;
	rtrnd_any_obj_t *obj;
	size_t oi;

	for(oi = 0, obj = gdl_first(&net->objs); obj != NULL; obj = gdl_next(&net->objs, obj), oi++) {
		routebox_t *rb = NULL;

		if (obj->hdr.terminal)
			last_in_subnet = NULL;

		MAPPED(obj) = 1;

		if ((obj->hdr.type == RTRND_LINE) && (obj->hdr.terminal))
			rb = AddTerm(layergroupboxes, obj, &rd->defaultstyle);
		else if (obj->hdr.type == RTRND_LINE)
			rb = crd_add_line(rd, layergroupboxes, layer_id(&obj->hdr.parent->layer), obj, last_in_net, &last_in_subnet);
		else
			rb = crd_add_misc(rd, layergroupboxes, obj);

		if (rb == NULL)
			continue;

		rb->ns = obj->hdr.netseg;
		assert(rb);

		/* update circular connectivity lists */
		if (last_in_subnet && rb != last_in_subnet)
			MergeNets(last_in_subnet, rb, ORIGINAL);
		if (*last_in_net && rb != *last_in_net)
			MergeNets(*last_in_net, rb, NET);
		last_in_subnet = *last_in_net = rb;
		rd->max_bloat = RND_MAX(rd->max_bloat, BLOAT(rb->style));
		rd->max_keep = RND_MAX(rd->max_keep, rb->style->Clearance);
	}
}

static void CreateRouteData_nets(routedata_t *rd, vtp0_t *layergroupboxes)
{
	htsp_entry_t *e;
	routebox_t *last_net = NULL;

	for(e = htsp_first(&PCB->nets); e != NULL; e = htsp_next(&PCB->nets, e)) {
		routebox_t *last_in_net = NULL;
		rtrnd_net_t *net = e->value;
		rtrnd_netseg_t *ns;

		for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns))
			CreateRouteData_subnet(rd, layergroupboxes, net, &last_in_net);

		if (last_net && last_in_net)
			MergeNets(last_net, last_in_net, DIFFERENT_NET);
		last_net = last_in_net;
	}
	rd->first_net = last_net;

	/* reset all nets to "original" connectivity (which we just set) */
	{
		routebox_t *net;
		LIST_LOOP(rd->first_net, different_net, net);
		ResetSubnet(net);
		PCB_END_LOOP;
	}
}

static routedata_t *CreateRouteData(rtrnd_t *ctx)
{
	vtp0_t layergroupboxes[PCB_MAX_LAYERGRP];
	routedata_t *rd;
	int group, i;

	/* create routedata */
	rd = calloc(sizeof(*rd), 1);
	rd->ctx = ctx;

	/* check which layers are active first */
	routing_layers = 0;
	for (group = 0; group < pcb_max_group(PCB); group++) {
		rtrnd_layer_t *ly = PCB->layers.array[group];
		routing_layers++;
		is_layer_group_active[group] = rtrnd_true;

		rd->annot[group] = rtrnd_annot_new(ctx, ly->name);
		strcpy(rd->annot[group]->color, ly->color);
	}

	rd->annot_via = rtrnd_annot_new(ctx, "vias");
	strcpy(rd->annot_via->color, "#BBBBBB");

	/* if via visibility is turned off, don't use them */
	AutoRouteParameters.use_vias = routing_layers > 1;

	front = 0;
	back = pcb_max_group(PCB) - 1;

	/* determine preferred routing direction on each group */
	for (i = 0; i < pcb_max_group(PCB); i++) {
		if (i != back && i != front) {
			x_cost[i] = (i & 1) ? 2 : 1;
			y_cost[i] = (i & 1) ? 1 : 2;
		}
		else if (i == back) {
			x_cost[i] = 4;
			y_cost[i] = 2;
		}
		else {
			x_cost[i] = 2;
			y_cost[i] = 2;
		}
	}

#warning TODO: load these from settings
	/* create default style */
	rd->defaultstyle.Thickness = wire_thick;
	rd->defaultstyle.Clearance = wire_clr;
	rd->defaultstyle.Diameter = via_dia;
	rd->max_bloat = BLOAT(&rd->defaultstyle);
	rd->max_keep = rd->defaultstyle.Clearance;

	/* initialize pointer vectors */
	for (i = 0; i < pcb_max_group(PCB); i++) {
		rtrnd_layer_t *layer = PCB->layers.array[i];
		vtp0_init(&layergroupboxes[i]);
		if (RND_RTREE_EMPTY(&layer->objs))
			usedGroup[i] = rtrnd_false;
		else
			usedGroup[i] = rtrnd_true;
	}
	usedGroup[front] = rtrnd_true;
	usedGroup[back] = rtrnd_true;

	CreateRouteData_nets(rd, layergroupboxes);
	CreateRouteData_obstacles(rd, layergroupboxes);

	/* create r-trees from pointer lists */
	for (i = 0; i < pcb_max_group(PCB); i++) {
		/* create the r-tree */
		rd->layergrouptree[i] = rnd_r_create_tree();
		rnd_r_insert_array(rd->layergrouptree[i], (const rtrnd_rtree_box_t **) layergroupboxes[i].array, vtp0_len(&layergroupboxes[i]));
	}

	if (AutoRouteParameters.use_vias) {
		rd->mtspace = mtspace_create();

		/* create "empty-space" structures for via placement (now that we know
		 * appropriate clearances for all the fixed elements) */
		for (i = 0; i < pcb_max_group(PCB); i++) {
			int ip;
			for(ip = 0; ip < vtp0_len(&layergroupboxes[i]); ip++) {
				void **ptr = &layergroupboxes[i].array[ip];
				routebox_t *rb = (routebox_t *) * ptr;
				if (!rb->flags.clear_poly)
					mtspace_add(rd->mtspace, &rb->box, FIXED, rb->style->Clearance);
			}
		}
	}
	/* free pointer lists */
	for (i = 0; i < pcb_max_group(PCB); i++)
		vtp0_uninit(&layergroupboxes[i]);
	/* done! */
	return rd;
}

void DestroyRouteData(routedata_t ** rd)
{
	int i;
	for (i = 0; i < pcb_max_group(PCB); i++) {
		rnd_r_free_tree_data((*rd)->layergrouptree[i], free);
		rnd_r_destroy_tree(&(*rd)->layergrouptree[i]);
	}
	if (AutoRouteParameters.use_vias)
		mtspace_destroy(&(*rd)->mtspace);
/*	free((*rd)->layergrouptree);*/
	free(*rd);
	*rd = NULL;
}

/*-----------------------------------------------------------------
 * routebox reference counting.
 */

/* increment the reference count on a routebox. */
static void RB_up_count(routebox_t * rb)
{
	assert(rb->flags.homeless);
	rb->refcount++;
}

/* decrement the reference count on a routebox, freeing if this box becomes
 * unused. */
static void RB_down_count(routebox_t * rb)
{
	assert(rb->type == EXPANSION_AREA);
	assert(rb->flags.homeless);
	assert(rb->refcount > 0);
	if (--rb->refcount == 0) {
		if (rb->parent.expansion_area->flags.homeless)
			RB_down_count(rb->parent.expansion_area);
		free(rb);
	}
}

/*-----------------------------------------------------------------
 * Rectangle-expansion routing code.
 */

static void ResetSubnet(routebox_t * net)
{
	routebox_t *rb;
	/* reset connectivity of everything on this net */
	LIST_LOOP(net, same_net, rb);
	rb->same_subnet = rb->original_subnet;
	PCB_END_LOOP;
}

static inline rnd_heap_cost_t pcb_cost_to_point_on_layer(const rnd_cheap_point_t * p1, const rnd_cheap_point_t * p2, long point_layer)
{
	rnd_heap_cost_t x_dist = p1->X - p2->X, y_dist = p1->Y - p2->Y, r;
	x_dist *= x_cost[point_layer];
	y_dist *= y_cost[point_layer];
	/* cost is proportional to orthogonal distance. */
	r = RND_ABS(x_dist) + RND_ABS(y_dist);
	if (p1->X != p2->X && p1->Y != p2->Y)
		r += AutoRouteParameters.JogPenalty;
	return r;
}

static rnd_heap_cost_t pcb_cost_to_point(const rnd_cheap_point_t * p1, long point_layer1, const rnd_cheap_point_t * p2, long point_layer2)
{
	rnd_heap_cost_t r = pcb_cost_to_point_on_layer(p1, p2, point_layer1);
	/* apply via cost penalty if layers differ */
	if (point_layer1 != point_layer2)
		r += AutoRouteParameters.ViaCost;
	return r;
}

/* return the minimum *cost* from a point to a box on any layer.
 * It's safe to return a smaller than minimum cost
 */
static rnd_heap_cost_t pcb_cost_to_layerless_box(const rnd_cheap_point_t * p, long point_layer, const rtrnd_rtree_box_t * b)
{
	rnd_cheap_point_t p2 = rnd_closest_cheap_point_in_box(p, b);
	register rnd_heap_cost_t c1, c2;

	c1 = p2.X - p->X;
	c2 = p2.Y - p->Y;

	c1 = RND_ABS(c1);
	c2 = RND_ABS(c2);
	if (c1 < c2)
		return c1 * AutoRouteParameters.MinPenalty + c2;
	else
		return c2 * AutoRouteParameters.MinPenalty + c1;
}

/* get to actual pins/pad target coordinates */
rtrnd_bool_t TargetPoint(rnd_cheap_point_t * nextpoint, const routebox_t * target)
{
/*	if (target->type == PIN) {
		nextpoint->X = target->parent.pin->X;
		nextpoint->Y = target->parent.pin->Y;
		return rtrnd_true;
	}*/
	nextpoint->X = RND_BOX_CENTER_X(target->sbox);
	nextpoint->Y = RND_BOX_CENTER_Y(target->sbox);
	return rtrnd_false;
}

/* return the *minimum cost* from a point to a route box, including possible
 * via costs if the route box is on a different layer.
 * assume routbox is bloated unless it is an expansion area
 */
static rnd_heap_cost_t pcb_cost_to_routebox(const rnd_cheap_point_t * p, long point_layer, const routebox_t * rb)
{
	register rnd_heap_cost_t trial = 0;
	rnd_cheap_point_t p2 = closest_point_in_routebox(p, rb);
	if (!usedGroup[point_layer] || !usedGroup[rb->group])
		trial = AutoRouteParameters.NewLayerPenalty;
	if ((p2.X - p->X) * (p2.Y - p->Y) != 0)
		trial += AutoRouteParameters.JogPenalty;
	/* special case for defered via searching */
	if (point_layer > pcb_max_group(PCB) || point_layer == rb->group)
		return trial + fabs(p2.X - p->X) + fabs(p2.Y - p->Y);
	/* if this target is only a via away, then the via is cheaper than the congestion */
	if (p->X == p2.X && p->Y == p2.Y)
		return trial + 1;
	trial += AutoRouteParameters.ViaCost;
	trial += fabs(p2.X - p->X) + fabs(p2.Y - p->Y);
	return trial;
}


static rtrnd_rtree_box_t bloat_routebox(routebox_t * rb)
{
	rtrnd_rtree_box_t r;
	double clearance;
	assert(__routepcb_box_is_good(rb));

	if (rb->flags.nobloat)
		return rb->sbox;

	/* Obstacle exclusion zones get bloated by the larger of
	 * the two required clearances plus half the track width.
	 */
	clearance = RND_MAX(AutoRouteParameters.style->Clearance, rb->style->Clearance);
	r = rnd_bloat_box(&rb->sbox, clearance + HALF_THICK(AutoRouteParameters.style->Thickness));
	return r;
}


#ifdef ROUTE_DEBUG  /* only for debugging expansion areas */

typedef short pcb_dimension_t;
/* makes a line on the solder layer silk surrounding the box */
static void showbox(rtrnd_rtree_box_t b, pcb_dimension_t thickness, int group)
{
	rtrnd_line_t *line;
	rtrnd_layer_t *csl, *SLayer = pcb_get_layer(PCB->Data, group);
	rnd_layer_id_t cs_id;
	if (showboxen < -1)
		return;
	if (showboxen != -1 && showboxen != group)
		return;

	if (ddraw != NULL) {
		ddraw->set_line_width(ar_gc, thickness);
		ddraw->set_line_cap(ar_gc, rnd_cap_round);
		ddraw->set_color(ar_gc, SLayer->Color);

		ddraw->draw_line(ar_gc, b.x1, b.y1, b.x2, b.y1);
		ddraw->draw_line(ar_gc, b.x1, b.y2, b.x2, b.y2);
		ddraw->draw_line(ar_gc, b.x1, b.y1, b.x1, b.y2);
		ddraw->draw_line(ar_gc, b.x2, b.y1, b.x2, b.y2);
	}

#if 1
	if (pcb_layer_find(PCB_LYT_TOP | PCB_LYT_SILK, &cs_id, 1) > 0)  {
		csl = pcb_get_layer(PCB->Data, cs_id);
		if (b.y1 == b.y2 || b.x1 == b.x2)
			thickness = 5;
		line = pcb_line_new(csl, b.x1, b.y1, b.x2, b.y1, thickness, 0, pcb_flag_make(0));
		pcb_undo_add_obj_to_create(csl, line, line);
		if (b.y1 != b.y2) {
			line = pcb_line_new(csl, b.x1, b.y2, b.x2, b.y2, thickness, 0, pcb_flag_make(0));
			pcb_undo_add_obj_to_create(PCB_OBJ_LINE, csl, line, line);
		}
		line = pcb_line_new(csl, b.x1, b.y1, b.x1, b.y2, thickness, 0, pcb_flag_make(0));
		pcb_undo_add_obj_to_create(PCB_OBJ_LINE, csl, line, line);
		if (b.x1 != b.x2) {
			line = pcb_line_new(csl, b.x2, b.y1, b.x2, b.y2, thickness, 0, pcb_flag_make(0));
			pcb_undo_add_obj_to_create(PCB_OBJ_LINE, csl, line, line);
		}
	}
#endif
}
#endif

#if defined(ROUTE_DEBUG)
static void showedge(edge_t * e)
{
	rtrnd_rtree_box_t *b = (rtrnd_rtree_box_t *) e->rb;

	if (ddraw == NULL)
		return;

	ddraw->set_line_cap(ar_gc, rnd_cap_round);
	ddraw->set_line_width(ar_gc, 1);
	ddraw->set_color(ar_gc, Settings.MaskColor);

	switch (e->expand_dir) {
	case RND_NORTH:
		ddraw->draw_line(ar_gc, b->X1, b->Y1, b->X2, b->Y1);
		break;
	case RND_SOUTH:
		ddraw->draw_line(ar_gc, b->X1, b->Y2, b->X2, b->Y2);
		break;
	case RND_WEST:
		ddraw->draw_line(ar_gc, b->X1, b->Y1, b->X1, b->Y2);
		break;
	case RND_EAST:
		ddraw->draw_line(ar_gc, b->X2, b->Y1, b->X2, b->Y2);
		break;
	default:
		break;
	}
}
#endif

#if defined(ROUTE_DEBUG)
static void showroutebox(routebox_t * rb)
{
	pcb_layerid_t cs_id;
	if (pcb_layer_find(PCB_LYT_TOP | PCB_LYT_SILK, &cs_id, 1) > 0)
		showbox(rb->sbox, rb->flags.source ? 20 : (rb->flags.target ? 10 : 1), rb->flags.is_via ? pcb_get_layer(PCB->Data, cs_id) : rb->group);
}
#endif

/* return a "parent" of this edge which immediately precedes it in the route.*/
static routebox_t *route_parent(routebox_t * rb)
{
	while (rb->flags.homeless && !rb->flags.is_via && !rb->flags.is_thermal) {
		assert(rb->type == EXPANSION_AREA);
		rb = rb->parent.expansion_area;
		assert(rb);
	}
	return rb;
}

static vector_t *path_conflicts(routebox_t * rb, routebox_t * conflictor, rtrnd_bool_t branch)
{
	if (branch)
		rb->conflicts_with = vector_duplicate(rb->conflicts_with);
	else if (!rb->conflicts_with)
		rb->conflicts_with = vector_create();
	vector_append(rb->conflicts_with, conflictor);
	return rb->conflicts_with;
}

/* Touch everything (except fixed) on each net found
 * in the conflicts vector. If the vector is different
 * from the last one touched, untouch the last batch
 * and touch the new one. Always call with touch=1
 * (except for recursive call). Call with NULL, 1 to
 * clear the last batch touched.
 *
 * touched items become invisible to current path
 * so we don't encounter the same conflictor more
 * than once
 */

static void touch_conflicts(vector_t * conflicts, int touch)
{
	static vector_t *last = NULL;
	static int last_size = 0;
	int i, n;
	i = 0;
	if (touch) {
		if (last && conflicts != last)
			touch_conflicts(last, 0);
		if (!conflicts)
			return;
		last = conflicts;
		i = last_size;
	}
	n = vector_size(conflicts);
	for (; i < n; i++) {
		routebox_t *rb = (routebox_t *) vector_element(conflicts, i);
		routebox_t *p;
		LIST_LOOP(rb, same_net, p);
		if (!p->flags.fixed)
			p->flags.touched = touch;
		PCB_END_LOOP;
	}
	if (!touch) {
		last = NULL;
		last_size = 0;
	}
	else
		last_size = n;
}

/* return a "parent" of this edge which resides in a r-tree somewhere */
/* -- actually, this "parent" *may* be a via box, which doesn't live in
 * a r-tree. -- */
static routebox_t *nonhomeless_parent(routebox_t * rb)
{
	return route_parent(rb);
}

/* some routines to find the minimum *cost* from a cost point to
 * a target (any target) */
struct minpcb_cost_target_closure {
	const rnd_cheap_point_t *CostPoint;
	long CostPointLayer;
	routebox_t *nearest;
	rnd_heap_cost_t nearest_cost;
};
static rnd_r_dir_t __region_within_guess(const rtrnd_rtree_box_t * region, void *cl)
{
	struct minpcb_cost_target_closure *mtc = (struct minpcb_cost_target_closure *) cl;
	rnd_heap_cost_t pcb_cost_to_region;
	if (mtc->nearest == NULL)
		return RND_R_DIR_FOUND_CONTINUE;
	pcb_cost_to_region = pcb_cost_to_layerless_box(mtc->CostPoint, mtc->CostPointLayer, region);
	assert(pcb_cost_to_region >= 0);
	/* if no guess yet, all regions are "close enough" */
	/* note that cost is *strictly more* than minimum distance, so we'll
	 * always search a region large enough. */
	return (pcb_cost_to_region < mtc->nearest_cost) ? RND_R_DIR_FOUND_CONTINUE : RND_R_DIR_NOT_FOUND;
}

static rnd_r_dir_t __found_new_guess(const rtrnd_rtree_box_t * box, void *cl)
{
	struct minpcb_cost_target_closure *mtc = (struct minpcb_cost_target_closure *) cl;
	routebox_t *guess = (routebox_t *) box;
	rnd_heap_cost_t pcb_cost_to_guess = pcb_cost_to_routebox(mtc->CostPoint, mtc->CostPointLayer, guess);
	assert(pcb_cost_to_guess >= 0);
	/* if this is cheaper than previous guess... */
	if (pcb_cost_to_guess < mtc->nearest_cost) {
		mtc->nearest = guess;
		mtc->nearest_cost = pcb_cost_to_guess;	/* this is our new guess! */
		return RND_R_DIR_FOUND_CONTINUE;
	}
	else
		return RND_R_DIR_NOT_FOUND;										/* not less expensive than our last guess */
}

/* target_guess is our guess at what the nearest target is, or NULL if we
 * just plum don't have a clue. */
static routebox_t *minpcb_cost_target_to_point(const rnd_cheap_point_t * CostPoint,
																					 long CostPointLayer, rtrnd_rtree_t * targets, routebox_t * target_guess)
{
	struct minpcb_cost_target_closure mtc;
	assert(target_guess == NULL || target_guess->flags.target);	/* this is a target, right? */
	mtc.CostPoint = CostPoint;
	mtc.CostPointLayer = CostPointLayer;
	mtc.nearest = target_guess;
	if (mtc.nearest)
		mtc.nearest_cost = pcb_cost_to_routebox(mtc.CostPoint, mtc.CostPointLayer, mtc.nearest);
	else
		mtc.nearest_cost = EXPENSIVE;
	rnd_r_search(targets, NULL, __region_within_guess, __found_new_guess, &mtc, NULL);
	assert(mtc.nearest != NULL && mtc.nearest_cost >= 0);
	assert(mtc.nearest->flags.target);	/* this is a target, right? */
	return mtc.nearest;
}

/* create edge from field values */
/* minpcb_cost_target_guess can be NULL */
static edge_t *CreateEdge(routebox_t * rb,
													double CostPointX, double CostPointY,
													rnd_heap_cost_t pcb_cost_to_point, routebox_t * minpcb_cost_target_guess, rnd_direction_t expand_dir, rtrnd_rtree_t * targets)
{
	edge_t *e;
	assert(__routepcb_box_is_good(rb));
	e = (edge_t *) malloc(sizeof(*e));
	memset((void *) e, 0, sizeof(*e));
	assert(e);
	e->rb = rb;
	if (rb->flags.homeless)
		RB_up_count(rb);
	e->cost_point.X = CostPointX;
	e->cost_point.Y = CostPointY;
	e->pcb_cost_to_point = pcb_cost_to_point;
	e->flags.via_search = 0;
	/* if this edge is created in response to a target, use it */
	if (targets)
		e->minpcb_cost_target = minpcb_cost_target_to_point(&e->cost_point, rb->group, targets, minpcb_cost_target_guess);
	else
		e->minpcb_cost_target = minpcb_cost_target_guess;
	e->expand_dir = expand_dir;
	assert(e->rb && e->minpcb_cost_target);	/* valid edge? */
	assert(!e->flags.is_via || e->expand_dir == RND_ANY_DIR);
	/* cost point should be on edge (unless this is a plane/via/conflict edge) */
#if 0
	assert(rb->type == PLANE || rb->conflicts_with != NULL || rb->flags.is_via
				 || rb->flags.is_thermal
				 || ((expand_dir == RND_NORTH || expand_dir == RND_SOUTH) ? rb->sbox.x1 <=
						 CostPointX && CostPointX < rb->sbox.x2 && CostPointY == (expand_dir == RND_NORTH ? rb->sbox.y1 : rb->sbox.y2 - DELTA) :
						 /* expand_dir==EAST || expand_dir==WEST */
						 rb->sbox.y1 <= CostPointY && CostPointY < rb->sbox.y2 &&
						 CostPointX == (expand_dir == RND_EAST ? rb->sbox.x2 - DELTA : rb->sbox.x1)));
#endif
	assert(__edge_is_good(e));
	/* done */
	return e;
}

/* create edge, using previous edge to fill in defaults. */
/* most of the work here is in determining a new cost point */
static edge_t *CreateEdge2(routebox_t * rb, rnd_direction_t expand_dir,
													 edge_t * previous_edge, rtrnd_rtree_t * targets, routebox_t * guess)
{
	rtrnd_rtree_box_t thisbox;
	rnd_cheap_point_t thiscost, prevcost;
	rnd_heap_cost_t d;

	assert(rb && previous_edge);
	/* okay, find cheapest costpoint to costpoint of previous edge */
	thisbox = edge_to_box(rb, expand_dir);
	prevcost = previous_edge->cost_point;
	/* find point closest to target */
	thiscost = rnd_closest_cheap_point_in_box(&prevcost, &thisbox);
	/* compute cost-to-point */
	d = pcb_cost_to_point_on_layer(&prevcost, &thiscost, rb->group);
	/* add in jog penalty */
	if (previous_edge->expand_dir != expand_dir)
		d += AutoRouteParameters.JogPenalty;
	/* okay, new edge! */
	return CreateEdge(rb, thiscost.X, thiscost.Y,
										previous_edge->pcb_cost_to_point + d, guess ? guess : previous_edge->minpcb_cost_target, expand_dir, targets);
}

/* create via edge, using previous edge to fill in defaults. */
static edge_t *CreateViaEdge(const rtrnd_rtree_box_t * area, long group,
														 routebox_t * parent, edge_t * previous_edge,
														 conflict_t to_site_conflict, conflict_t through_site_conflict, rtrnd_rtree_t * targets)
{
	routebox_t *rb;
	rnd_cheap_point_t costpoint;
	rnd_heap_cost_t d;
	edge_t *ne;
	rnd_heap_cost_t scale[3];

	scale[0] = 1;
	scale[1] = AutoRouteParameters.LastConflictPenalty;
	scale[2] = AutoRouteParameters.ConflictPenalty;

	assert(rnd_box_is_good(area));
	assert(AutoRouteParameters.with_conflicts || (to_site_conflict == NO_CONFLICT && through_site_conflict == NO_CONFLICT));
	rb = CreateExpansionArea(area, group, parent, rtrnd_true, previous_edge);
	rb->flags.is_via = 1;
	rb->came_from = RND_ANY_DIR;
#if defined(ROUTE_DEBUG) && defined(DEBUG_SHOW_VIA_BOXES)
	showroutebox(rb);
#endif /* ROUTE_DEBUG && DEBUG_SHOW_VIA_BOXES */
	/* for planes, choose a point near the target */
	if (previous_edge->flags.in_plane) {
		routebox_t *target;
		rnd_cheap_point_t pnt;
		/* find a target near this via box */
		pnt.X = RND_BOX_CENTER_X(*area);
		pnt.Y = RND_BOX_CENTER_Y(*area);
		target = minpcb_cost_target_to_point(&pnt, rb->group, targets, previous_edge->minpcb_cost_target);
		/* now find point near the target */
		pnt.X = RND_BOX_CENTER_X(target->box);
		pnt.Y = RND_BOX_CENTER_Y(target->box);
		costpoint = closest_point_in_routebox(&pnt, rb);
		/* we moved from the previous cost point through the plane which is free travel */
		d = (scale[through_site_conflict] * pcb_cost_to_point(&costpoint, group, &costpoint, previous_edge->rb->group));
		ne = CreateEdge(rb, costpoint.X, costpoint.Y, previous_edge->pcb_cost_to_point + d, target, RND_ANY_DIR, NULL);
		ne->minpcb_cost_target = target;
	}
	else {
		routebox_t *target;
		target = previous_edge->minpcb_cost_target;
		costpoint = closest_point_in_routebox(&previous_edge->cost_point, rb);
		d =
			(scale[to_site_conflict] *
			 pcb_cost_to_point_on_layer(&costpoint, &previous_edge->cost_point,
															previous_edge->rb->group)) +
			(scale[through_site_conflict] * pcb_cost_to_point(&costpoint, group, &costpoint, previous_edge->rb->group));
		/* if the target is just this via away, then this via is cheaper */
		if (target->group == group && point_in_shrunk_box(target, costpoint.X, costpoint.Y))
			d -= AutoRouteParameters.ViaCost / 2;
		ne =
			CreateEdge(rb, costpoint.X, costpoint.Y, previous_edge->pcb_cost_to_point + d, previous_edge->minpcb_cost_target, RND_ANY_DIR, targets);
	}
	ne->flags.is_via = 1;
	ne->flags.via_conflict_level = to_site_conflict;
	assert(__edge_is_good(ne));
	return ne;
}

/* create "interior" edge for routing with conflicts */
/* Presently once we "jump inside" the conflicting object
 * we consider it a routing highway to travel inside since
 * it will become available if the conflict is elliminated.
 * That is why we ignore the interior_edge argument.
 */
static edge_t *CreateEdgeWithConflicts(const rtrnd_rtree_box_t * interior_edge,
																			 routebox_t * container, edge_t * previous_edge,
																			 rnd_heap_cost_t cost_penalty_to_box, rtrnd_rtree_t * targets)
{
	routebox_t *rb;
	rnd_cheap_point_t costpoint;
	rnd_heap_cost_t d;
	edge_t *ne;
	assert(interior_edge && container && previous_edge && targets);
	assert(!container->flags.homeless);
	assert(AutoRouteParameters.with_conflicts);
	assert(container->flags.touched == 0);
	assert(previous_edge->rb->group == container->group);
	/* use the caller's idea of what this box should be */
	rb = CreateExpansionArea(interior_edge, previous_edge->rb->group, previous_edge->rb, rtrnd_true, previous_edge);
	path_conflicts(rb, container, rtrnd_true);	/* crucial! */
	costpoint = rnd_closest_cheap_point_in_box(&previous_edge->cost_point, interior_edge);
	d = pcb_cost_to_point_on_layer(&costpoint, &previous_edge->cost_point, previous_edge->rb->group);
	d *= cost_penalty_to_box;
	d += previous_edge->pcb_cost_to_point;
	ne = CreateEdge(rb, costpoint.X, costpoint.Y, d, NULL, RND_ANY_DIR, targets);
	ne->flags.is_interior = 1;
	assert(__edge_is_good(ne));
	return ne;
}

static void KillEdge(void *edge)
{
	edge_t *e = (edge_t *) edge;
	assert(e);
	if (e->rb->flags.homeless)
		RB_down_count(e->rb);
	if (e->flags.via_search)
		mtsFreeWork(&e->work);
	free(e);
}

static void DestroyEdge(edge_t ** e)
{
	assert(e && *e);
	KillEdge(*e);
	*e = NULL;
}

/* cost function for an edge. */
static rnd_heap_cost_t edge_cost(const edge_t * e, const rnd_heap_cost_t too_big)
{
	rnd_heap_cost_t penalty = e->pcb_cost_to_point;
	if (e->rb->flags.is_thermal || e->rb->type == PLANE)
		return penalty;							/* thermals are cheap */
	if (penalty > too_big)
		return penalty;

	/* pcb_cost_to_routebox adds in our via correction, too. */
	return penalty + pcb_cost_to_routebox(&e->cost_point, e->rb->group, e->minpcb_cost_target);
}

/* given an edge of a box, return a box containing exactly the points on that
 * edge.  Note that the return box is treated as closed; that is, the bottom and
 * right "edges" consist of points (just barely) not in the (half-open) box. */
static rtrnd_rtree_box_t edge_to_box(const routebox_t * rb, rnd_direction_t expand_dir)
{
	rtrnd_rtree_box_t b = shrink_routebox(rb);
	/* narrow box down to just the appropriate edge */
	switch (expand_dir) {
	case RND_NORTH:
		b.y2 = b.y1 + DELTA;
		break;
	case RND_EAST:
		b.x1 = b.x2 - DELTA;
		break;
	case RND_SOUTH:
		b.y1 = b.y2 - DELTA;
		break;
	case RND_WEST:
		b.x2 = b.x1 + DELTA;
		break;
	default:
		/* This used to be an assert(0), but it seems a polygon connected to
		   a terminal triggers it while simply falling through doesn't cause
		   any problem. This bug is present in both the 2011 version of
		   PCB and in PCB 4.2.0 as well. */
		break;
	}
	/* done! */
	return b;
}

struct broken_boxes {
	rtrnd_rtree_box_t left, center, right;
	rtrnd_bool_t is_valid_left, is_valid_center, is_valid_right;
};

static struct broken_boxes break_box_edge(const rtrnd_rtree_box_t * original, rnd_direction_t which_edge, routebox_t * breaker)
{
	rtrnd_rtree_box_t origbox, breakbox;
	struct broken_boxes result;

	assert(original && breaker);

	origbox = *original;
	breakbox = bloat_routebox(breaker);
	RND_BOX_ROTATE_TO_NORTH(origbox, which_edge);
	RND_BOX_ROTATE_TO_NORTH(breakbox, which_edge);
	result.right.y1 = result.center.y1 = result.left.y1 = origbox.y1;
	result.right.y2 = result.center.y2 = result.left.y2 = origbox.y1 + DELTA;
	/* validity of breaker is not important because the boxes are marked invalid */
	/*assert (breakbox.x1 <= origbox.x2 && breakbox.x2 >= origbox.x1); */
	/* left edge piece */
	result.left.x1 = origbox.x1;
	result.left.x2 = breakbox.x1;
	/* center (ie blocked) edge piece */
	result.center.x1 = RND_MAX(breakbox.x1, origbox.x1);
	result.center.x2 = RND_MIN(breakbox.x2, origbox.x2);
	/* right edge piece */
	result.right.x1 = breakbox.x2;
	result.right.x2 = origbox.x2;
	/* validity: */
	result.is_valid_left = (result.left.x1 < result.left.x2);
	result.is_valid_center = (result.center.x1 < result.center.x2);
	result.is_valid_right = (result.right.x1 < result.right.x2);
	/* rotate back */
	RND_BOX_ROTATE_FROM_NORTH(result.left, which_edge);
	RND_BOX_ROTATE_FROM_NORTH(result.center, which_edge);
	RND_BOX_ROTATE_FROM_NORTH(result.right, which_edge);
	/* done */
	return result;
}

#ifndef NDEBUG
static int share_edge(const rtrnd_rtree_box_t * child, const rtrnd_rtree_box_t * parent)
{
	return
		(child->x1 == parent->x2 || child->x2 == parent->x1 ||
		 child->y1 == parent->y2 || child->y2 == parent->y1) &&
		((parent->x1 <= child->x1 && child->x2 <= parent->x2) || (parent->y1 <= child->y1 && child->y2 <= parent->y2));
}

static int edge_intersect(const rtrnd_rtree_box_t * child, const rtrnd_rtree_box_t * parent)
{
	return (child->x1 <= parent->x2) && (child->x2 >= parent->x1) && (child->y1 <= parent->y2) && (child->y2 >= parent->y1);
}
#endif

/* area is the expansion area, on layer group 'group'. 'parent' is the
 * immediately preceding expansion area, for backtracing. 'lastarea' is
 * the last expansion area created, we string these together in a loop
 * so we can remove them all easily at the end. */
static routebox_t *CreateExpansionArea(const rtrnd_rtree_box_t * area, long group,
																			 routebox_t * parent, rtrnd_bool_t relax_edge_requirements, edge_t * src_edge)
{
	routebox_t *rb = (routebox_t *) malloc(sizeof(*rb));
	memset((void *) rb, 0, sizeof(*rb));
	assert(area && parent);
	init_const_box(rb, area->x1, area->y1, area->x2, area->y2, 0);
	rb->group = group;
	rb->type = EXPANSION_AREA;
	/* should always share edge or overlap with parent */
#ifndef NDEBUG
	{
		/* work around rounding errors: grow both boxes by a few nm */
		rtrnd_rtree_box_t b1 = rb->sbox, b2 = parent->sbox;
		double r = DELTA;
		b1.x1-=r;b1.y1-=r;b1.x2+=r;b1.y2+=r;
		b2.x1-=r;b2.y1-=r;b2.x2+=r;b2.y2+=r;
		assert(relax_edge_requirements ? rnd_box_intersect(&b1, &b2)
					 : share_edge(&rb->sbox, &parent->sbox));
	}
#endif
	rb->parent.expansion_area = route_parent(parent);
	rb->cost_point = rnd_closest_cheap_point_in_box(&rb->parent.expansion_area->cost_point, area);
	rb->cost =
		rb->parent.expansion_area->cost +
		pcb_cost_to_point_on_layer(&rb->parent.expansion_area->cost_point, &rb->cost_point, rb->group);
	assert(relax_edge_requirements ? edge_intersect(&rb->sbox, &parent->sbox)
				 : share_edge(&rb->sbox, &parent->sbox));
	if (rb->parent.expansion_area->flags.homeless)
		RB_up_count(rb->parent.expansion_area);
	rb->flags.homeless = 1;
	rb->flags.nobloat = 1;
	rb->style = AutoRouteParameters.style;
	rb->conflicts_with = parent->conflicts_with;
/* we will never link an EXPANSION_AREA into the nets because they
 * are *ONLY* used for path searching. No need to call  InitLists ()
 */
	rb->came_from = src_edge->expand_dir;
#if defined(ROUTE_DEBUG) && defined(DEBUG_SHOW_EXPANSION_BOXES)
	showroutebox(rb);
#endif /* ROUTE_DEBUG && DEBUG_SHOW_EXPANSION_BOXES */
	return rb;
}

/*------ Expand ------*/
struct E_result {
	routebox_t *parent;
	routebox_t *n, *e, *s, *w;
	double keep, bloat;
	rtrnd_rtree_box_t inflated, orig;
	int done;
};

/* test method for Expand()
 * this routebox potentially is a blocker limiting expansion
 * if this is so, we limit the inflate box so another exactly
 * like it wouldn't be seen. We do this while keep the inflated
 * box as large as possible.
 */
static rnd_r_dir_t __Expand_this_rect(const rtrnd_rtree_box_t * box, void *cl)
{
	struct E_result *res = (struct E_result *) cl;
	routebox_t *rb = (routebox_t *) box;
	rtrnd_rtree_box_t rbox;
	double dn, de, ds, dw, bloat;

	/* we don't see conflicts already encountered */
	if (rb->flags.touched)
		return RND_R_DIR_NOT_FOUND;

	/* The inflated box outer edges include its own
	 * track width plus its own clearance.
	 *
	 * To check for intersection, we need to expand
	 * anything with greater clearance by its excess
	 * clearance.
	 *
	 * If something has nobloat then we need to shrink
	 * the inflated box back and see if it still touches.
	 */

	if (rb->flags.nobloat) {
		rbox = rb->sbox;
		bloat = res->bloat;
		if (rbox.x2 <= res->inflated.x1 + bloat ||
				rbox.x1 >= res->inflated.x2 - bloat || rbox.y1 >= res->inflated.y2 - bloat || rbox.y2 <= res->inflated.y1 + bloat)
			return RND_R_DIR_NOT_FOUND;									/* doesn't touch */
	}
	else {
		if (rb->style->Clearance > res->keep)
			rbox = rnd_bloat_box(&rb->sbox, rb->style->Clearance - res->keep);
		else
			rbox = rb->sbox;

		if (rbox.x2 <= res->inflated.x1 || rbox.x1 >= res->inflated.x2
				|| rbox.y1 >= res->inflated.y2 || rbox.y2 <= res->inflated.y1)
			return RND_R_DIR_NOT_FOUND;									/* doesn't touch */
		bloat = 0;
	}

	/* this is an intersecting box; it has to jump through a few more hoops */
	if (rb == res->parent || rb->parent.expansion_area == res->parent)
		return RND_R_DIR_NOT_FOUND;										/* don't see what we came from */

	/* if we are expanding a source edge, don't let other sources
	 * or their expansions stop us.
	 */
#if 1
	if (res->parent->flags.source)
		if (rb->flags.source || (rb->type == EXPANSION_AREA && rb->parent.expansion_area->flags.source))
			return RND_R_DIR_NOT_FOUND;
#endif

	/* we ignore via expansion boxes because maybe its
	 * cheaper to get there without the via through
	 * the path we're exploring  now.
	 */
	if (rb->flags.is_via && rb->type == EXPANSION_AREA)
		return RND_R_DIR_NOT_FOUND;

	if (rb->type == PLANE) {			/* expanding inside a plane is not good */
		if (rbox.x1 < res->orig.x1 && rbox.x2 > res->orig.x2 && rbox.y1 < res->orig.y1 && rbox.y2 > res->orig.y2) {
			res->inflated = rnd_bloat_box(&res->orig, res->bloat);
			return RND_R_DIR_FOUND_CONTINUE;
		}
	}
	/* calculate the distances from original box to this blocker */
	dn = de = ds = dw = 0;
	if (!(res->done & _NORTH) && rbox.y1 <= res->orig.y1 && rbox.y2 > res->inflated.y1)
		dn = res->orig.y1 - rbox.y2;
	if (!(res->done & _EAST) && rbox.x2 >= res->orig.x2 && rbox.x1 < res->inflated.x2)
		de = rbox.x1 - res->orig.x2;
	if (!(res->done & _SOUTH) && rbox.y2 >= res->orig.y2 && rbox.y1 < res->inflated.y2)
		ds = rbox.y1 - res->orig.y2;
	if (!(res->done & _WEST) && rbox.x1 <= res->orig.x1 && rbox.x2 > res->inflated.x1)
		dw = res->orig.x1 - rbox.x2;
	if (dn <= 0 && de <= 0 && ds <= 0 && dw <= 0)
		return RND_R_DIR_FOUND_CONTINUE;
	/* now shrink the inflated box to the largest blocking direction */
	if (dn >= de && dn >= ds && dn >= dw) {
		res->inflated.y1 = rbox.y2 - bloat;
		res->n = rb;
	}
	else if (de >= ds && de >= dw) {
		res->inflated.x2 = rbox.x1 + bloat;
		res->e = rb;
	}
	else if (ds >= dw) {
		res->inflated.y2 = rbox.y1 + bloat;
		res->s = rb;
	}
	else {
		res->inflated.x1 = rbox.x2 - bloat;
		res->w = rb;
	}
	return RND_R_DIR_FOUND_CONTINUE;
}

static rtrnd_bool_t boink_box(routebox_t * rb, struct E_result *res, rnd_direction_t dir)
{
	double bloat;
	if (rb->style->Clearance > res->keep)
		bloat = res->keep - rb->style->Clearance;
	else
		bloat = 0;
	if (rb->flags.nobloat)
		bloat = res->bloat;
	switch (dir) {
	case RND_NORTH:
	case RND_SOUTH:
		if (rb->sbox.x2 <= res->inflated.x1 + bloat || rb->sbox.x1 >= res->inflated.x2 - bloat)
			return rtrnd_false;
		return rtrnd_true;
	case RND_EAST:
	case RND_WEST:
		if (rb->sbox.y1 >= res->inflated.y2 - bloat || rb->sbox.y2 <= res->inflated.y1 + bloat)
			return rtrnd_false;
		return rtrnd_true;
		break;
	default:
		assert(0);
	}
	return rtrnd_false;
}

/* main Expand routine.
 *
 * The expansion probe edge includes the clearance and half thickness
 * as the search is performed in order to see everything relevant.
 * The result is backed off by this amount before being returned.
 * Targets (and other no-bloat routeboxes) go all the way to touching.
 * This is accomplished by backing off the probe edge when checking
 * for touch against such an object. Usually the expanding edge
 * bumps into neighboring pins on the same device that require a
 * clearance, preventing seeing a target immediately. Rather than await
 * another expansion to actually touch the target, the edge breaker code
 * looks past the clearance to see these targets even though they
 * weren't actually touched in the expansion.
 */
struct E_result *Expand(rtrnd_rtree_t * rtree, edge_t * e, const rtrnd_rtree_box_t * box)
{
	static struct E_result ans;
	int noshrink;									/* bit field of which edges to not shrink */

	ans.bloat = AutoRouteParameters.bloat;
	ans.orig = *box;
	ans.n = ans.e = ans.s = ans.w = NULL;

	/* the inflated box must be bloated in all directions that it might
	 * hit something in order to guarantee that we see object in the
	 * tree it might hit. The tree holds objects bloated by their own
	 * clearance so we are guaranteed to honor that.
	 */
	switch (e->expand_dir) {
	case RND_ANY_DIR:
		ans.inflated.x1 = (e->rb->came_from == RND_EAST ? ans.orig.x1 : 0);
		ans.inflated.y1 = (e->rb->came_from == RND_SOUTH ? ans.orig.y1 : 0);
		ans.inflated.x2 = (e->rb->came_from == RND_WEST ? ans.orig.x2 : PCB->hdr.bbox.x2);
		ans.inflated.y2 = (e->rb->came_from == RND_NORTH ? ans.orig.y2 : PCB->hdr.bbox.y2);
		if (e->rb->came_from == RND_NORTH)
			ans.done = noshrink = _SOUTH;
		else if (e->rb->came_from == RND_EAST)
			ans.done = noshrink = _WEST;
		else if (e->rb->came_from == RND_SOUTH)
			ans.done = noshrink = _NORTH;
		else if (e->rb->came_from == RND_WEST)
			ans.done = noshrink = _EAST;
		else
			ans.done = noshrink = 0;
		break;
	case RND_NORTH:
		ans.done = _SOUTH + _EAST + _WEST;
		noshrink = _SOUTH;
		ans.inflated.x1 = box->x1 - ans.bloat;
		ans.inflated.x2 = box->x2 + ans.bloat;
		ans.inflated.y2 = box->y2;
		ans.inflated.y1 = 0;				/* far north */
		break;
	case RND_NE:
		ans.done = _SOUTH + _WEST;
		noshrink = 0;
		ans.inflated.x1 = box->x1 - ans.bloat;
		ans.inflated.x2 = PCB->hdr.bbox.x2;
		ans.inflated.y2 = box->y2 + ans.bloat;
		ans.inflated.y1 = 0;
		break;
	case RND_EAST:
		ans.done = _NORTH + _SOUTH + _WEST;
		noshrink = _WEST;
		ans.inflated.y1 = box->y1 - ans.bloat;
		ans.inflated.y2 = box->y2 + ans.bloat;
		ans.inflated.x1 = box->x1;
		ans.inflated.x2 = PCB->hdr.bbox.x2;
		break;
	case RND_SE:
		ans.done = _NORTH + _WEST;
		noshrink = 0;
		ans.inflated.x1 = box->x1 - ans.bloat;
		ans.inflated.x2 = PCB->hdr.bbox.x2;
		ans.inflated.y2 = PCB->hdr.bbox.y2;
		ans.inflated.y1 = box->y1 - ans.bloat;
		break;
	case RND_SOUTH:
		ans.done = _NORTH + _EAST + _WEST;
		noshrink = _NORTH;
		ans.inflated.x1 = box->x1 - ans.bloat;
		ans.inflated.x2 = box->x2 + ans.bloat;
		ans.inflated.y1 = box->y1;
		ans.inflated.y2 = PCB->hdr.bbox.y2;
		break;
	case RND_SW:
		ans.done = _NORTH + _EAST;
		noshrink = 0;
		ans.inflated.x1 = 0;
		ans.inflated.x2 = box->x2 + ans.bloat;
		ans.inflated.y2 = PCB->hdr.bbox.y2;
		ans.inflated.y1 = box->y1 - ans.bloat;
		break;
	case RND_WEST:
		ans.done = _NORTH + _SOUTH + _EAST;
		noshrink = _EAST;
		ans.inflated.y1 = box->y1 - ans.bloat;
		ans.inflated.y2 = box->y2 + ans.bloat;
		ans.inflated.x1 = 0;
		ans.inflated.x2 = box->x2;
		break;
	case RND_NW:
		ans.done = _SOUTH + _EAST;
		noshrink = 0;
		ans.inflated.x1 = 0;
		ans.inflated.x2 = box->x2 + ans.bloat;
		ans.inflated.y2 = box->y2 + ans.bloat;
		ans.inflated.y1 = 0;
		break;
	default:
		noshrink = ans.done = 0;
		assert(0);
	}
	ans.keep = e->rb->style->Clearance;
	ans.parent = nonhomeless_parent(e->rb);
	rnd_r_search(rtree, &ans.inflated, NULL, __Expand_this_rect, &ans, NULL);
/* because the overlaping boxes are found in random order, some blockers
 * may have limited edges prematurely, so we check if the blockers realy
 * are blocking, and make another try if not
 */
	if (ans.n && !boink_box(ans.n, &ans, RND_NORTH))
		ans.inflated.y1 = 0;
	else
		ans.done |= _NORTH;
	if (ans.e && !boink_box(ans.e, &ans, RND_EAST))
		ans.inflated.x2 = PCB->hdr.bbox.x2;
	else
		ans.done |= _EAST;
	if (ans.s && !boink_box(ans.s, &ans, RND_SOUTH))
		ans.inflated.y2 = PCB->hdr.bbox.y2;
	else
		ans.done |= _SOUTH;
	if (ans.w && !boink_box(ans.w, &ans, RND_WEST))
		ans.inflated.x1 = 0;
	else
		ans.done |= _WEST;
	if (ans.done != _NORTH + _EAST + _SOUTH + _WEST) {
		rnd_r_search(rtree, &ans.inflated, NULL, __Expand_this_rect, &ans, NULL);
	}
	if ((noshrink & _NORTH) == 0)
		ans.inflated.y1 += ans.bloat;
	if ((noshrink & _EAST) == 0)
		ans.inflated.x2 -= ans.bloat;
	if ((noshrink & _SOUTH) == 0)
		ans.inflated.y2 -= ans.bloat;
	if ((noshrink & _WEST) == 0)
		ans.inflated.x1 += ans.bloat;
	return &ans;
}

/* blocker_to_heap puts the blockers into a heap so they
 * can be retrieved in clockwise order. If a blocker
 * is also a target, it gets put into the vector too.
 * It returns 1 for any fixed blocker that is not part
 * of this net and zero otherwise.
 */
static int blocker_to_heap(rnd_heap_t * heap, routebox_t * rb, rtrnd_rtree_box_t * box, rnd_direction_t dir)
{
	rtrnd_rtree_box_t b = rb->sbox;
	if (rb->style->Clearance > AutoRouteParameters.style->Clearance)
		b = rnd_bloat_box(&b, rb->style->Clearance - AutoRouteParameters.style->Clearance);
	b = rnd_clip_box(&b, box);
	assert(rnd_box_is_good(&b));
	/* we want to look at the blockers clockwise around the box */
	switch (dir) {
		/* we need to use the other coordinate fraction to resolve
		 * ties since we want the shorter of the furthest
		 * first.
		 */
	case RND_NORTH:
		rnd_heap_insert(heap, b.x1 - b.x1 / (b.x2 + DELTA), rb);
		break;
	case RND_EAST:
		rnd_heap_insert(heap, b.y1 - b.y1 / (b.y2 + DELTA), rb);
		break;
	case RND_SOUTH:
		rnd_heap_insert(heap, -(b.x2 + b.x1 / (b.x2 + DELTA)), rb);
		break;
	case RND_WEST:
		rnd_heap_insert(heap, -(b.y2 + b.y1 / (b.y2 + DELTA)), rb);
		break;
	default:
		assert(0);
	}
	if (rb->flags.fixed && !rb->flags.target && !rb->flags.source)
		return 1;
	return 0;
}

/* this creates an EXPANSION_AREA to bridge small gaps or,
 * (more commonly) create a supper-thin box to provide a
 * home for an expansion edge.
 */
static routebox_t *CreateBridge(const rtrnd_rtree_box_t * area, routebox_t * parent, rnd_direction_t dir)
{
	routebox_t *rb = (routebox_t *) malloc(sizeof(*rb));
	memset((void *) rb, 0, sizeof(*rb));
	assert(area && parent);
	init_const_box(rb, area->x1, area->y1, area->x2, area->y2, 0);
	rb->group = parent->group;
	rb->type = EXPANSION_AREA;
	rb->came_from = dir;
	rb->cost_point = rnd_closest_cheap_point_in_box(&parent->cost_point, area);
	rb->cost = parent->cost + pcb_cost_to_point_on_layer(&parent->cost_point, &rb->cost_point, rb->group);
	rb->parent.expansion_area = route_parent(parent);
	if (rb->parent.expansion_area->flags.homeless)
		RB_up_count(rb->parent.expansion_area);
	rb->flags.homeless = 1;
	rb->flags.nobloat = 1;
	rb->style = parent->style;
	rb->conflicts_with = parent->conflicts_with;
#if defined(ROUTE_DEBUG) && defined(DEBUG_SHOW_EDGES)
	showroutebox(rb);
#endif
	return rb;
}

/* moveable_edge prepares the new search edges based on the
 * starting box, direction and blocker if any.
 */
void
moveable_edge(vector_t * result, const rtrnd_rtree_box_t * box, rnd_direction_t dir,
							routebox_t * rb,
							routebox_t * blocker, edge_t * e, rtrnd_rtree_t * targets,
							routeone_state_t *s, rtrnd_rtree_t * tree, vector_t * area_vec)
{
	rtrnd_rtree_box_t b;
	assert(rnd_box_is_good(box));
	b = *box;
	/* for the cardinal directions, move the box to overlap the
	 * the parent by 1 unit. Corner expansions overlap more
	 * and their starting boxes are pre-prepared.
	 * Check if anything is headed off the board edges
	 */
	switch (dir) {
	default:
		break;
	case RND_NORTH:
		b.y2 = b.y1;
		b.y1-=DELTA;
		if (b.y1 <= AutoRouteParameters.bloat)
			return;										/* off board edge */
		break;
	case RND_EAST:
		b.x1 = b.x2;
		b.x2+=DELTA;
		if (b.x2 >= PCB->hdr.bbox.x2 - AutoRouteParameters.bloat)
			return;										/* off board edge */
		break;
	case RND_SOUTH:
		b.y1 = b.y2;
		b.y2+=DELTA;
		if (b.y2 >= PCB->hdr.bbox.y2 - AutoRouteParameters.bloat)
			return;										/* off board edge */
		break;
	case RND_WEST:
		b.x2 = b.x1;
		b.x1-=DELTA;
		if (b.x1 <= AutoRouteParameters.bloat)
			return;										/* off board edge */
		break;
	case RND_NE:
		if (b.y1 <= AutoRouteParameters.bloat + DELTA && b.x2 >= PCB->hdr.bbox.x2 - AutoRouteParameters.bloat - DELTA)
			return;										/* off board edge */
		if (b.y1 <= AutoRouteParameters.bloat + DELTA)
			dir = RND_EAST;								/* north off board edge */
		if (b.x2 >= PCB->hdr.bbox.x2 - AutoRouteParameters.bloat - DELTA)
			dir = RND_NORTH;							/* east off board edge */
		break;
	case RND_SE:
		if (b.y2 >= PCB->hdr.bbox.y2 - AutoRouteParameters.bloat - DELTA && b.x2 >= PCB->hdr.bbox.x2 - AutoRouteParameters.bloat - DELTA)
			return;										/* off board edge */
		if (b.y2 >= PCB->hdr.bbox.y2 - AutoRouteParameters.bloat - DELTA)
			dir = RND_EAST;								/* south off board edge */
		if (b.x2 >= PCB->hdr.bbox.x2 - AutoRouteParameters.bloat - DELTA)
			dir = RND_SOUTH;							/* east off board edge */
		break;
	case RND_SW:
		if (b.y2 >= PCB->hdr.bbox.y2 - AutoRouteParameters.bloat - DELTA && b.x1 <= AutoRouteParameters.bloat + DELTA)
			return;										/* off board edge */
		if (b.y2 >= PCB->hdr.bbox.y2 - AutoRouteParameters.bloat - DELTA)
			dir = RND_WEST;								/* south off board edge */
		if (b.x1 <= AutoRouteParameters.bloat + 1)
			dir = RND_SOUTH;							/* west off board edge */
		break;
	case RND_NW:
		if (b.y1 <= AutoRouteParameters.bloat + DELTA && b.x1 <= AutoRouteParameters.bloat + DELTA)
			return;										/* off board edge */
		if (b.y1 <= AutoRouteParameters.bloat + DELTA)
			dir = RND_WEST;								/* north off board edge */
		if (b.x1 <= AutoRouteParameters.bloat + DELTA)
			dir = RND_NORTH;							/* west off board edge */
		break;
	}

	if (!blocker) {
		edge_t *ne;
		routebox_t *nrb = CreateBridge(&b, rb, dir);
		/* move the cost point in corner expansions
		 * these boxes are bigger, so move close to the target
		 */
		if (dir == RND_NE || dir == RND_SE || dir == RND_SW || dir == RND_NW) {
			rnd_cheap_point_t p;
			p = rnd_closest_cheap_point_in_box(&nrb->cost_point, &e->minpcb_cost_target->sbox);
			p = rnd_closest_cheap_point_in_box(&p, &b);
			nrb->cost += pcb_cost_to_point_on_layer(&p, &nrb->cost_point, nrb->group);
			nrb->cost_point = p;
		}
		ne = CreateEdge(nrb, nrb->cost_point.X, nrb->cost_point.Y, nrb->cost, NULL, dir, targets);
		vector_append(result, ne);
	}
	else if (AutoRouteParameters.with_conflicts && !blocker->flags.target
					 && !blocker->flags.fixed && !blocker->flags.touched && !blocker->flags.source && blocker->type != EXPANSION_AREA) {
		edge_t *ne;
		routebox_t *nrb;
		/* make a bridge to the edge of the blocker
		 * in all directions from there
		 */
		switch (dir) {
		case RND_NORTH:
			b.y1 = blocker->sbox.y2 - DELTA;
			break;
		case RND_EAST:
			b.x2 = blocker->sbox.x1 + DELTA;
			break;
		case RND_SOUTH:
			b.y2 = blocker->sbox.y1 + DELTA;
			break;
		case RND_WEST:
			b.x1 = blocker->sbox.x2 - DELTA;
			break;
		default:
			assert(0);
		}
		if (!rnd_box_is_good(&b))
			return;										/* how did this happen ? */
		nrb = CreateBridge(&b, rb, dir);
		rnd_r_insert_entry(tree, &nrb->box);
		vector_append(area_vec, nrb);
		nrb->flags.homeless = 0;		/* not homeless any more */
		/* mark this one as conflicted */
		path_conflicts(nrb, blocker, rtrnd_true);
		/* and make an expansion edge */
		nrb->cost_point = rnd_closest_cheap_point_in_box(&nrb->cost_point, &blocker->sbox);
		nrb->cost +=
			pcb_cost_to_point_on_layer(&nrb->parent.expansion_area->cost_point, &nrb->cost_point, nrb->group) * CONFLICT_PENALTY(blocker);

		ne = CreateEdge(nrb, nrb->cost_point.X, nrb->cost_point.Y, nrb->cost, NULL, RND_ANY_DIR, targets);
		ne->flags.is_interior = 1;
		vector_append(result, ne);
	}
#if 1
	else if (blocker->type == EXPANSION_AREA) {
		if (blocker->cost < rb->cost || blocker->cost <= rb->cost +
				pcb_cost_to_point_on_layer(&blocker->cost_point, &rb->cost_point, rb->group))
			return;
		if (blocker->conflicts_with || rb->conflicts_with)
			return;
		/* does the blocker overlap this routebox ?? */
		/* does this re-parenting operation leave a memory leak? */
		if (blocker->parent.expansion_area->flags.homeless)
			RB_down_count(blocker->parent.expansion_area);
		blocker->parent.expansion_area = rb;
		return;
	}
#endif
	else if (blocker->flags.target) {
		routebox_t *nrb;
		edge_t *ne;
		b = rnd_bloat_box(&b, DELTA);
		if (!rnd_box_intersect(&b, &blocker->sbox)) {
			/* if the expansion edge stopped before touching, expand the bridge */
			switch (dir) {
			case RND_NORTH:
				b.y1 -= AutoRouteParameters.bloat + DELTA;
				break;
			case RND_EAST:
				b.x2 += AutoRouteParameters.bloat + DELTA;
				break;
			case RND_SOUTH:
				b.y2 += AutoRouteParameters.bloat + DELTA;
				break;
			case RND_WEST:
				b.x1 -= AutoRouteParameters.bloat + DELTA;
				break;
			default:
				assert(0);
			}
		}
		assert(rnd_box_intersect(&b, &blocker->sbox));
		b = rnd_shrink_box(&b, DELTA);
		nrb = CreateBridge(&b, rb, dir);
		rnd_r_insert_entry(tree, &nrb->box);
		vector_append(area_vec, nrb);
		nrb->flags.homeless = 0;		/* not homeless any more */
		ne = CreateEdge(nrb, nrb->cost_point.X, nrb->cost_point.Y, nrb->cost, blocker, dir, NULL);
		best_path_candidate(s, ne, blocker);
		DestroyEdge(&ne);
	}
}

struct break_info {
	rnd_heap_t *heap;
	routebox_t *parent;
	rtrnd_rtree_box_t box;
	rnd_direction_t dir;
	rtrnd_bool_t ignore_source;
};

static rnd_r_dir_t __GatherBlockers(const rtrnd_rtree_box_t * box, void *cl)
{
	routebox_t *rb = (routebox_t *) box;
	struct break_info *bi = (struct break_info *) cl;
	rtrnd_rtree_box_t b;

	if (bi->parent == rb || rb->flags.touched || bi->parent->parent.expansion_area == rb)
		return RND_R_DIR_NOT_FOUND;
	if (rb->flags.source && bi->ignore_source)
		return RND_R_DIR_NOT_FOUND;
	b = rb->sbox;
	if (rb->style->Clearance > AutoRouteParameters.style->Clearance)
		b = rnd_bloat_box(&b, rb->style->Clearance - AutoRouteParameters.style->Clearance);
	if (b.x2 <= bi->box.x1 || b.x1 >= bi->box.x2 || b.y1 >= bi->box.y2 || b.y2 <= bi->box.y1)
		return RND_R_DIR_NOT_FOUND;
	if (blocker_to_heap(bi->heap, rb, &bi->box, bi->dir))
		return RND_R_DIR_FOUND_CONTINUE;
	return RND_R_DIR_NOT_FOUND;
}

/* shrink the box to the last limit for the previous direction,
 * i.e. if dir is SOUTH, then this means fixing up an EAST leftover
 * edge, which would be the southern most edge for that example.
 */
static inline rtrnd_rtree_box_t previous_edge(double last, rnd_direction_t i, const rtrnd_rtree_box_t * b)
{
	rtrnd_rtree_box_t db = *b;
	switch (i) {
	case RND_EAST:
		db.x1 = last;
		break;
	case RND_SOUTH:
		db.y1 = last;
		break;
	case RND_WEST:
		db.x2 = last;
		break;
	default:
		fprintf(stderr, ERROR "previous edge bogus direction!");
		assert(0);
	}
	return db;
}

/* Break all the edges of the box that need breaking, handling
 * targets as they are found, and putting any moveable edges
 * in the return vector.
 */
vector_t *BreakManyEdges(routeone_state_t * s, rtrnd_rtree_t * targets, rtrnd_rtree_t * tree,
												 vector_t * area_vec, struct E_result * ans, routebox_t * rb, edge_t * e)
{
	struct break_info bi;
	vector_t *edges;
	rnd_heap_t *heap[4];
	double first, last;
	double bloat;
	rnd_direction_t dir;
	routebox_t fake;

	edges = vector_create();
	bi.ignore_source = rb->parent.expansion_area->flags.source;
	bi.parent = rb;
	/* we add 2 to the bloat.
	 * 1 will get us to the actual blocker that Expand() hit
	 * but 1 more is needed because the new expansion edges
	 * move out by 1 so they don't overlap their parents
	 * this extra expansion could "trap" the edge if
	 * there is a blocker 2 units from the original rb,
	 * it is 1 unit from the new expansion edge which
	 * would prevent expansion. So we want to break the
	 * edge on it now to avoid the trap.
	 */

	bloat = AutoRouteParameters.bloat + DELTA * 2;
	/* for corner expansion, we need to have a fake blocker
	 * to prevent expansion back where we came from since
	 * we still need to break portions of all 4 edges
	 */
	if (e->expand_dir == RND_NE || e->expand_dir == RND_SE || e->expand_dir == RND_SW || e->expand_dir == RND_NW) {
		rtrnd_rtree_box_t *fb = (rtrnd_rtree_box_t *) & fake.sbox;
		memset(&fake, 0, sizeof(fake));
		*fb = e->rb->sbox;
		fake.flags.fixed = 1;				/* this stops expansion there */
		fake.type = LINE;
		fake.style = AutoRouteParameters.style;
#ifndef NDEBUG
		/* the routbox_is_good checker wants a lot more! */
		fake.flags.inited = 1;
		fb = (rtrnd_rtree_box_t *) & fake.box;
		*fb = e->rb->sbox;
		fake.same_net.next = fake.same_net.prev = &fake;
		fake.same_subnet.next = fake.same_subnet.prev = &fake;
		fake.original_subnet.next = fake.original_subnet.prev = &fake;
		fake.different_net.next = fake.different_net.prev = &fake;
#endif
	}
	/* gather all of the blockers in heaps so they can be accessed
	 * in clockwise order, which allows finding corners that can
	 * be expanded.
	 */
	for (dir = RND_NORTH; dir <= RND_WEST; dir = directionIncrement(dir)) {
		int tmp;
		/* don't break the edge we came from */
		if (e->expand_dir != ((dir + 2) % 4)) {
			heap[dir] = rnd_heap_create();
			bi.box = rnd_bloat_box(&rb->sbox, bloat);
			bi.heap = heap[dir];
			bi.dir = dir;
			/* convert to edge */
			switch (dir) {
			case RND_NORTH:
				bi.box.y2 = bi.box.y1 + bloat + DELTA;
				/* for corner expansion, block the start edges and
				 * limit the blocker search to only the new edge segment
				 */
				if (e->expand_dir == RND_SE || e->expand_dir == RND_SW)
					blocker_to_heap(heap[dir], &fake, &bi.box, dir);
				if (e->expand_dir == RND_SE)
					bi.box.x1 = e->rb->sbox.x2;
				if (e->expand_dir == RND_SW)
					bi.box.x2 = e->rb->sbox.x1;
				rnd_r_search(tree, &bi.box, NULL, __GatherBlockers, &bi, &tmp);
				rb->n = tmp;
				break;
			case RND_EAST:
				bi.box.x1 = bi.box.x2 - bloat - DELTA;
				/* corner, same as above */
				if (e->expand_dir == RND_SW || e->expand_dir == RND_NW)
					blocker_to_heap(heap[dir], &fake, &bi.box, dir);
				if (e->expand_dir == RND_SW)
					bi.box.y1 = e->rb->sbox.y2;
				if (e->expand_dir == RND_NW)
					bi.box.y2 = e->rb->sbox.y1;
				rnd_r_search(tree, &bi.box, NULL, __GatherBlockers, &bi, &tmp);
				rb->e = tmp;
				break;
			case RND_SOUTH:
				bi.box.y1 = bi.box.y2 - bloat - DELTA;
				/* corner, same as above */
				if (e->expand_dir == RND_NE || e->expand_dir == RND_NW)
					blocker_to_heap(heap[dir], &fake, &bi.box, dir);
				if (e->expand_dir == RND_NE)
					bi.box.x1 = e->rb->sbox.x2;
				if (e->expand_dir == RND_NW)
					bi.box.x2 = e->rb->sbox.x1;
				rnd_r_search(tree, &bi.box, NULL, __GatherBlockers, &bi, &tmp);
				rb->s = tmp;
				break;
			case RND_WEST:
				bi.box.x2 = bi.box.x1 + bloat + DELTA;
				/* corner, same as above */
				if (e->expand_dir == RND_NE || e->expand_dir == RND_SE)
					blocker_to_heap(heap[dir], &fake, &bi.box, dir);
				if (e->expand_dir == RND_SE)
					bi.box.y1 = e->rb->sbox.y2;
				if (e->expand_dir == RND_NE)
					bi.box.y2 = e->rb->sbox.y1;
				rnd_r_search(tree, &bi.box, NULL, __GatherBlockers, &bi, &tmp);
				rb->w = tmp;
				break;
			default:
				assert(0);
			}
		}
		else
			heap[dir] = NULL;
	}
#if 1
	rb->cost += (rb->n + rb->e + rb->s + rb->w) * AutoRouteParameters.CongestionPenalty / box_area(rb->sbox);
#endif
/* now handle the blockers:
 * Go around the expansion area clockwise (North->East->South->West)
 * pulling blockers from the heap (which makes them come out in the right
 * order). Break the edges on the blocker and make the segments and corners
 * moveable as possible.
 */
	first = last = -1;
	for (dir = RND_NORTH; dir <= RND_WEST; dir = directionIncrement(dir)) {
		if (heap[dir] && !rnd_heap_is_empty(heap[dir])) {
			/* pull the very first one out of the heap outside of the
			 * heap loop because it is special; it can be part of a corner
			 */
			routebox_t *blk = (routebox_t *) rnd_heap_remove_smallest(heap[dir]);
			rtrnd_rtree_box_t b = rb->sbox;
			struct broken_boxes broke = break_box_edge(&b, dir, blk);
			if (broke.is_valid_left) {
				/* if last > 0, then the previous edge had a segment
				 * joining this one, so it forms a valid corner expansion
				 */
				if (last > 0) {
					/* make a corner expansion */
					rtrnd_rtree_box_t db = b;
					switch (dir) {
					case RND_EAST:
						/* possible NE expansion */
						db.x1 = last;
						db.y2 = RND_MIN(db.y2, broke.left.y2);
						break;
					case RND_SOUTH:
						/* possible SE expansion */
						db.y1 = last;
						db.x1 = RND_MAX(db.x1, broke.left.x1);
						break;
					case RND_WEST:
						/* possible SW expansion */
						db.x2 = last;
						db.y1 = RND_MAX(db.y1, broke.left.y1);
						break;
					default:
						assert(0);
						break;
					}
					moveable_edge(edges, &db, (rnd_direction_t) (dir + 3), rb, NULL, e, targets, s, NULL, NULL);
				}
				else if (dir == RND_NORTH) {	/* north is start, so nothing "before" it */
					/* save for a possible corner once we've
					 * finished circling the box
					 */
					first = RND_MAX(b.x1, broke.left.x2);
				}
				else {
					/* this is just a boring straight expansion
					 * since the orthogonal segment was blocked
					 */
					moveable_edge(edges, &broke.left, dir, rb, NULL, e, targets, s, NULL, NULL);
				}
			}													/* broke.is_valid_left */
			else if (last > 0) {
				/* if the last one didn't become a corner,
				 * we still want to expand it straight out
				 * in the direction of the previous edge,
				 * which it belongs to.
				 */
				rtrnd_rtree_box_t db = previous_edge(last, dir, &rb->sbox);
				moveable_edge(edges, &db, (rnd_direction_t) (dir - 1), rb, NULL, e, targets, s, NULL, NULL);
			}
			if (broke.is_valid_center && !blk->flags.source)
				moveable_edge(edges, &broke.center, dir, rb, blk, e, targets, s, tree, area_vec);
			/* this is the heap extraction loop. We break out
			 * if there's nothing left in the heap, but if we * are blocked all the way to the far edge, we can
			 * just leave stuff in the heap when it is destroyed
			 */
			while (broke.is_valid_right) {
				/* move the box edge to the next potential free point */
				switch (dir) {
				case RND_NORTH:
					last = b.x1 = RND_MAX(broke.right.x1, b.x1);
					break;
				case RND_EAST:
					last = b.y1 = RND_MAX(broke.right.y1, b.y1);
					break;
				case RND_SOUTH:
					last = b.x2 = RND_MIN(broke.right.x2, b.x2);
					break;
				case RND_WEST:
					last = b.y2 = RND_MIN(broke.right.y2, b.y2);
					break;
				default:
					assert(0);
				}
				if (rnd_heap_is_empty(heap[dir]))
					break;
				blk = (routebox_t *) rnd_heap_remove_smallest(heap[dir]);
				broke = break_box_edge(&b, dir, blk);
				if (broke.is_valid_left)
					moveable_edge(edges, &broke.left, dir, rb, NULL, e, targets, s, NULL, NULL);
				if (broke.is_valid_center && !blk->flags.source)
					moveable_edge(edges, &broke.center, dir, rb, blk, e, targets, s, tree, area_vec);
			}
			if (!broke.is_valid_right)
				last = -1;
		}
		else {											/* if (heap[dir]) */

			/* nothing touched this edge! Expand the whole edge unless
			 * (1) it hit the board edge or (2) was the source of our expansion
			 *
			 * for this case (of hitting nothing) we give up trying for corner
			 * expansions because it is likely that they're not possible anyway
			 */
			if ((e->expand_dir == RND_ANY_DIR ? e->rb->came_from : e->expand_dir) != ((dir + 2) % 4)) {
				/* ok, we are not going back on ourselves, and the whole edge seems free */
				moveable_edge(edges, &rb->sbox, dir, rb, NULL, e, targets, s, NULL, NULL);
			}

			if (last > 0) {
				/* expand the leftover from the prior direction */
				rtrnd_rtree_box_t db = previous_edge(last, dir, &rb->sbox);
				moveable_edge(edges, &db, (rnd_direction_t) (dir - 1), rb, NULL, e, targets, s, NULL, NULL);
			}
			last = -1;
		}
	}															/* for loop */
	/* finally, check for the NW corner now that we've come full circle */
	if (first > 0 && last > 0) {
		rtrnd_rtree_box_t db = rb->sbox;
		db.x2 = first;
		db.y2 = last;
		moveable_edge(edges, &db, RND_NW, rb, NULL, e, targets, s, NULL, NULL);
	}
	else {
		if (first > 0) {
			rtrnd_rtree_box_t db = rb->sbox;
			db.x2 = first;
			moveable_edge(edges, &db, RND_NORTH, rb, NULL, e, targets, s, NULL, NULL);
		}
		else if (last > 0) {
			rtrnd_rtree_box_t db = rb->sbox;
			db.y2 = last;
			moveable_edge(edges, &db, RND_WEST, rb, NULL, e, targets, s, NULL, NULL);
		}
	}
	/* done with all expansion edges of this box */
	for (dir = RND_NORTH; dir <= RND_WEST; dir = directionIncrement(dir)) {
		if (heap[dir])
			rnd_heap_destroy(&heap[dir]);
	}
	return edges;
}

static routebox_t *rb_source(routebox_t * rb)
{
	while (rb && !rb->flags.source) {
		assert(rb->type == EXPANSION_AREA);
		rb = rb->parent.expansion_area;
	}
	assert(rb);
	return rb;
}

/* ------------ */

struct foib_info {
	const rtrnd_rtree_box_t *box;
	routebox_t *intersect;
	jmp_buf env;
};

static rnd_r_dir_t foib_rect_in_reg(const rtrnd_rtree_box_t * box, void *cl)
{
	struct foib_info *foib = (struct foib_info *) cl;
	rtrnd_rtree_box_t rbox;
	routebox_t *rb = (routebox_t *) box;
	if (rb->flags.touched)
		return RND_R_DIR_NOT_FOUND;
/*  if (rb->type == EXPANSION_AREA && !rb->flags.is_via)*/
	/*   return RND_R_DIR_NOT_FOUND; */
	rbox = bloat_routebox(rb);
	if (!rnd_box_intersect(&rbox, foib->box))
		return RND_R_DIR_NOT_FOUND;
	/* this is an intersector! */
	foib->intersect = (routebox_t *) box;
	longjmp(foib->env, 1);				/* skip to the end! */
	return RND_R_DIR_FOUND_CONTINUE;
}

static routebox_t *FindOneInBox(rtrnd_rtree_t * rtree, routebox_t * rb)
{
	struct foib_info foib;
	rtrnd_rtree_box_t r;

	r = rb->sbox;
	foib.box = &r;
	foib.intersect = NULL;

	if (setjmp(foib.env) == 0)
		rnd_r_search(rtree, &r, NULL, foib_rect_in_reg, &foib, NULL);
	return foib.intersect;
}

struct therm_info {
	routebox_t *plane;
	rtrnd_rtree_box_t query;
	jmp_buf env;
};
static rnd_r_dir_t ftherm_rect_in_reg(const rtrnd_rtree_box_t * box, void *cl)
{
	routebox_t *rbox = (routebox_t *) box;
	struct therm_info *ti = (struct therm_info *) cl;
	rtrnd_rtree_box_t sq, sb;

	if (rbox->type != TERM)
		return RND_R_DIR_NOT_FOUND;
	if (rbox->group != ti->plane->group)
		return RND_R_DIR_NOT_FOUND;

	sb = shrink_routebox(rbox);
	switch (rbox->type) {
	case TERM:
	case VIA_SHADOW:
		sq = rnd_shrink_box(&ti->query, rbox->style->Diameter);
		if (!rnd_box_intersect(&sb, &sq))
			return RND_R_DIR_NOT_FOUND;
		sb.x1 = RND_BOX_CENTER_X(sb);
		sb.y1 = RND_BOX_CENTER_Y(sb);
		break;
	default:
		assert(0);
	}
	ti->plane = rbox;
	longjmp(ti->env, 1);
	return RND_R_DIR_FOUND_CONTINUE;
}

/* check for a pin or via target that a polygon can just use a thermal to connect to */
routebox_t *FindThermable(rtrnd_rtree_t * rtree, routebox_t * rb)
{
	struct therm_info info;

	info.plane = rb;
	info.query = shrink_routebox(rb);

	if (setjmp(info.env) == 0) {
		rnd_r_search(rtree, &info.query, NULL, ftherm_rect_in_reg, &info, NULL);
		return NULL;
	}
	return info.plane;
}

/*--------------------------------------------------------------------
 * Route-tracing code: once we've got a path of expansion boxes, trace
 * a line through them to actually create the connection.
 */
static void RD_DrawThermal(routedata_t * rd, double X, double Y, long group, long layer, routebox_t * subnet, rtrnd_bool_t is_bad)
{
	routebox_t *rb;
	rb = (routebox_t *) malloc(sizeof(*rb));
	memset((void *) rb, 0, sizeof(*rb));
	init_const_box(rb, X, Y, X + DELTA, Y + DELTA, 0);
	rb->group = group;
	rb->layer = layer;
	rb->flags.fixed = 0;
	rb->flags.is_bad = is_bad;
	rb->flags.is_odd = AutoRouteParameters.is_odd;
	rb->flags.circular = 0;
	rb->style = AutoRouteParameters.style;
	rb->type = THERMAL;
	InitLists(rb);
	MergeNets(rb, subnet, NET);
	MergeNets(rb, subnet, SUBNET);
	/* add it to the r-tree, this may be the whole route! */
	rnd_r_insert_entry(rd->layergrouptree[rb->group], &rb->box);
	rb->flags.homeless = 0;
}

static void RD_DrawVia(routedata_t * rd, double X, double Y, double radius, routebox_t * subnet, rtrnd_bool_t is_bad)
{
	routebox_t *rb, *first_via = NULL;
	int i;
	int ka = AutoRouteParameters.style->Clearance;

	/* a via cuts through every layer group */
	for (i = 0; i < pcb_max_group(PCB); i++) {
		if (!is_layer_group_active[i])
			continue;
		rb = (routebox_t *) malloc(sizeof(*rb));
		memset((void *) rb, 0, sizeof(*rb));
		init_const_box(rb,
									 /*X1 */ X - radius, /*Y1 */ Y - radius,
									 /*X2 */ X + radius + DELTA, /*Y2 */ Y + radius + DELTA, ka);
		rb->group = i;
		rb->flags.fixed = 0;				/* indicates that not on PCB yet */
		rb->flags.is_odd = AutoRouteParameters.is_odd;
		rb->flags.is_bad = is_bad;
		rb->came_from = RND_ANY_DIR;
		rb->flags.circular = rtrnd_true;
		rb->style = AutoRouteParameters.style;
		rb->pass = AutoRouteParameters.pass;
		if (first_via == NULL) {
			rb->type = VIA;
			rb->parent.via = NULL;		/* indicates that not on PCB yet */
			first_via = rb;
			/* only add the first via to mtspace, not the shadows too */
			mtspace_add(rd->mtspace, &rb->box, rb->flags.is_odd ? ODD : EVEN, rb->style->Clearance);
		}
		else {
			rb->type = VIA_SHADOW;
			rb->parent.via_shadow = first_via;
		}
		InitLists(rb);
		/* add these to proper subnet. */
		MergeNets(rb, subnet, NET);
		MergeNets(rb, subnet, SUBNET);
		assert(__routepcb_box_is_good(rb));
		/* and add it to the r-tree! */
		rnd_r_insert_entry(rd->layergrouptree[rb->group], &rb->box);
		rb->flags.homeless = 0;			/* not homeless anymore */
	}
}

static void
RD_DrawLine(routedata_t * rd,
						double X1, double Y1, double X2,
						double Y2, double halfthick, long group, routebox_t * subnet, rtrnd_bool_t is_bad, rtrnd_bool_t is_45)
{
	/* we hold the line in a queue to concatenate segments that
	 * ajoin one another. That reduces the number of things in
	 * the trees and allows conflict boxes to be larger, both of
	 * which are really useful.
	 */
	static double qX1 = -1, qY1, qX2, qY2;
	static double qhthick;
	static long qgroup;
	static rtrnd_bool_t qis_45, qis_bad;
	static routebox_t *qsn;

	routebox_t *rb;
	double ka = AutoRouteParameters.style->Clearance;

	/* don't draw zero-length segments. */
	if (X1 == X2 && Y1 == Y2)
		return;
	if (qX1 == -1) {							/* first ever */
		qX1 = X1;
		qY1 = Y1;
		qX2 = X2;
		qY2 = Y2;
		qhthick = halfthick;
		qgroup = group;
		qis_45 = is_45;
		qis_bad = is_bad;
		qsn = subnet;
		return;
	}
	/* Check if the lines concatenat. We only check the
	 * normal expected nextpoint=lastpoint condition
	 */
	if (X1 == qX2 && Y1 == qY2 && qhthick == halfthick && qgroup == group) {
		if (qX1 == qX2 && X1 == X2) {	/* everybody on the same X here */
			qY2 = Y2;
			return;
		}
		if (qY1 == qY2 && Y1 == Y2) {	/* same Y all around */
			qX2 = X2;
			return;
		}
	}
	/* dump the queue, no match here */
	if (qX1 == -1)
		return;											/* but not this! */
	rb = (routebox_t *) malloc(sizeof(*rb));
	memset((void *) rb, 0, sizeof(*rb));
	assert(is_45 ? (fabs(qX2 - qX1) == fabs(qY2 - qY1))	/* line must be 45-degrees */
				 : (qX1 == qX2 || qY1 == qY2) /* line must be ortho */ );
	init_const_box(rb,
								 /*X1 */ RND_MIN(qX1, qX2) - qhthick,
								 /*Y1 */ RND_MIN(qY1, qY2) - qhthick,
								 /*X2 */ RND_MAX(qX1, qX2) + qhthick + DELTA,
								 /*Y2 */ RND_MAX(qY1, qY2) + qhthick + DELTA, ka);
	rb->group = qgroup;
	rb->type = LINE;
	rb->line.x1 = qX1;
	rb->line.x2 = qX2;
	rb->line.y1 = qY1;
	rb->line.y2 = qY2;
	rb->parent.line = NULL;				/* indicates that not on PCB yet */
	rb->flags.fixed = 0;					/* indicates that not on PCB yet */
	rb->flags.is_odd = AutoRouteParameters.is_odd;
	rb->flags.is_bad = qis_bad;
	rb->came_from = RND_ANY_DIR;
	rb->flags.homeless = 0;				/* we're putting this in the tree */
	rb->flags.nonstraight = qis_45;
	rb->flags.bl_to_ur = ((qX2 >= qX1 && qY2 <= qY1)
												|| (qX2 <= qX1 && qY2 >= qY1));
	rb->style = AutoRouteParameters.style;
	rb->pass = AutoRouteParameters.pass;
	InitLists(rb);
	/* add these to proper subnet. */
	MergeNets(rb, qsn, NET);
	MergeNets(rb, qsn, SUBNET);
	assert(__routepcb_box_is_good(rb));
	/* and add it to the r-tree! */
	rnd_r_insert_entry(rd->layergrouptree[rb->group], &rb->box);

	/* and to the via space structures */
	if (AutoRouteParameters.use_vias)
		mtspace_add(rd->mtspace, &rb->box, rb->flags.is_odd ? ODD : EVEN, rb->style->Clearance);
	usedGroup[rb->group] = rtrnd_true;
	/* and queue this one */
	qX1 = X1;
	qY1 = Y1;
	qX2 = X2;
	qY2 = Y2;
	qhthick = halfthick;
	qgroup = group;
	qis_45 = is_45;
	qis_bad = is_bad;
	qsn = subnet;
}

static rtrnd_bool_t
RD_DrawManhattanLine(routedata_t * rd,
										 const rtrnd_rtree_box_t * box1, const rtrnd_rtree_box_t * box2,
										 rnd_cheap_point_t start, rnd_cheap_point_t end,
										 double halfthick, long group, routebox_t * subnet, rtrnd_bool_t is_bad, rtrnd_bool_t last_was_x)
{
	rnd_cheap_point_t knee = start;
	if (end.X == start.X) {
		RD_DrawLine(rd, start.X, start.Y, end.X, end.Y, halfthick, group, subnet, is_bad, rtrnd_false);
		return rtrnd_false;
	}
	else if (end.Y == start.Y) {
		RD_DrawLine(rd, start.X, start.Y, end.X, end.Y, halfthick, group, subnet, is_bad, rtrnd_false);
		return rtrnd_true;
	}
	/* find where knee belongs */
	if (rnd_point_in_box(box1, end.X, start.Y)
			|| rnd_point_in_box(box2, end.X, start.Y)) {
		knee.X = end.X;
		knee.Y = start.Y;
	}
	else {
		knee.X = start.X;
		knee.Y = end.Y;
	}
	if ((knee.X == end.X && !last_was_x) && (rnd_point_in_box(box1, start.X, end.Y)
																					 || rnd_point_in_box(box2, start.X, end.Y))) {
		knee.X = start.X;
		knee.Y = end.Y;
	}
	assert(AutoRouteParameters.is_smoothing || rnd_point_in_box(box1, knee.X, knee.Y)
				 || rnd_point_in_box(box2, knee.X, knee.Y));

	if (1 || !AutoRouteParameters.is_smoothing) {
		/* draw standard manhattan paths */
		RD_DrawLine(rd, start.X, start.Y, knee.X, knee.Y, halfthick, group, subnet, is_bad, rtrnd_false);
		RD_DrawLine(rd, knee.X, knee.Y, end.X, end.Y, halfthick, group, subnet, is_bad, rtrnd_false);
	}
	else {
		/* draw 45-degree path across knee */
		double len45 = RND_MIN(fabs(start.X - end.X), fabs(start.Y - end.Y));
		rnd_cheap_point_t kneestart = knee, kneeend = knee;
		if (kneestart.X == start.X)
			kneestart.Y += (kneestart.Y > start.Y) ? -len45 : len45;
		else
			kneestart.X += (kneestart.X > start.X) ? -len45 : len45;
		if (kneeend.X == end.X)
			kneeend.Y += (kneeend.Y > end.Y) ? -len45 : len45;
		else
			kneeend.X += (kneeend.X > end.X) ? -len45 : len45;
		RD_DrawLine(rd, start.X, start.Y, kneestart.X, kneestart.Y, halfthick, group, subnet, is_bad, rtrnd_false);
		RD_DrawLine(rd, kneestart.X, kneestart.Y, kneeend.X, kneeend.Y, halfthick, group, subnet, is_bad, rtrnd_true);
		RD_DrawLine(rd, kneeend.X, kneeend.Y, end.X, end.Y, halfthick, group, subnet, is_bad, rtrnd_false);
	}
	return (knee.X != end.X);
}

/* for smoothing, don't pack traces to min clearance gratuitously */
#if 0
static void add_clearance(rnd_cheap_point_t * nextpoint, const rtrnd_rtree_box_t * b)
{
	if (nextpoint->X == b->X1) {
		if (nextpoint->X + AutoRouteParameters.style->Clearance < (b->X1 + b->X2) / 2)
			nextpoint->X += AutoRouteParameters.style->Clearance;
		else
			nextpoint->X = (b->X1 + b->X2) / 2;
	}
	else if (nextpoint->X == b->X2) {
		if (nextpoint->X - AutoRouteParameters.style->Clearance > (b->X1 + b->X2) / 2)
			nextpoint->X -= AutoRouteParameters.style->Clearance;
		else
			nextpoint->X = (b->X1 + b->X2) / 2;
	}
	else if (nextpoint->Y == b->Y1) {
		if (nextpoint->Y + AutoRouteParameters.style->Clearance < (b->Y1 + b->Y2) / 2)
			nextpoint->Y += AutoRouteParameters.style->Clearance;
		else
			nextpoint->Y = (b->Y1 + b->Y2) / 2;
	}
	else if (nextpoint->Y == b->Y2) {
		if (nextpoint->Y - AutoRouteParameters.style->Clearance > (b->Y1 + b->Y2) / 2)
			nextpoint->Y -= AutoRouteParameters.style->Clearance;
		else
			nextpoint->Y = (b->Y1 + b->Y2) / 2;
	}
}
#endif

/* This back-traces the expansion boxes along the best path
 * it draws the lines that will make the actual path.
 * during refinement passes, it should try to maximize the area
 * for other tracks so routing completion is easier.
 *
 * during smoothing passes, it should try to make a better path,
 * possibly using diagonals, etc. The path boxes are larger on
 * average now so there is more possiblity to decide on a nice
 * path. Any combination of lines and arcs is possible, so long
 * as they don't poke more than half thick outside the path box.
 */

static void TracePath(routedata_t * rd, routebox_t * path, const routebox_t * target, routebox_t * subnet, rtrnd_bool_t is_bad)
{
	rtrnd_bool_t last_x = rtrnd_false;
	double halfwidth = HALF_THICK(AutoRouteParameters.style->Thickness);
	double radius = HALF_THICK(AutoRouteParameters.style->Diameter);
	rnd_cheap_point_t lastpoint, nextpoint;
	routebox_t *lastpath;
	rtrnd_rtree_box_t b;

	assert(subnet->style == AutoRouteParameters.style);
	/*XXX: because we round up odd thicknesses, there's the possibility that
	 * a connecting line end-point might be 0.005 mil off the "real" edge.
	 * don't worry about this because line *thicknesses* are always >= 0.01 mil. */

	/* if we start with a thermal the target was a plane
	 * or the target was a pin and the source a plane
	 * in which case this thermal is the whole path
	 */
	if (path->flags.is_thermal) {
		/* the target was a plane, so we need to find a good spot for the via
		 * now. It's logical to place it close to the source box which
		 * is where we're utlimately headed on this path. However, it
		 * must reside in the plane as well as the via area too.
		 */
		nextpoint.X = RND_BOX_CENTER_X(path->sbox);
		nextpoint.Y = RND_BOX_CENTER_Y(path->sbox);
		if (path->parent.expansion_area->flags.is_via) {
			TargetPoint(&nextpoint, rb_source(path));
			/* nextpoint is the middle of the source terminal now */
			b = rnd_clip_box(&path->sbox, &path->parent.expansion_area->sbox);
			nextpoint = rnd_closest_cheap_point_in_box(&nextpoint, &b);
			/* now it's in the via and plane near the source */
		}
		else {											/* no via coming, target must have been a pin */

			assert(target->type == TERM);
			TargetPoint(&nextpoint, target);
		}
		assert(rnd_point_in_box(&path->sbox, nextpoint.X, nextpoint.Y));
		RD_DrawThermal(rd, nextpoint.X, nextpoint.Y, path->group, path->layer, subnet, is_bad);
	}
	else {
		/* start from best place of target box */
		lastpoint.X = RND_BOX_CENTER_X(target->sbox);
		lastpoint.Y = RND_BOX_CENTER_Y(target->sbox);
		TargetPoint(&lastpoint, target);
		if (AutoRouteParameters.last_smooth && rnd_box_in_box(&path->sbox, &target->sbox))
			path = path->parent.expansion_area;
		b = path->sbox;
		if (path->flags.circular)
			b = rnd_shrink_box(&b, RND_MIN(b.x2 - b.x1, b.y2 - b.y1) / 5);
		nextpoint = rnd_closest_cheap_point_in_box(&lastpoint, &b);
		if (AutoRouteParameters.last_smooth)
			RD_DrawLine(rd, lastpoint.X, lastpoint.Y, nextpoint.X, nextpoint.Y, halfwidth, path->group, subnet, is_bad, rtrnd_true);
		else
			last_x = RD_DrawManhattanLine(rd, &target->sbox, &path->sbox,
																		lastpoint, nextpoint, halfwidth, path->group, subnet, is_bad, last_x);
	}
#if defined(ROUTE_DEBUG) && defined(DEBUG_SHOW_ROUTE_BOXES)
	showroutebox(path);
#if defined(ROUTE_VERBOSE)
	rnd_printf("TRACEPOINT start %#mD\n", nextpoint.X, nextpoint.Y);
#endif
#endif

	do {
		lastpoint = nextpoint;
		lastpath = path;
		assert(path->type == EXPANSION_AREA);
		path = path->parent.expansion_area;
		b = path->sbox;
		if (path->flags.circular)
			b = rnd_shrink_box(&b, RND_MIN(b.x2 - b.x1, b.y2 - b.y1) / 5);
		assert(b.x1 != b.x2 && b.y1 != b.y2);	/* need someplace to put line! */
		/* find point on path perimeter closest to last point */
		/* if source terminal, try to hit a good place */
		nextpoint = rnd_closest_cheap_point_in_box(&lastpoint, &b);
#if 0
		/* leave more clearance if this is a smoothing pass */
		if (AutoRouteParameters.is_smoothing && (nextpoint.X != lastpoint.X || nextpoint.Y != lastpoint.Y))
			add_clearance(&nextpoint, &b);
#endif
		if (path->flags.source && path->type != PLANE)
			TargetPoint(&nextpoint, path);
		assert(rnd_point_in_box(&lastpath->box, lastpoint.X, lastpoint.Y));
		assert(rnd_point_in_box(&path->box, nextpoint.X, nextpoint.Y));
#if defined(ROUTE_DEBUG_VERBOSE)
		fprintf(stderr, INFO "TRACEPATH: ");
		DumpRouteBox(path);
		fprintf(stderr, INFO "TRACEPATH: point %f %f to point %f %f layer %d\n",
							 lastpoint.X, lastpoint.Y, nextpoint.X, nextpoint.Y, path->group);
#endif

		/* draw orthogonal lines from lastpoint to nextpoint */
		/* knee is placed in lastpath box */
		/* should never cause line to leave union of lastpath/path boxes */
		if (AutoRouteParameters.last_smooth)
			RD_DrawLine(rd, lastpoint.X, lastpoint.Y, nextpoint.X, nextpoint.Y, halfwidth, path->group, subnet, is_bad, rtrnd_true);
		else
			last_x = RD_DrawManhattanLine(rd, &lastpath->sbox, &path->sbox,
																		lastpoint, nextpoint, halfwidth, path->group, subnet, is_bad, last_x);
		if (path->flags.is_via) {		/* if via, then add via */
#ifdef ROUTE_VERBOSE
			fprintf(stderr, INFO " (vias)");
#endif
			assert(rnd_point_in_box(&path->box, nextpoint.X, nextpoint.Y));
			RD_DrawVia(rd, nextpoint.X, nextpoint.Y, radius, subnet, is_bad);
		}

		assert(lastpath->flags.is_via || path->group == lastpath->group);

#if defined(ROUTE_DEBUG) && defined(DEBUG_SHOW_ROUTE_BOXES)
		showroutebox(path);
#endif /* ROUTE_DEBUG && DEBUG_SHOW_ROUTE_BOXES */
		/* if this is connected to a plane, draw the thermal */
		if (path->flags.is_thermal || path->type == PLANE)
			RD_DrawThermal(rd, lastpoint.X, lastpoint.Y, path->group, path->layer, subnet, is_bad);
		/* when one hop from the source, make an extra path in *this* box */
		if (path->type == EXPANSION_AREA && path->parent.expansion_area->flags.source && path->parent.expansion_area->type != PLANE) {
			/* find special point on source (if it exists) */
			if (TargetPoint(&lastpoint, path->parent.expansion_area)) {
				lastpoint = closest_point_in_routebox(&lastpoint, path);
				b = shrink_routebox(path);
#if 0
				if (AutoRouteParameters.is_smoothing)
					add_clearance(&lastpoint, &b);
#else
				if (AutoRouteParameters.last_smooth)
					RD_DrawLine(rd, lastpoint.X, lastpoint.Y, nextpoint.X, nextpoint.Y, halfwidth, path->group, subnet, is_bad, rtrnd_true);
				else
#endif
					last_x = RD_DrawManhattanLine(rd, &b, &b, nextpoint, lastpoint, halfwidth, path->group, subnet, is_bad, last_x);
#if defined(ROUTE_DEBUG_VERBOSE)
				fprintf(stderr, INFO "TRACEPATH: ");
				DumpRouteBox(path);
				fprintf(stderr, INFO "TRACEPATH: (to source) point %f %f to point %f %f layer %d\n",
					 nextpoint.X, nextpoint.Y, lastpoint.X, lastpoint.Y, path->group);
#endif

				nextpoint = lastpoint;
			}
		}
	}
	while (!path->flags.source);
	/* flush the line queue */
	RD_DrawLine(rd, -1, 0, 0, 0, 0, 0, NULL, rtrnd_false, rtrnd_false);

#ifdef ROUTE_DEBUG
	if (ddraw != NULL)
		ddraw->flush_debug_draw();
#endif
}

/* create a fake "edge" used to defer via site searching. */
static void
CreateSearchEdge(routeone_state_t *s, vetting_t * work, edge_t * parent,
								 routebox_t * rb, conflict_t conflict, rtrnd_rtree_t * targets, rtrnd_bool_t in_plane)
{
	routebox_t *target;
	rtrnd_rtree_box_t b;
	rnd_heap_cost_t cost;
	assert(__routepcb_box_is_good(rb));
	/* find the cheapest target */
#if 0
	target = minpcb_cost_target_to_point(&parent->cost_point, pcb_max_group(PCB) + DELTA, targets, parent->minpcb_cost_target);
#else
	target = parent->minpcb_cost_target;
#endif
	b = shrink_routebox(target);
	cost = parent->pcb_cost_to_point + AutoRouteParameters.ViaCost + pcb_cost_to_layerless_box(&rb->cost_point, 0, &b);
	if (cost < s->best_cost) {
		edge_t *ne;
		ne = (edge_t *) malloc(sizeof(*ne));
		memset((void *) ne, 0, sizeof(*ne));
		assert(ne);
		ne->flags.via_search = 1;
		ne->flags.in_plane = in_plane;
		ne->rb = rb;
		if (rb->flags.homeless)
			RB_up_count(rb);
		ne->work = work;
		ne->minpcb_cost_target = target;
		ne->flags.via_conflict_level = conflict;
		ne->pcb_cost_to_point = parent->pcb_cost_to_point;
		ne->cost_point = parent->cost_point;
		ne->cost = cost;
		rnd_heap_insert(s->workheap, ne->cost, ne);
	}
	else {
		mtsFreeWork(&work);
	}
}

static void add_or_destroy_edge(routeone_state_t *s, edge_t * e)
{
	e->cost = edge_cost(e, s->best_cost);
	assert(__edge_is_good(e));
	assert(is_layer_group_active[e->rb->group]);
	if (e->cost < s->best_cost)
		rnd_heap_insert(s->workheap, e->cost, e);
	else
		DestroyEdge(&e);
}

static void best_path_candidate(routeone_state_t *s, edge_t * e, routebox_t * best_target)
{
	e->cost = edge_cost(e, EXPENSIVE);
	if (s->best_path == NULL || e->cost < s->best_cost) {
#if defined(ROUTE_DEBUG) && defined (ROUTE_VERBOSE)
		fprintf(stderr, INFO "New best path seen! cost = %f\n", e->cost);
#endif
		/* new best path! */
		if (s->best_path && s->best_path->flags.homeless)
			RB_down_count(s->best_path);
		s->best_path = e->rb;
		s->best_target = best_target;
		s->best_cost = e->cost;
		assert(s->best_cost >= 0);
		/* don't free this when we destroy edge! */
		if (s->best_path->flags.homeless)
			RB_up_count(s->best_path);
	}
}


/* vectors for via site candidates (see mtspace.h) */
struct routeone_via_site_state {
	vector_t *free_space_vec;
	vector_t *lo_conflict_space_vec;
	vector_t *hi_conflict_space_vec;
};

void
add_via_sites(routeone_state_t *s,
							struct routeone_via_site_state *vss,
							mtspace_t * mtspace, routebox_t * within,
							conflict_t within_conflict_level, edge_t * parent_edge, rtrnd_rtree_t * targets, double shrink, rtrnd_bool_t in_plane)
{
	double radius, clearance;
	vetting_t *work;
	rtrnd_rtree_box_t region = shrink_routebox(within);
	rnd_shrink_box(&region, shrink);

	radius = HALF_THICK(AutoRouteParameters.style->Diameter);
	clearance = AutoRouteParameters.style->Clearance;
	assert(AutoRouteParameters.use_vias);
	/* XXX: need to clip 'within' to shrunk_pcb_bounds, because when
	   XXX: routing with conflicts may poke over edge. */

	/* ask for a via box near our cost_point first */
	work = mtspace_query_rect(mtspace, &region, radius, clearance,
														NULL, vss->free_space_vec,
														vss->lo_conflict_space_vec,
														vss->hi_conflict_space_vec,
														AutoRouteParameters.is_odd, AutoRouteParameters.with_conflicts, &parent_edge->cost_point);
	if (!work)
		return;
	CreateSearchEdge(s, work, parent_edge, within, within_conflict_level, targets, in_plane);
}

void
do_via_search(edge_t * search, routeone_state_t *s,
							struct routeone_via_site_state *vss, mtspace_t * mtspace, rtrnd_rtree_t * targets)
{
	int i, j, count = 0;
	double radius, clearance;
	vetting_t *work;
	routebox_t *within;
	conflict_t within_conflict_level;

	radius = HALF_THICK(AutoRouteParameters.style->Diameter);
	clearance = AutoRouteParameters.style->Clearance;
	work = mtspace_query_rect(mtspace, NULL, 0, 0,
														search->work, vss->free_space_vec,
														vss->lo_conflict_space_vec,
														vss->hi_conflict_space_vec, AutoRouteParameters.is_odd, AutoRouteParameters.with_conflicts, NULL);
	within = search->rb;
	within_conflict_level = search->flags.via_conflict_level;
	for (i = 0; i < 3; i++) {
		vector_t *v =
			(i == NO_CONFLICT ? vss->free_space_vec :
			 i == LO_CONFLICT ? vss->lo_conflict_space_vec : i == HI_CONFLICT ? vss->hi_conflict_space_vec : NULL);
		assert(v);
		while (!vector_is_empty(v)) {
			rtrnd_rtree_box_t cliparea;
			rtrnd_rtree_box_t *area = (rtrnd_rtree_box_t *) vector_remove_last(v);
			if (!(i == NO_CONFLICT || AutoRouteParameters.with_conflicts)) {
				free(area);
				continue;
			}
			/* answers are bloated by radius + clearance */
			cliparea = rnd_shrink_box(area, radius + clearance);
			free(area);
			assert(rnd_box_is_good(&cliparea));
			count++;
			for (j = 0; j < pcb_max_group(PCB); j++) {
				edge_t *ne;
				if (j == within->group || !is_layer_group_active[j])
					continue;
				ne = CreateViaEdge(&cliparea, j, within, search, within_conflict_level, (conflict_t) i, targets);
				add_or_destroy_edge(s, ne);
			}
		}
	}
	/* prevent freeing of work when this edge is destroyed */
	search->flags.via_search = 0;
	if (!work)
		return;
	CreateSearchEdge(s, work, search, within, within_conflict_level, targets, search->flags.in_plane);
	assert(vector_is_empty(vss->free_space_vec));
	assert(vector_is_empty(vss->lo_conflict_space_vec));
	assert(vector_is_empty(vss->hi_conflict_space_vec));
}

/* vector of expansion areas to be eventually removed from r-tree
 * this is a global for troubleshooting
 */
vector_t *area_vec;

/* some routines for use in gdb while debugging */
#if defined(ROUTE_DEBUG)
static void list_conflicts(routebox_t * rb)
{
	int i, n;
	if (!rb->conflicts_with)
		return;
	n = vector_size(rb->conflicts_with);
	for (i = 0; i < n; i++)
		fprintf(stderr, "%p, ", (void *)vector_element(rb->conflicts_with, i));
}

static void show_area_vec(int lay)
{
	int n, save;

	if (!area_vec)
		return;
	save = showboxen;
	showboxen = lay;
	for (n = 0; n < vector_size(area_vec); n++) {
		routebox_t *rb = (routebox_t *) vector_element(area_vec, n);
		showroutebox(rb);
	}
	showboxen = save;
}

static rtrnd_bool_t net_id(routebox_t * rb, long int id)
{
	routebox_t *p;
	LIST_LOOP(rb, same_net, p);
	if (p->flags.source && p->parent.pad->ID == id)
		return rtrnd_true;
	PCB_END_LOOP;
	return rtrnd_false;
}

static void trace_parents(routebox_t * rb)
{
	while (rb && rb->type == EXPANSION_AREA) {
		fprintf(stderr, " %p ->", (void *)rb);
		rb = rb->parent.expansion_area;
	}
	if (rb)
		fprintf(stderr, " %p is source\n", (void *)rb);
	else
		fprintf(stderr, "NULL!\n");
}

static void show_one(routebox_t * rb)
{
	int save = showboxen;
	showboxen = -1;
	showroutebox(rb);
	showboxen = save;
}

static void show_path(routebox_t * rb)
{
	while (rb && rb->type == EXPANSION_AREA) {
		show_one(rb);
		rb = rb->parent.expansion_area;
	}
	show_one(rb);
}

static void show_sources(routebox_t * rb)
{
	routebox_t *p;
	if (!rb->flags.source && !rb->flags.target) {
		fprintf(stderr, ERROR "start with a source or target please\n");
		return;
	}
	LIST_LOOP(rb, same_net, p);
	if (p->flags.source)
		show_one(p);
	PCB_END_LOOP;
}

#endif

static rnd_r_dir_t __conflict_source(const rtrnd_rtree_box_t * box, void *cl)
{
	routebox_t *rb = (routebox_t *) box;
	if (rb->flags.touched || rb->flags.fixed)
		return RND_R_DIR_NOT_FOUND;
	else {
		routebox_t *dis = (routebox_t *) cl;
		path_conflicts(dis, rb, rtrnd_false);
		touch_conflicts(dis->conflicts_with, 1);
	}
	return RND_R_DIR_FOUND_CONTINUE;
}

static void source_conflicts(rtrnd_rtree_t * tree, routebox_t * rb)
{
	if (!AutoRouteParameters.with_conflicts)
		return;
	rnd_r_search(tree, &rb->sbox, NULL, __conflict_source, rb, NULL);
	touch_conflicts(NULL, 1);
}

typedef struct routeone_status_s {
	rtrnd_bool_t found_route;
	int route_had_conflicts;
	rnd_heap_cost_t best_route_cost;
	rtrnd_bool_t net_completely_routed;
} routeone_status_t;


static routeone_status_t RouteOne(routedata_t * rd, routebox_t * from, routebox_t * to, int max_edges)
{
	routeone_status_t result;
	routebox_t *p;
	int seen, i;
	const rtrnd_rtree_box_t **target_list;
	int num_targets;
	rtrnd_rtree_t *targets;
	/* vector of source edges for filtering */
	vector_t *source_vec;
	/* working vector */
	vector_t *edge_vec;

	routeone_state_t s;
	struct routeone_via_site_state vss;

	assert(rd && from);
	result.route_had_conflicts = 0;
	/* no targets on to/from net need clearance areas */
	LIST_LOOP(from, same_net, p);
	p->flags.nobloat = 1;
	PCB_END_LOOP;
	/* set 'source' flags */
	LIST_LOOP(from, same_subnet, p);
	if (!p->flags.nonstraight)
		p->flags.source = 1;
	PCB_END_LOOP;

	/* count up the targets */
	num_targets = 0;
	seen = 0;
	/* remove source/target flags from non-straight obstacles, because they
	 * don't fill their bounding boxes and so connecting to them
	 * after we've routed is problematic.  Better solution? */
	if (to) {											/* if we're routing to a specific target */
		if (!to->flags.source) {		/* not already connected */
			/* check that 'to' and 'from' are on the same net */
			seen = 0;
#ifndef NDEBUG
			LIST_LOOP(from, same_net, p);
			if (p == to)
				seen = 1;
			PCB_END_LOOP;
#endif
			assert(seen);							/* otherwise from and to are on different nets! */
			/* set target flags only on 'to's subnet */
			LIST_LOOP(to, same_subnet, p);
			if (!p->flags.nonstraight && is_layer_group_active[p->group]) {
				p->flags.target = 1;
				num_targets++;
			}
			PCB_END_LOOP;
		}
	}
	else {
		/* all nodes on the net but not connected to from are targets */
		LIST_LOOP(from, same_net, p);
		if (!p->flags.source && is_layer_group_active[p->group]
				&& !p->flags.nonstraight) {
			p->flags.target = 1;
			num_targets++;
		}
		PCB_END_LOOP;
	}

	/* if no targets, then net is done!  reset flags and return. */
	if (num_targets == 0) {
		LIST_LOOP(from, same_net, p);
		p->flags.source = p->flags.target = p->flags.nobloat = 0;
		PCB_END_LOOP;
		result.found_route = rtrnd_false;
		result.net_completely_routed = rtrnd_true;
		result.best_route_cost = 0;
		result.route_had_conflicts = 0;

		return result;
	}
	result.net_completely_routed = rtrnd_false;

	/* okay, there's stuff to route */
	assert(!from->flags.target);
	assert(num_targets > 0);
	/* create list of target pointers and from that a r-tree of targets */
	target_list = (const rtrnd_rtree_box_t **) malloc(num_targets * sizeof(*target_list));
	i = 0;
	LIST_LOOP(from, same_net, p);
	if (p->flags.target) {
		target_list[i++] = &p->box;
#if defined(ROUTE_DEBUG) && defined(DEBUG_SHOW_TARGETS)
		showroutebox(p);
#endif
	}
	PCB_END_LOOP;
	targets = rnd_r_create_tree();
	rnd_r_insert_array(targets, (const rtrnd_rtree_box_t **)target_list, i);
	assert(i <= num_targets);
	free(target_list);

	source_vec = vector_create();
	/* touch the source subnet to prepare check for conflictors */
	LIST_LOOP(from, same_subnet, p);
	p->flags.touched = 1;
	PCB_END_LOOP;
	LIST_LOOP(from, same_subnet, p);
	{
		/* we need the test for 'source' because this box may be nonstraight */
		if (p->flags.source && is_layer_group_active[p->group]) {
			rnd_cheap_point_t cp;
			edge_t *e;
			rtrnd_rtree_box_t b = shrink_routebox(p);

#if defined(ROUTE_DEBUG) && defined(DEBUG_SHOW_SOURCES)
			showroutebox(p);
#endif
			/* may expand in all directions from source; center edge cost point. */
			/* note that planes shouldn't really expand, but we need an edge */

			cp.X = RND_BOX_CENTER_X(b);
			cp.Y = RND_BOX_CENTER_Y(b);
			e = CreateEdge(p, cp.X, cp.Y, 0, NULL, RND_ANY_DIR, targets);
			cp = rnd_closest_cheap_point_in_box(&cp, &e->minpcb_cost_target->sbox);
			cp = rnd_closest_cheap_point_in_box(&cp, &b);
			e->cost_point = cp;
			p->cost_point = cp;
			source_conflicts(rd->layergrouptree[p->group], p);
			vector_append(source_vec, e);
		}
	}
	PCB_END_LOOP;
	LIST_LOOP(from, same_subnet, p);
	p->flags.touched = 0;
	PCB_END_LOOP;
	/* break source edges; some edges may be too near obstacles to be able
	 * to exit from. */

	/* okay, main expansion-search routing loop. */
	/* set up the initial activity heap */
	s.workheap = rnd_heap_create();
	assert(s.workheap);
	while (!vector_is_empty(source_vec)) {
		edge_t *e = (edge_t *) vector_remove_last(source_vec);
		assert(is_layer_group_active[e->rb->group]);
		e->cost = edge_cost(e, EXPENSIVE);
		rnd_heap_insert(s.workheap, e->cost, e);
	}
	vector_destroy(&source_vec);
	/* okay, process items from heap until it is empty! */
	s.best_path = NULL;
	s.best_cost = EXPENSIVE;
	area_vec = vector_create();
	edge_vec = vector_create();
	vss.free_space_vec = vector_create();
	vss.lo_conflict_space_vec = vector_create();
	vss.hi_conflict_space_vec = vector_create();
	while (!rnd_heap_is_empty(s.workheap)) {
		edge_t *e = (edge_t *) rnd_heap_remove_smallest(s.workheap);
#ifdef ROUTE_DEBUG
		if (aabort)
			goto dontexpand;
#endif
		/* don't bother expanding this edge if the minimum possible edge cost
		 * is already larger than the best edge cost we've found. */
		if (s.best_path && e->cost >= s.best_cost) {
			rnd_heap_free(s.workheap, KillEdge);
			goto dontexpand;					/* skip this edge */
		}
		/* surprisingly it helps to give up and not try too hard to find
		 * a route! This is not only faster, but results in better routing.
		 * who would have guessed?
		 */
		if (seen++ > max_edges)
			goto dontexpand;
		assert(__edge_is_good(e));
		/* mark or unmark conflictors as needed */
		touch_conflicts(e->rb->conflicts_with, 1);
		if (e->flags.via_search) {
			do_via_search(e, &s, &vss, rd->mtspace, targets);
			goto dontexpand;
		}
		/* we should never add edges on inactive layer groups to the heap. */
		assert(is_layer_group_active[e->rb->group]);
#if defined(ROUTE_DEBUG) && defined(DEBUG_SHOW_EXPANSION_BOXES)
		/*showedge (e); */
#endif
		if (e->rb->flags.is_thermal) {
			best_path_candidate(&s, e, e->minpcb_cost_target);
			goto dontexpand;
		}
		/* for a plane, look for quick connections with thermals or vias */
		if (e->rb->type == PLANE) {
			routebox_t *pin = FindThermable(targets, e->rb);
			if (pin) {
				rtrnd_rtree_box_t b = shrink_routebox(pin);
				edge_t *ne;
				routebox_t *nrb;
				assert(pin->flags.target);
				nrb = CreateExpansionArea(&b, e->rb->group, e->rb, rtrnd_true, e);
				nrb->flags.is_thermal = 1;
				/* moving through the plane is free */
				e->cost_point.X = b.x1;
				e->cost_point.Y = b.y1;
				ne = CreateEdge2(nrb, e->expand_dir, e, NULL, pin);
				best_path_candidate(&s, ne, pin);
				DestroyEdge(&ne);
			}
			else {
				/* add in possible via sites in plane */
				if (AutoRouteParameters.use_vias && e->cost + AutoRouteParameters.ViaCost < s.best_cost) {
					/* we need a giant thermal */
					routebox_t *nrb = CreateExpansionArea(&e->rb->sbox, e->rb->group, e->rb,
																								rtrnd_true, e);
					edge_t *ne = CreateEdge2(nrb, e->expand_dir, e, NULL,
																	 e->minpcb_cost_target);
					nrb->flags.is_thermal = 1;
					add_via_sites(&s, &vss, rd->mtspace, nrb, NO_CONFLICT, ne, targets, e->rb->style->Diameter, rtrnd_true);
				}
			}
			goto dontexpand;					/* planes only connect via thermals */
		}
		if (e->flags.is_via) {			/* special case via */
			routebox_t *intersecting;
			assert(AutoRouteParameters.use_vias);
			assert(e->expand_dir == RND_ANY_DIR);
			assert(vector_is_empty(edge_vec));
			/* if there is already something here on this layer (like an
			 * EXPANSION_AREA), then we don't want to expand from here
			 * at least not inside the expansion area. A PLANE on the
			 * other hand may be a target, or not.
			 */
			intersecting = FindOneInBox(rd->layergrouptree[e->rb->group], e->rb);

			if (intersecting && intersecting->flags.target && intersecting->type == PLANE) {
				/* we have hit a plane */
				edge_t *ne;
				routebox_t *nrb;
				rtrnd_rtree_box_t b = shrink_routebox(e->rb);
				/* limit via region to that inside the plane */
				rnd_clip_box(&b, &intersecting->sbox);
				nrb = CreateExpansionArea(&b, e->rb->group, e->rb, rtrnd_true, e);
				nrb->flags.is_thermal = 1;
				ne = CreateEdge2(nrb, e->expand_dir, e, NULL, intersecting);
				best_path_candidate(&s, ne, intersecting);
				DestroyEdge(&ne);
				goto dontexpand;
			}
			else if (intersecting == NULL) {
				/* this via candidate is in an open area; add it to r-tree as
				 * an expansion area */
				assert(e->rb->type == EXPANSION_AREA && e->rb->flags.is_via);
				/*assert (!rnd_r_search(rd->layergrouptree[e->rb->group],
				   &e->rb->box, NULL, no_planes,0));
				 */
				rnd_r_insert_entry(rd->layergrouptree[e->rb->group], &e->rb->box);
				e->rb->flags.homeless = 0;	/* not homeless any more */
				/* add to vector of all expansion areas in r-tree */
				vector_append(area_vec, e->rb);
				/* mark reset refcount to 0, since this is not homeless any more. */
				e->rb->refcount = 0;
				/* go ahead and expand this edge! */
			}
			else if (1)
				goto dontexpand;
			else if (0) {							/* XXX: disabling this causes no via
																   collisions. */
				rtrnd_rtree_box_t a = bloat_routebox(intersecting), b;
				edge_t *ne;
				int i, j;
				/* something intersects this via candidate.  split via candidate
				 * into pieces and add these pieces to the workheap. */
				for (i = 0; i < 3; i++) {
					for (j = 0; j < 3; j++) {
						b = shrink_routebox(e->rb);
						switch (i) {
						case 0:
							b.x2 = RND_MIN(b.x2, a.x1);
							break;						/* left */
						case 1:
							b.x1 = RND_MAX(b.x1, a.x1);
							b.x2 = RND_MIN(b.x2, a.x2);
							break;						/*c */
						case 2:
							b.x1 = RND_MAX(b.x1, a.x2);
							break;						/* right */
						default:
							assert(0);
						}
						switch (j) {
						case 0:
							b.y2 = RND_MIN(b.y2, a.y1);
							break;						/* top */
						case 1:
							b.y1 = RND_MAX(b.y1, a.y1);
							b.y2 = RND_MIN(b.y2, a.y2);
							break;						/*c */
						case 2:
							b.y1 = RND_MAX(b.y1, a.y2);
							break;						/* bottom */
						default:
							assert(0);
						}
						/* skip if this box is not valid */
						if (!(b.x1 < b.x2 && b.y1 < b.y2))
							continue;
						if (i == 1 && j == 1) {
							/* this bit of the via space is obstructed. */
							if (intersecting->type == EXPANSION_AREA || intersecting->flags.fixed)
								continue;				/* skip this bit, it's already been done. */
							/* create an edge with conflicts, if enabled */
							if (!AutoRouteParameters.with_conflicts)
								continue;
							ne = CreateEdgeWithConflicts(&b, intersecting, e, 1
																					 /*cost penalty to box */
																					 , targets);
							add_or_destroy_edge(&s, ne);
						}
						else {
							/* if this is not the intersecting piece, create a new
							 * (hopefully unobstructed) via edge and add it back to the
							 * workheap. */
							ne = CreateViaEdge(&b, e->rb->group, e->rb->parent.expansion_area, e, e->flags.via_conflict_level, NO_CONFLICT
																 /* value here doesn't matter */
																 , targets);
							add_or_destroy_edge(&s, ne);
						}
					}
				}
				goto dontexpand;
			}
			/* between the time these edges are inserted and the
			 * time they are processed, new expansion boxes (which
			 * conflict with these edges) may be added to the graph!
			 * w.o vias this isn't a problem because the broken box
			 * is not homeless. */
		}
		if (1) {
			routebox_t *nrb;
			struct E_result *ans;
			rtrnd_rtree_box_t b;
			vector_t *broken;
			if (e->flags.is_interior) {
				assert(AutoRouteParameters.with_conflicts);	/* no interior edges unless
																										   routing with conflicts! */
				assert(e->rb->conflicts_with);
				b = e->rb->sbox;
				switch (e->rb->came_from) {
				case RND_NORTH:
					b.y2 = b.y1 + DELTA;
					b.x1 = RND_BOX_CENTER_X(b);
					b.x2 = b.x1 + DELTA;
					break;
				case RND_EAST:
					b.x1 = b.x2 - DELTA;
					b.y1 = RND_BOX_CENTER_Y(b);
					b.y2 = b.y1 + DELTA;
					break;
				case RND_SOUTH:
					b.y1 = b.y2 - DELTA;
					b.x1 = RND_BOX_CENTER_X(b);
					b.x2 = b.x1 + DELTA;
					break;
				case RND_WEST:
					b.x2 = b.x1 + DELTA;
					b.y1 = RND_BOX_CENTER_Y(b);
					b.y2 = b.y1 + DELTA;
					break;
				default:
					assert(0);
				}
			}
			/* sources may not expand to their own edges because of
			 * adjacent blockers.
			 */
			else if (e->rb->flags.source)
				b = rnd_box_center(&e->rb->sbox);
			else
				b = e->rb->sbox;
			ans = Expand(rd->layergrouptree[e->rb->group], e, &b);
			if (!rnd_box_intersect(&ans->inflated, &ans->orig))
				goto dontexpand;
#if 0
			/* skip if it didn't actually expand */
			if (ans->inflated.x1 >= e->rb->sbox.x1 &&
					ans->inflated.x2 <= e->rb->sbox.x2 && ans->inflated.y1 >= e->rb->sbox.y1 && ans->inflated.y2 <= e->rb->sbox.y2)
				goto dontexpand;
#endif

			if (!rnd_box_is_good(&ans->inflated))
				goto dontexpand;
			nrb = CreateExpansionArea(&ans->inflated, e->rb->group, e->rb, rtrnd_true, e);
			rnd_r_insert_entry(rd->layergrouptree[nrb->group], &nrb->box);
			vector_append(area_vec, nrb);
			nrb->flags.homeless = 0;	/* not homeless any more */
			broken = BreakManyEdges(&s, targets, rd->layergrouptree[nrb->group], area_vec, ans, nrb, e);
			while (!vector_is_empty(broken)) {
				edge_t *ne = (edge_t *) vector_remove_last(broken);
				add_or_destroy_edge(&s, ne);
			}
			vector_destroy(&broken);

			/* add in possible via sites in nrb */
			if (AutoRouteParameters.use_vias && !e->rb->flags.is_via && e->cost + AutoRouteParameters.ViaCost < s.best_cost)
				add_via_sites(&s, &vss, rd->mtspace, nrb, NO_CONFLICT, e, targets, 0, rtrnd_false);
			goto dontexpand;
		}
	dontexpand:
		DestroyEdge(&e);
	}
	touch_conflicts(NULL, 1);
	rnd_heap_destroy(&s.workheap);
	rnd_r_destroy_tree(&targets);
	assert(vector_is_empty(edge_vec));
	vector_destroy(&edge_vec);

	/* we should have a path in best_path now */
	if (s.best_path) {
		routebox_t *rb;
#ifdef ROUTE_VERBOSE
		fprintf(stderr, INFO "%d:%d RC %.0f", ro++, seen, s.best_cost);
#endif
		result.found_route = rtrnd_true;
		result.best_route_cost = s.best_cost;
		/* determine if the best path had conflicts */
		result.route_had_conflicts = 0;
		if (AutoRouteParameters.with_conflicts && s.best_path->conflicts_with) {
			while (!vector_is_empty(s.best_path->conflicts_with)) {
				rb = (routebox_t *) vector_remove_last(s.best_path->conflicts_with);
				rb->flags.is_bad = 1;
				result.route_had_conflicts++;
			}
		}
#ifdef ROUTE_VERBOSE
		if (result.route_had_conflicts)
			fprintf(stderr, " (%d conflicts)", result.route_had_conflicts);
#endif
		if (result.route_had_conflicts < AutoRouteParameters.hi_conflict) {
			/* back-trace the path and add lines/vias to r-tree */
			TracePath(rd, s.best_path, s.best_target, from, result.route_had_conflicts);
			MergeNets(from, s.best_target, SUBNET);
		}
		else {
#ifdef ROUTE_VERBOSE
			fprintf(stderr, " (too many in fact)");
#endif
			result.found_route = rtrnd_false;
		}
#ifdef ROUTE_VERBOSE
		fprintf(stderr, "\n");
#endif
	}
	else {
#ifdef ROUTE_VERBOSE
		fprintf(stderr, INFO "%d:%d NO PATH FOUND.\n", ro++, seen);
#endif
		result.best_route_cost = s.best_cost;
		result.found_route = rtrnd_false;
	}
	/* now remove all expansion areas from the r-tree. */
	while (!vector_is_empty(area_vec)) {
		routebox_t *rb = (routebox_t *) vector_remove_last(area_vec);
		assert(!rb->flags.homeless);
		if (rb->conflicts_with && rb->parent.expansion_area->conflicts_with != rb->conflicts_with)
			vector_destroy(&rb->conflicts_with);
		rnd_r_delete_entry_free_data(rd->layergrouptree[rb->group], &rb->box, free);
	}
	vector_destroy(&area_vec);
	/* clean up; remove all 'source', 'target', and 'nobloat' flags */
	LIST_LOOP(from, same_net, p);
	if (p->flags.source && p->conflicts_with)
		vector_destroy(&p->conflicts_with);
	p->flags.touched = p->flags.source = p->flags.target = p->flags.nobloat = 0;
	PCB_END_LOOP;

	vector_destroy(&vss.free_space_vec);
	vector_destroy(&vss.lo_conflict_space_vec);
	vector_destroy(&vss.hi_conflict_space_vec);

	return result;
}

static void InitAutoRouteParameters(int pass, pcb_route_style_t * style, rtrnd_bool_t with_conflicts, rtrnd_bool_t is_smoothing, rtrnd_bool_t lastpass)
{
	int i;
	/* routing style */
	AutoRouteParameters.style = style;
	AutoRouteParameters.bloat = style->Clearance + HALF_THICK(style->Thickness);
	/* costs */
	AutoRouteParameters.ViaCost = 88.9 + style->Diameter * (is_smoothing ? 80 : 30);
	AutoRouteParameters.LastConflictPenalty = ((400 * pass / passes + 2) / (pass + 1)) / 1000000.0;
	AutoRouteParameters.ConflictPenalty = (4 * AutoRouteParameters.LastConflictPenalty) / 1000000.0;
	AutoRouteParameters.JogPenalty = (1000 * (is_smoothing ? 20 : 4)) / 1000000.0;
	AutoRouteParameters.CongestionPenalty = 1e6 / 1000000.0;
	AutoRouteParameters.MinPenalty = EXPENSIVE;
	for (i = 0; i < pcb_max_group(PCB); i++) {
		if (is_layer_group_active[i]) {
			AutoRouteParameters.MinPenalty = RND_MIN(x_cost[i], AutoRouteParameters.MinPenalty);
			AutoRouteParameters.MinPenalty = RND_MIN(y_cost[i], AutoRouteParameters.MinPenalty);
		}
	}
	AutoRouteParameters.NewLayerPenalty = is_smoothing ? 0.5 * EXPENSIVE : 10 * AutoRouteParameters.ViaCost;
	/* other */
	AutoRouteParameters.hi_conflict = RND_MAX(8 * (passes - pass + 1), 6);
	AutoRouteParameters.is_odd = (pass & 1);
	AutoRouteParameters.with_conflicts = with_conflicts;
	AutoRouteParameters.is_smoothing = is_smoothing;
	AutoRouteParameters.rip_always = is_smoothing;
	AutoRouteParameters.last_smooth = 0;	/*lastpass; */
	AutoRouteParameters.pass = pass + 1;
}

#ifndef NDEBUG
rnd_r_dir_t bad_boy(const rtrnd_rtree_box_t * b, void *cl)
{
	routebox_t *box = (routebox_t *) b;
	if (box->type == EXPANSION_AREA)
		return RND_R_DIR_FOUND_CONTINUE;
	return RND_R_DIR_NOT_FOUND;
}

rtrnd_bool_t no_expansion_boxes(routedata_t * rd)
{
	int i;
	rtrnd_rtree_box_t big;
	big.x1 = 0;
	big.x2 = RND_MAX_COORD;
	big.y1 = 0;
	big.y2 = RND_MAX_COORD;
	for (i = 0; i < pcb_max_group(PCB); i++) {
		if (rnd_r_search(rd->layergrouptree[i], &big, NULL, bad_boy, NULL, NULL))
			return rtrnd_false;
	}
	return rtrnd_true;
}
#endif


struct routeall_status {
	/* --- for completion rate statistics ---- */
	int total_subnets;
	/* total subnets routed without conflicts */
	int routed_subnets;
	/* total subnets routed with conflicts */
	int conflict_subnets;
	/* net failted entirely */
	int failed;
	/* net was ripped */
	int ripped;
	int total_nets_routed;
};

static double calculate_progress(double this_heap_item, double this_heap_size, struct routeall_status *ras)
{
	double total_passes = passes + smoothes + 1;	/* + 1 is the refinement pass */
	double this_pass = AutoRouteParameters.pass - 1;	/* Number passes from zero */
	double heap_fraction = (double) (ras->routed_subnets + ras->conflict_subnets + ras->failed) / (double) ras->total_subnets;
	double pass_fraction = (this_heap_item + heap_fraction) / this_heap_size;
	double process_fraction = (this_pass + pass_fraction) / total_passes;

	return process_fraction;
}

struct routeall_status RouteAll(routedata_t * rd)
{
	struct routeall_status ras;
	routeone_status_t ros;
	rtrnd_bool_t rip;
	int request_cancel;
#ifdef NET_HEAP
	rnd_heap_t *net_heap;
#endif
	rnd_heap_t *this_pass, *next_pass, *tmp;
	routebox_t *net, *p, *pp;
	rnd_heap_cost_t total_net_cost, last_cost = 0, this_cost = 0;
	int i;
	int this_heap_size;
	int this_heap_item;

	/* initialize heap for first pass;
	 * do smallest area first; that makes
	 * the subsequent costs more representative */
	this_pass = rnd_heap_create();
	next_pass = rnd_heap_create();
#ifdef NET_HEAP
	net_heap = rnd_heap_create();
#endif
	LIST_LOOP(rd->first_net, different_net, net);
	{
		double area;
		rtrnd_rtree_box_t bb = shrink_routebox(net);
		LIST_LOOP(net, same_net, p);
		{
			RND_MAKE_MIN(bb.x1, p->sbox.x1);
			RND_MAKE_MIN(bb.y1, p->sbox.y1);
			RND_MAKE_MAX(bb.x2, p->sbox.x2);
			RND_MAKE_MAX(bb.y2, p->sbox.y2);
		}
		PCB_END_LOOP;
		area = (double) (bb.x2 - bb.x1) * (bb.y2 - bb.y1);
		rnd_heap_insert(this_pass, area, net);
	}
	PCB_END_LOOP;

	ras.total_nets_routed = 0;
	/* refinement/finishing passes */
	for (i = 0; i <= passes + smoothes; i++) {
#ifdef ROUTE_VERBOSE
		if (i > 0 && i <= passes)
			fprintf(stderr, INFO "--------- STARTING REFINEMENT PASS %d ------------\n", i);
		else if (i > passes)
			fprintf(stderr, INFO "--------- STARTING SMOOTHING PASS %d -------------\n", i - passes);
#endif
		ras.total_subnets = ras.routed_subnets = ras.conflict_subnets = ras.failed = ras.ripped = 0;
		assert(rnd_heap_is_empty(next_pass));

		this_heap_size = rnd_heap_size(this_pass);
		for (this_heap_item = 0; !rnd_heap_is_empty(this_pass); this_heap_item++) {
#ifdef ROUTE_DEBUG
			if (aabort)
				break;
#endif
			net = (routebox_t *) rnd_heap_remove_smallest(this_pass);
			InitAutoRouteParameters(i, net->style, i < passes, i > passes, i == passes + smoothes);
			if (i > 0) {
				/* rip up all unfixed traces in this net ? */
				if (AutoRouteParameters.rip_always)
					rip = rtrnd_true;
				else {
					rip = rtrnd_false;
					LIST_LOOP(net, same_net, p);
					if (p->flags.is_bad) {
						rip = rtrnd_true;
						break;
					}
					PCB_END_LOOP;
				}

				LIST_LOOP(net, same_net, p);
				p->flags.is_bad = 0;
				if (!p->flags.fixed) {
#ifndef NDEBUG
					rtrnd_bool_t del;
#endif
					assert(!p->flags.homeless);
					if (rip) {
						RemoveFromNet(p, NET);
						RemoveFromNet(p, SUBNET);
					}
					if (AutoRouteParameters.use_vias && p->type != VIA_SHADOW && p->type != PLANE) {
						mtspace_remove(rd->mtspace, &p->box, p->flags.is_odd ? ODD : EVEN, p->style->Clearance);
						if (!rip)
							mtspace_add(rd->mtspace, &p->box, p->flags.is_odd ? EVEN : ODD, p->style->Clearance);
					}
					if (rip) {
#ifndef NDEBUG
						del =
#endif
							rnd_r_delete_entry_free_data(rd->layergrouptree[p->group], &p->box, free);
#ifndef NDEBUG
						assert(del);
#endif
					}
					else {
						p->flags.is_odd = AutoRouteParameters.is_odd;
					}
				}
				PCB_END_LOOP;
				/* reset to original connectivity */
				if (rip) {
					ras.ripped++;
					ResetSubnet(net);
				}
				else {
					rnd_heap_insert(next_pass, 0, net);
					continue;
				}
			}
			/* count number of subnets */
			FOREACH_SUBNET(net, p);
			ras.total_subnets++;
			END_FOREACH(net, p);
			/* the first subnet doesn't require routing. */
			ras.total_subnets--;
			/* and re-route! */
			total_net_cost = 0;
			/* only route that which isn't fully routed */
#ifdef ROUTE_DEBUG
			if (ras.total_subnets == 0 || aabort)
#else
			if (ras.total_subnets == 0)
#endif
			{
				rnd_heap_insert(next_pass, 0, net);
				continue;
			}

			/* the loop here ensures that we get to all subnets even if
			 * some of them are unreachable from the first subnet. */
			LIST_LOOP(net, same_net, p);
			{
#ifdef NET_HEAP
				rtrnd_rtree_box_t b = shrink_routebox(p);
				/* using a heap allows us to start from smaller objects and
				 * end at bigger ones. also prefer to start at planes, then pads */
				rnd_heap_insert(net_heap, (float) (b.x2 - b.x1) *
#if defined(ROUTE_RANDOMIZED)
										(0.3 + rnd_rand() / (RAND_MAX + 1.0)) *
#endif
										(b.y2 - b.y1) * (p->type == PLANE ? -1 : ((p->type == TERM) ? 1 : 10)), p);
			}
			PCB_END_LOOP;
			ros.net_completely_routed = 0;
			while (!rnd_heap_is_empty(net_heap)) {
				p = (routebox_t *) rnd_heap_remove_smallest(net_heap);
#endif
				if (!p->flags.fixed || p->flags.subnet_processed || p->type == OTHER)
					continue;

				while (!ros.net_completely_routed) {
					double percent;

					assert(no_expansion_boxes(rd));
					/* FIX ME: the number of edges to examine should be in autoroute parameters
					 * i.e. the 2000 and 800 hard-coded below should be controllable by the user
					 */
					ros = RouteOne(rd, p, NULL, ((AutoRouteParameters.is_smoothing ? 2000 : 800) * (i + 1)) * routing_layers);
					total_net_cost += ros.best_route_cost;
					if (ros.found_route) {
						if (ros.route_had_conflicts)
							ras.conflict_subnets++;
						else {
							ras.routed_subnets++;
							ras.total_nets_routed++;
						}
					}
					else {
						if (!ros.net_completely_routed)
							ras.failed++;
						/* don't bother trying any other source in this subnet */
						LIST_LOOP(p, same_subnet, pp);
						pp->flags.subnet_processed = 1;
						PCB_END_LOOP;
						break;
					}
					/* note that we can infer nothing about ras.total_subnets based
					 * on the number of calls to RouteOne, because we may be unable
					 * to route a net from a particular starting point, but perfectly
					 * able to route it from some other. */
					percent = calculate_progress(this_heap_item, this_heap_size, &ras);
					request_cancel = rtrnd_progress(rd->ctx, percent);
					if (request_cancel) {
						ras.total_nets_routed = 0;
						ras.conflict_subnets = 0;
						goto out;
					}
				}
			}
#ifndef NET_HEAP
			PCB_END_LOOP;
#endif
			if (!ros.net_completely_routed)
				net->flags.is_bad = 1;	/* don't skip this the next round */

			/* Route easiest nets from this pass first on next pass.
			 * This works best because it's likely that the hardest
			 * is the last one routed (since it has the most obstacles)
			 * but it will do no good to rip it up and try it again
			 * without first changing any of the other routes
			 */
			rnd_heap_insert(next_pass, total_net_cost, net);
			if (total_net_cost < EXPENSIVE)
				this_cost += total_net_cost;
			/* reset subnet_processed flags */
			LIST_LOOP(net, same_net, p);
			{
				p->flags.subnet_processed = 0;
			}
			PCB_END_LOOP;
		}
		/* swap this_pass and next_pass and do it all over again! */
		ro = 0;
		assert(rnd_heap_is_empty(this_pass));
		tmp = this_pass;
		this_pass = next_pass;
		next_pass = tmp;
#if defined(ROUTE_DEBUG) || defined (ROUTE_VERBOSE)
		fprintf(stderr, INFO "END OF PASS %d: %d/%d subnets routed without conflicts at cost %.0f, %d conflicts, %d failed %d ripped\n",
			 i, ras.routed_subnets, ras.total_subnets, this_cost, ras.conflict_subnets, ras.failed, ras.ripped);
#endif
#ifdef ROUTE_DEBUG
		if (aabort)
			break;
#endif
		/* if no conflicts found, skip directly to smoothing pass! */
		if (ras.conflict_subnets == 0 && ras.routed_subnets == ras.total_subnets && i <= passes)
			i = passes - (smoothes ? 0 : 1);
		/* if no changes in a smoothing round, then we're done */
		if (this_cost == last_cost && i > passes && i < passes + smoothes)
			i = passes + smoothes - 1;
		last_cost = this_cost;
		this_cost = 0;
	}

#warning TODO: pass this back
/*	rnd_message(RND_MSG_INFO, "%d of %d nets successfully routed.\n", ras.routed_subnets, ras.total_subnets);*/

out:
	rnd_heap_destroy(&this_pass);
	rnd_heap_destroy(&next_pass);
#ifdef NET_HEAP
	rnd_heap_destroy(&net_heap);
#endif

	/* no conflicts should be left at the end of the process. */
	assert(ras.conflict_subnets == 0);

	return ras;
}

struct fpin_info {
	rtrnd_via_t *ps;
	double x, y;
	jmp_buf env;
};

static rnd_r_dir_t fpstk_rect(const rtrnd_rtree_box_t * b, void *cl)
{
	rtrnd_via_t *ps = (rtrnd_via_t *)b;
	struct fpin_info *info = (struct fpin_info *) cl;
	if (ps->x == info->x && ps->y == info->y) {
		info->ps = ps;
		longjmp(info->env, 1);
	}
	return RND_R_DIR_NOT_FOUND;
}

static int FindPin(const rtrnd_rtree_box_t *box, rtrnd_via_t **ps_out)
{
	struct fpin_info info;

	info.ps = NULL;
	info.x = box->x1;
	info.y = box->y1;
	if (setjmp(info.env) == 0) {
		rnd_r_search(&PCB->vias, box, NULL, fpstk_rect, &info, NULL);
	}
	else {
		*ps_out = info.ps;
		return RTRND_VIA;
	}

	*ps_out = NULL;
	return RTRND_VOID;
}


/* paths go on first 'on' layer in group */
/* returns 'rtrnd_true' if any paths were added. */
rtrnd_bool_t IronDownAllUnfixedPaths(routedata_t * rd)
{
	rtrnd_bool_t changed = rtrnd_false;
	routebox_t *net, *p;
	int i;
	LIST_LOOP(rd->first_net, different_net, net);
	{
		LIST_LOOP(net, same_net, p);
		{
			if (!p->flags.fixed) {
				rtrnd_net_t *rnet = NULL;
				if (net->ns != NULL)
					rnet = net->ns->net;

				/* find first on layer in this group */
				assert(is_layer_group_active[p->group]);
				assert(p->type != EXPANSION_AREA);
				if (p->type == LINE) {
					double halfwidth = HALF_THICK(p->style->Thickness);
					double th = halfwidth * 2 + DELTA;
					rtrnd_rtree_box_t b;
					assert(p->parent.line == NULL);
					/* orthogonal; thickness is 2*halfwidth */
					/* flip coordinates, if bl_to_ur */
					b = p->sbox;
					total_wire_length += sqrt((b.x2 - b.x1 - th) * (b.x2 - b.x1 - th) + (b.y2 - b.y1 - th) * (b.y2 - b.y1 - th));
					b = rnd_shrink_box(&b, halfwidth);
					if (b.x2 == b.x1 + DELTA)
						b.x2 = b.x1;
					if (b.y2 == b.y1 + DELTA)
						b.y2 = b.y1;
					if (p->flags.bl_to_ur) {
						double t;
						t = b.x1;
						b.x1 = b.x2;
						b.x2 = t;
					}

					/* using CreateDrawn instead of CreateNew concatenates sequential lines */
					rtrnd_draw_res_line(rd->ctx, PCB->layers.array[p->group], rd->annot[p->group], rnet,
						p->line.x1, p->line.y1, p->line.x2, p->line.y2, p->style->Thickness, p->style->Clearance,
						p->style->Thickness, p->style->Clearance);
					changed = rtrnd_true;
				}
				else if (p->type == VIA || p->type == VIA_SHADOW) {
					routebox_t *pp = (p->type == VIA_SHADOW) ? p->parent.via_shadow : p;
					double radius = HALF_THICK(pp->style->Diameter);
					rtrnd_rtree_box_t b = shrink_routebox(p);
					total_via_count++;
					assert(pp->type == VIA);
					if (pp->parent.via == NULL) {
						assert(labs((b.x1 + radius) - (b.x2 - radius)) < 2);
						assert(labs((b.y1 + radius) - (b.y2 - radius)) < 2);
						pp->parent.via = 1;
						rtrnd_draw_res_via(rd->ctx, rd->annot_via, rnet,
							b.x1 + radius, b.y1 + radius,
							pp->style->Diameter, pp->style->Clearance, pp->style->Diameter, pp->style->Clearance);
						changed = rtrnd_true;
					}
					assert(pp->parent.via);
					if (p->type == VIA_SHADOW) {
						p->type = VIA;
						p->parent.via = pp->parent.via;
					}
				}
				else if (p->type == THERMAL)
					/* nothing to do because, the via might not be there yet */ ;
				else
					assert(0);
			}
		}
		PCB_END_LOOP;
		/* loop again to place all the thermals now that the vias are down */
		LIST_LOOP(net, same_net, p);
		{
			if (p->type == THERMAL) {
				rtrnd_via_t *pin = NULL;
				/* thermals are alread a single point search, no need to shrink */
				int type = FindPin(&p->box, &pin);
				if (pin) {
#warning TODO: add thermal
#if 0
					pcb_undo_add_obj_to_clear_poly(type, pin->parent.data, pin, pin, rtrnd_false);
					pcb_poly_restore_to_poly(PCB->Data, PCB_OBJ_PSTK, pcb_get_layer(PCB->Data, p->layer), pin);
					pcb_undo_add_obj_to_flag(pin);
					PCB_FLAG_THERM_ASSIGN(p->layer, autoroute_therm_style, pin);
					pcb_undo_add_obj_to_clear_poly(type, pin->parent.data, pin, pin, rtrnd_true);
					pcb_poly_clear_from_poly(PCB->Data, PCB_OBJ_PSTK, pcb_get_layer(PCB->Data, p->layer), pin);
#endif
					changed = rtrnd_true;
				}
			}
		}
		PCB_END_LOOP;
	}
	PCB_END_LOOP;
	return changed;
}

RTRND_INLINE int group_of_obj(rtrnd_any_obj_t *o)
{
	switch(o->hdr.type) {
		case RTRND_LINE:
		case RTRND_ARC:
		case RTRND_POLY:
			return layer_id(&o->hdr.parent->layer);
		default:;
	}
	return 0;
}

static rtrnd_bool_t route_hace(rtrnd_t *ctx, rtrnd_bool_t selected)
{
	rtrnd_bool_t changed = rtrnd_false;
	routedata_t *rd;
	rtrnd_rat_t *rat;
	int i;

	PCB = ctx->board;
	total_wire_length = 0;
	total_via_count = 0;

#ifdef ROUTE_DEBUG
	ddraw = rnd_gui->request_debug_draw();
	if (ddraw != NULL) {
		ar_gc = ddraw->make_gc();
		ddraw->set_line_cap(ar_gc, rnd_cap_round);
	}
#endif

	rd = CreateRouteData(ctx);
	if (rd == NULL) {
		fprintf(stderr, ERROR "Failed to initialize data; might be missing\n" "top or bottom copper layer.\n");
		return rtrnd_false;
	}

	rtrnd_ratsnest_map(&rd->nest, ctx->board, 0);
	rd->annot_rats = rtrnd_annot_new(ctx, "rat");
	strcpy(rd->annot_rats->color, "#cb9800");
	rtrnd_ratsnest_draw(&rd->nest, rd->annot_rats);

	if (1) {
		routebox_t *net, *rb, *last;
		int i = rd->nest.lst.length;
		
#ifdef ROUTE_VERBOSE
		fprintf(stderr, INFO "%d nets!\n", i);
#endif
		if (i == 0)
			goto donerouting;					/* nothing to do here */
		/* if only one rat selected, do things the quick way. =) */
		if (i == 1) {
				/* look up the end points of this rat line */
				routebox_t *a;
				routebox_t *b;

				rat = gdl_first(&rd->nest.lst);

				a = FindRouteBoxOnLayerGroup(rd, rat->x[0], rat->y[0], group_of_obj(rat->o[0]));
				b = FindRouteBoxOnLayerGroup(rd, rat->x[1], rat->y[1], group_of_obj(rat->o[1]));
				assert(a != NULL && b != NULL);
				assert(a->style == b->style);
/*
	      if (a->type != PAD && b->type == PAD)
	        {
	          routebox_t *t = a;
		  a = b;
		  b = t;
	        }
*/
				/* route exactly one net, without allowing conflicts */
				InitAutoRouteParameters(0, a->style, rtrnd_false, rtrnd_true, rtrnd_true);
				/* hace planes work better as sources than targets */
				changed = RouteOne(rd, a, b, 150000).found_route || changed;
				goto donerouting;
		}

		/* otherwise, munge the netlists so that only the selected rats
		 * get connected. */
		/* first, separate all sub nets into separate nets */
		/* note that this code works because LIST_LOOP is clever enough not to
		 * be fooled when the list is changing out from under it. */
		last = NULL;
		LIST_LOOP(rd->first_net, different_net, net);
		{
			FOREACH_SUBNET(net, rb);
			{
				if (last) {
					last->different_net.next = rb;
					rb->different_net.prev = last;
				}
				last = rb;
			}
			END_FOREACH(net, rb);
			LIST_LOOP(net, same_net, rb);
			{
				rb->same_net = rb->same_subnet;
			}
			PCB_END_LOOP;
			/* at this point all nets are equal to their subnets */
		}
		PCB_END_LOOP;
		if (last) {
			last->different_net.next = rd->first_net;
			rd->first_net->different_net.prev = last;
		}

		/* now merge only those subnets connected by a rat line */
		for(rat = gdl_first(&rd->nest.lst); rat != NULL; rat = gdl_next(&rd->nest.lst, rat)) {
			/* look up the end points of this rat line */
			routebox_t *a = FindRouteBoxOnLayerGroup(rd, rat->x[0], rat->y[0], group_of_obj(rat->o[0]));
			routebox_t *b = FindRouteBoxOnLayerGroup(rd, rat->x[1], rat->y[1], group_of_obj(rat->o[1]));

			if (!a || !b) {
				fprintf(stderr, ERROR "The rats nest is stale! Aborting...\n");
				assert(!"stale rats");
				goto donerouting;
			}
			/* merge subnets into a net! */
			MergeNets(a, b, NET);
		}

		/* now 'different_net' may point to too many different nets.  Reset. */
		LIST_LOOP(rd->first_net, different_net, net);
		{
			if (!net->flags.touched) {
				LIST_LOOP(net, same_net, rb);
				rb->flags.touched = 1;
				PCB_END_LOOP;
			}
			else											/* this is not a "different net"! */
				RemoveFromNet(net, DIFFERENT_NET);
		}
		PCB_END_LOOP;
		/* reset "touched" flag */
		LIST_LOOP(rd->first_net, different_net, net);
		{
			LIST_LOOP(net, same_net, rb);
			{
				assert(rb->flags.touched);
				rb->flags.touched = 0;
			}
			PCB_END_LOOP;
		}
		PCB_END_LOOP;
	}
	rtrnd_ratsnest_uninit(&rd->nest);

	/* okay, rd's idea of netlist now corresponds to what we want routed */
	/* auto-route all nets */
	changed = (RouteAll(rd).total_nets_routed > 0) || changed;
donerouting:
	rtrnd_progress(ctx, 1);

#ifdef ROUTE_DEBUG
	if (ddraw != NULL)
		ddraw->finish_debug_draw();
#endif

	if (changed)
		changed = IronDownAllUnfixedPaths(rd);
#warning TODO: pass this back:
/*	rnd_message(RND_MSG_INFO, "Total added wire length = %$mS, %d vias added\n", (double) total_wire_length, total_via_count);*/
	DestroyRouteData(&rd);

#if defined (ROUTE_DEBUG)
	aabort = 0;
#endif
	return !changed;
}

static const rtrnd_router_t rt_hace = {
	"hace", "gridless rectangle-expansion router",
	hace_cfg_desc,
	route_hace
};

void rt_hace_init(void)
{
	vtp0_append(&rtrnd_all_router, (void *)&rt_hace);
}
