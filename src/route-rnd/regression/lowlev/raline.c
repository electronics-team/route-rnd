#include <stdio.h>
#include <assert.h>
#include "../util_grid.h"


static void draw(rtrnd_raline_t *ra)
{
	char line[71];
	long n;

	memset(line, ' ', sizeof(line));
	line[70] = '\0';
	for(n = 0; n < ra->minor.used; n+=2) {
		int from = ra->minor.array[n], to = ra->minor.array[n+1];
		assert(from < to);
		assert(from >= 0);
		assert(to <= 70);
		assert(from != to);
		memset(line+from, '+', to-from+1);
	}
	printf("%s\n", line);
}

static void dump(rtrnd_raline_t *ra)
{
	long n;
	for(n = 0; n < ra->minor.used; n+=2) {
		if (n > 0) printf(" ");
		printf("%.0f+%.0f", ra->minor.array[n], ra->minor.array[n+1]);
	}
	printf("\n");
}


void reset(rtrnd_raline_t *ra)
{
	ra->minor.used = 0;
	vtd0_append(&ra->minor, 00); vtd0_append(&ra->minor, 10);
	vtd0_append(&ra->minor, 20); vtd0_append(&ra->minor, 30);
	vtd0_append(&ra->minor, 40); vtd0_append(&ra->minor, 50);
	vtd0_append(&ra->minor, 60); vtd0_append(&ra->minor, 70);
}

int main()
{
	rtrnd_raline_t ra = {0};


	printf("\n1 removing within a negative range:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 13, 16) == 0);
	draw(&ra); dump(&ra);

	printf("\n2 removing within a positive range:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 23, 26) == 0);
	draw(&ra); dump(&ra);

	printf("\n3 removing from within pos to within next pos:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 23, 46) == 0);
	draw(&ra); dump(&ra);

	printf("\n4 removing from within pos to within far pos:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 23, 66) == 0);
	draw(&ra); dump(&ra);

	printf("\n5 removing from start of pos to within far pos:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 20, 66) == 0);
	draw(&ra); dump(&ra);

	printf("\n6 removing from within pos to end of far pos:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 23, 50) == 0);
	draw(&ra); dump(&ra);

	printf("\n7 removing from within a hole to middle of a pos:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 13, 65) == 0);
	draw(&ra); dump(&ra);

	printf("\n8 removing from within a hole to middle of a hole:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 13, 55) == 0);
	draw(&ra); dump(&ra);


	printf("\n9 removing from pos-end to middle of a hole:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 20, 55) == 0);
	draw(&ra); dump(&ra);

	printf("\n10 removing from pos-end to pos-begin:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 20, 60) == 0);
	draw(&ra); dump(&ra);

	printf("\n10 removing from pos-end to pos-end:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 20, 50) == 0);
	draw(&ra); dump(&ra);


	printf("\n10 removing everything:\n");
	reset(&ra);
	draw(&ra);
	assert(rtrnd_raline_block(&ra, 00, 70) == 0);
	draw(&ra); dump(&ra);

	return 0;
}
