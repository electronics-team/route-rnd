#include <stdio.h>
#include "../util_grid.h"

int main()
{
	double d, origin, spacing;
	int res;

	vtd0_t coords = {0};
	while(scanf("%lf\n", &d) == 1)
		vtd0_append(&coords, d);

	res = rtrnd_grid_detect(&coords, &origin, &spacing);
	printf("res=%d %f:%f\n", res, spacing, origin);

}