/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  map network segments
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef RTRND_NETSEG_H
#define RTRND_NETSEG_H

int rtrnd_netseg_map_from_obj(rtrnd_board_t *brd, rtrnd_any_obj_t *obj);
int rtrnd_netseg_map_board(rtrnd_board_t *brd);

void rtrnd_netseg_init(rtrnd_netseg_t *ns);
rtrnd_netseg_t *rtrnd_netseg_new(void);
void rtrnd_netseg_free(rtrnd_netseg_t *ns);

void rtrnd_netseg_unreg_obj(rtrnd_netseg_t *ns, rtrnd_any_obj_t *o);
void rtrnd_netseg_reg_obj(rtrnd_netseg_t *ns, rtrnd_any_obj_t *o);


/* register a net segment either in the related net's segments list or in
   the board's orphaned_segments list */
void rtrnd_netseg_reg_net(rtrnd_board_t *brd, rtrnd_netseg_t *ns);

/* Move all objects from src to dst, moving now empty src to board's orphaned
   net segs */
void rtrnd_netseg_merge(rtrnd_netseg_t *dst, rtrnd_netseg_t *src);

#endif

