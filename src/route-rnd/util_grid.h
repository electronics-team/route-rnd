#ifndef RTRND_UTIL_GRID_H
#define RTRND_UTIL_GRID_H

#include <genvector/vtd0.h>

#include "data.h"

int rtrnd_grid_detect(vtd0_t *samples, double *origin, double *spacing);
int rtrnd_grid_detect_terms(const rtrnd_board_t *brd, double *x_orig, double *x_spacing, double *y_orig, double *y_spacing);

/* range-line at a major coord; minor holds endpoints of positive ranges in
   [n*2]..[n*2+1] (ordered ascending); initially the whole line is a single
   positive range between its absolute min..max, then negative ranges are
   added by removing segments, splitting the positive range up into a series
   of smaller positive ranges */
typedef struct {
	double major;
	vtd0_t minor;
	rtrnd_udata_t rt_data;  /* optional custom data filled in by the router algorithm using the raline */
} rtrnd_raline_t;

/* remove a segment (from..to inclusive) from a raline, making it negative */
int rtrnd_raline_block(rtrnd_raline_t *ra, double from, double to);

/* A grid is an ordered (by major) list of ralines */
typedef struct {
	double spacing;
	long len;
	rtrnd_raline_t *raline;
} rtrnd_ragrid_t;

void rtrnd_ragrid_init(rtrnd_ragrid_t *grid, double min_coord, double max_coord, double origin, double spacing, double minor0, double minor1);
void rtrnd_ragrid_uninit(rtrnd_ragrid_t *grid);

/* render the grid onto a (typically annotation) layer with thin lines */
void rtrnd_ragrid_draw(rtrnd_ragrid_t *grid, rtrnd_layer_t *layer, int is_major_x);

/* Determine the size (cross section range, minor coords) of the object
   sliced by a grid line */
int rtrnd_raline_obj_mask_size_at(rtrnd_raline_t *ra, int is_major_x, double lthick, double lclr, rtrnd_any_obj_t *obj, double *minor_from, double *minor_to);

/* Mask out objects from the grid, assuming the grid is created for lthick/lclr wires */
int rtrnd_raline_mask_obj(rtrnd_raline_t *ra, int is_major_x, double lthick, double lclr, rtrnd_any_obj_t *obj);
int rtrnd_grid_mask_obj(rtrnd_ragrid_t *grid, int is_major_x, double lthick, double lclr, rtrnd_any_obj_t *obj);
void rtrnd_grid_mask_objs(rtrnd_ragrid_t *grid, int is_major_x, rtrnd_board_t *brd, rtrnd_layer_t *layer, double wire_thick, double wire_clr);

/* Find the last raline whose major coord is smaller than major */
rtrnd_raline_t *rtrnd_grid_find_major_before(rtrnd_ragrid_t *grid, double major);

/* Returns 1 if range between from..to, inclusive, is within a single positive range */
int rtrnd_raline_range_avail(rtrnd_raline_t *ra, double from, double to);

/* Return the index of the positive range start minor_pt is in
   located right before. Returns:
   - the index of the positive range (ra->minor[ret] .. ra->minor[ret+1] is the endpoints)
   - or -1 if minor_pt is in a negative range
*/
long rtrnd_raline_pt_range(rtrnd_raline_t *ra, double minor_pt);

/* Same as rtrnd_raline_pt_range() but is searching for a negative range */
long rtrnd_raline_pt_neg_range(rtrnd_raline_t *ra, double minor_pt);


/* Returns 1 if ra is part of grid */
int rtrnd_raline_in_grid(rtrnd_ragrid_t *grid, rtrnd_raline_t *ra);

/* Calculate the distance between ra and rb as ra-rb: 0 means they are
   the same, +1 means ra is the next line after rb, -1 means ra is the
   last line before rb */
long rtrnd_raline_dist(rtrnd_ragrid_t *grid, rtrnd_raline_t *ra, rtrnd_raline_t *rb);

/* Return a neighbour raline stepping delta from ra; returns NULL if stepping
   went out of range */
rtrnd_raline_t *rtrnd_raline_step(rtrnd_ragrid_t *grid, rtrnd_raline_t *ra, long delta);


#endif
