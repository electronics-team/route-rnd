#include "htdi.h"
#define HT(x) htdi_ ## x
#include <genht/ht.c>
#include <genht/hash.h>

int htdi_keyeq(htdi_key_t a, htdi_key_t b)
{
	return a == b;
}

unsigned int htdi_keyhash(htdi_const_key_t a)
{
	return jenhash(&a, sizeof(a));
}

#undef HT
