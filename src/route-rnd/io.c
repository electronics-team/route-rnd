/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  load/save/export using plugins
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <stdio.h>
#include <string.h>
#include "route-rnd.h"
#include "io.h"

#define ERROR "route-rnd error: "

int rtrnd_load(rtrnd_t *ctx, const char *ifn)
{
	int n;

	FILE *f;

	f = fopen(ifn, "r");
	if (f == NULL) {
		fprintf(stderr, ERROR "failed to open '%s' for read\n", ifn);
		return -1;
	}

	for(n = 0; n < rtrnd_all_io.used; n++) {
		rtrnd_io_t *io = rtrnd_all_io.array[n];

		if ((io == NULL) || (io->load == NULL))
			continue;

		if (io->test_parse != NULL) {
			rewind(f);
			if (io->test_parse(f) == 0)
				continue;
		}

		rewind(f);
		if (io->load(ctx, f) == 0) {
			fclose(f);
			ctx->fn = ifn;
			ctx->io = io;
			return 0;
		}
	}

	fprintf(stderr, ERROR "none of the format plugins could read '%s'\n", ifn);
	fclose(f);
	return -1;
}

int rtrnd_io_set_by_name(rtrnd_t *ctx, const char *name)
{
	int n;
	for(n = 0; n < rtrnd_all_io.used; n++) {
		rtrnd_io_t *io = rtrnd_all_io.array[n];
		if (strcmp(io->name, name) == 0) {
			ctx->io = io;
			return 0;
		}
	}
	return -1;
}


int rtrnd_export(rtrnd_t *ctx, const char *fmt, const char *basename, rtrnd_layer_t *layer, vtp0_t *annot)
{
	int n;

	for(n = 0; n < rtrnd_all_export.used; n++) {
		rtrnd_export_t *io = rtrnd_all_export.array[n];

		if ((io == NULL) || (io->export == NULL) || (strcmp(fmt, io->name) != 0))
			continue;

		return io->export(ctx, basename, layer, annot);
	}

	fprintf(stderr, ERROR "export format plugin '%s' not found\n", fmt);
	return -1;
}
