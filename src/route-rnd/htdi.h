#ifndef GENHT_HTDI_H
#define GENHT_HTDI_H

typedef double htdi_key_t;
typedef long int htdi_value_t;
#define HT(x) htdi_ ## x
#include <genht/ht.h>
#undef HT

int htdi_keyeq(htdi_key_t a, htdi_key_t b);
unsigned int htdi_keyhash(htdi_const_key_t a);


#endif
