#ifndef RTRND_GEO_H
#define RTRND_GEO_H

#include "config.h"

#define G2D_INLINE RTRND_INLINE
#include "gengeo2d/typecfg_double_double.h"
#include <gengeo2d/prim.h>

#endif
