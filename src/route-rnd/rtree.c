#include "config.h"
#include <string.h>
#include <assert.h>

#define RTRND_RTREE_KEEP_RTR
#include "rtree.h"

#include <genrtree/genrtree_impl.h>
#include <genrtree/genrtree_search.h>
#include <genrtree/genrtree_delete.h>

