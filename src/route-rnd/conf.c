/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router conf settings
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include "conf.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "compat_misc.h"

static const char *type_names[] = {
	"<terminator>", "boolean", "integer", "double", "coord", "string"
};

const char *rtrnd_conf_type2name(rtrnd_conf_type_t type)
{
	if ((type >= 0) && (type < sizeof(type_names)/sizeof(type_names[0])))
		return type_names[type];
	return "<invalid>";
}

rtrnd_conf_type_t rtrnd_conf_name2type(const char *name)
{
	rtrnd_conf_type_t i;
	const char **n;
	for(n = type_names+1, i = 1; i < sizeof(type_names)/sizeof(type_names[0]); i++,n++)
		if (strcmp(*n, name) == 0)
			return i;
	return RTRND_CT_INVALID;
}

static int rtrnd_conf_setval(rtrnd_conf_t *row, const char *val)
{
	switch(row->type) {
		case RTRND_CT_BOOLEAN:
			if ((*val == '1') || (*val == 't') || (*val == 'T') || (*val == 'y') || (*val == 'Y'))
				*row->data.i = 1;
			if ((*val == '0') || (*val == 'f') || (*val == 'F') || (*val == 'n') || (*val == 'N'))
				*row->data.i = 1;
			else
				return -1;
			return 0;
		case RTRND_CT_INTEGER:
			{
				char *end;
				long l = strtol(val, &end, 10);
				if (*end != '\0') return -1;
				if ((row->min != RTRND_CONF_NOVAL) && (l < row->min)) return -1;
				if ((row->max != RTRND_CONF_NOVAL) && (l > row->max)) return -1;
				*row->data.i = l;
				return 0;
			}
		case RTRND_CT_DOUBLE:
		case RTRND_CT_COORD:
			{
				char *end;
				double d = strtod(val, &end);
				if (*end != '\0') return -1;
				if ((row->min != RTRND_CONF_NOVAL) && (d < row->min)) return -1;
				if ((row->max != RTRND_CONF_NOVAL) && (d > row->max)) return -1;
				*row->data.d = d;
				return 0;
			}
		case RTRND_CT_STRING:
			free(row->data.s);
			row->data.s = rnd_strdup(val);
			return 0;
		case RTRND_CT_TERMINATOR:
			return -1;
	}
	return 0;
}

int rtrnd_conf_set(rtrnd_conf_t *table, const char *key, const char *val)
{
	rtrnd_conf_t *c;
	for(c = table; c->type != RTRND_CT_TERMINATOR; c++) {
		if (strcmp(c->name, key) == 0)
			return rtrnd_conf_setval(c, val);
	}
	return -1;
}


static int rtrnd_conf_setdef(rtrnd_conf_t *row)
{

	switch(row->type) {
		case RTRND_CT_BOOLEAN:
			if (row->defval.dval == RTRND_CONF_NOVAL)
				return -1;
			*row->data.i = !!row->defval.dval; break;
		case RTRND_CT_INTEGER:
			if (row->defval.dval == RTRND_CONF_NOVAL)
				return -1;
			*row->data.i = row->defval.dval; break;
		case RTRND_CT_DOUBLE:
		case RTRND_CT_COORD:
			if (row->defval.dval == RTRND_CONF_NOVAL)
				return -1;
			*row->data.d = row->defval.dval; break;
		case RTRND_CT_STRING:
			free(row->data.s);
			if (row->defval.sval != NULL)
				row->data.s = rnd_strdup(row->defval.sval);
			else
				row->data.s = NULL;
			break;
		case RTRND_CT_TERMINATOR:
			return -1;
	}
	return 0;
}

int rtrnd_conf_defaults(rtrnd_conf_t *table)
{
	rtrnd_conf_t *c;
	for(c = table; c->type != RTRND_CT_TERMINATOR; c++)
		rtrnd_conf_setdef(c);
	return 0;
}
