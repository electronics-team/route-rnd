#ifndef RTRND_COMPAT_MISC_H
#define RTRND_COMPAT_MISC_H

char *rnd_strndup(const char *s, int len);
char *rnd_strdup(const char *s);
char *rnd_strdup_safe(const char *s);
int rnd_strcasecmp(const char *s1, const char *s2);
int rnd_strncasecmp(const char *s1, const char *s2, size_t n);

#endif
