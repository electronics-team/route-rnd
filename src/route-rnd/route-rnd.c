/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  main code for the executable
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "route-rnd.h"
#include "data.h"
#include "netseg.h"
#include "buildin.h"
#include "route_res.h"
#include "io.h"

#define ERROR "route-rnd error: "

vtp0_t rtrnd_all_router, rtrnd_all_export, rtrnd_all_io;

void rtrnd_error(const char *fmt, ...)
{
	va_list ap;

	fprintf(stderr, ERROR);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}


void rtrnd_init(rtrnd_t *ctx, int verbose)
{
	memset(ctx, 0, sizeof(rtrnd_t));
	ctx->verbose = verbose;
}

void rtrnd_uninit(rtrnd_t *ctx)
{
	long n;

	for(n = 0; n < ctx->annots.used; n++) {
		rtrnd_layer_uninit(ctx->annots.array[n]);
		free(ctx->annots.array[n]);
	}

	if (ctx->board != NULL)
		rtrnd_board_free(ctx->board);
	vtp0_uninit(&rtrnd_all_router);
	vtp0_uninit(&rtrnd_all_export);
	vtp0_uninit(&rtrnd_all_io);

	vtp0_uninit(&ctx->annots);
	free((char *)ctx->name);
}

int rtrnd_find_router(rtrnd_t *ctx, const char *method)
{
	int n;

	for(n = 0; n < rtrnd_all_router.used; n++) {
		rtrnd_router_t *rt = rtrnd_all_router.array[n];
		if ((rt != NULL) && (rt->route != NULL) && (strcmp(method, rt->name) == 0)) {
			ctx->rt = rt;
			return 0;
		}
	}

	fprintf(stderr, ERROR "routing method '%s' not found\n", method);
	return -1;
}

void list_methods(void)
{
	int n;

	for(n = 0; n < rtrnd_all_router.used; n++) {
		rtrnd_router_t *rt = rtrnd_all_router.array[n];
		printf("%s\t%s\n", rt->name, rt->desc);
	}
}

static int setup_res(rtrnd_t *ctx, char *ofn_, const char *ifn)
{
	char *fr = NULL, *ofn = ofn_;

	if ((ofn == NULL) && (ifn == NULL)) {
		/* special case: no input, no output: probably called with -l, use stdout */
		ctx->resf = stdout;
		return 0;
	}

	if ((ofn != NULL) && ((strcmp(ofn, "-") == 0) || (strcmp(ofn, "/dev/stdout") == 0))) {
		ctx->resf = stdout;
		return 0;
	}


	if (ofn == NULL) {
		long len = strlen(ifn);
		char *sep;
		fr = ofn = malloc(len + 8);
		strcpy(ofn, ifn);
		sep = strrchr(ofn, '.');
		if (sep != NULL) {
			memmove(sep+4, sep, strlen(sep)+1);
			memcpy(sep, ".res", 4);
		}
	}
	ctx->resf = fopen(ofn, "w");
	if (ctx->resf == NULL) {
		fprintf(stderr, ERROR "failed to open result file '%s' for write\n", ofn);
		return 1;
	}
	free(fr);
	return 0;
}

static void help_generic(const char *prg)
{
	printf("route-rnd: external PCB autorouter\n");
	printf("Usage: %s [-v] -m method [-o out_fn] inp_fn\n", prg);
	printf("       %s -m method -l\n", prg);
	printf("       %s -M\n", prg);
	printf("\nOptions:\n");
	printf(" -M            list available methods (in TSV format)\n");
	printf(" -m method     set method (by name, as returned by -M, first column)\n");
	printf(" -l            list config options of a method (in tEDAx route res)\n");
	printf(" -v            increase verbosity (can be specified multiple times)\n");
	printf(" -o ofn        output file name (for tEDAx route result)\n");
	printf(" inp_fn        input file name (tEDAx route request)\n");
	printf("\n");
}

static void help(const char *prg, const char *topic)
{
	help_generic(prg);
}

int route_rnd_main(int argc, char *argv[])
{
	char *ifn = NULL, *ofn = NULL, *method = "horver";
	int n, verbose = 0, list_conf = 0, res = 0;
	rtrnd_t ctx;

	rtrnd_buildin_init();

	for(n = 1; n < argc; n++) {
		char *cmd = argv[n], *arg = argv[n+1];
		if (*cmd == '\0') continue; /* ignore empty args, useful for the tester */
		if (*cmd == '-') {
			while(*cmd == '-') cmd++;
			switch(*cmd) {
				case 'h': help(argv[0], arg); exit(0);
				case 'v': verbose++; break;
				case 'm': method = arg; n++; break;
				case 'l': list_conf = 1; break;
				case 'o': ofn = arg; n++; break;
				case 'M': list_methods(); return 0;
				default:
					fprintf(stderr, ERROR "unknown command '%s'\n", cmd);
					return 1;
			}
		}
		else {
			if (ifn != NULL) {
				fprintf(stderr, ERROR "multiple request file names are not supported ('%s' and '%s')\n", ifn, cmd);
				return 1;
			}
			ifn = cmd;
		}
	}

	rtrnd_init(&ctx, verbose);

	if (rtrnd_find_router(&ctx, method) != 0)
		return 1;

	if (ifn != NULL)
		rtrnd_conf_defaults(ctx.rt->conf);

	if ((ifn != NULL) && (rtrnd_load(&ctx, ifn) != 0)) {
		fprintf(stderr, ERROR "failed to load route request '%s'\n", ifn);
		rtrnd_uninit(&ctx);
		return 1;
	}

	if (setup_res(&ctx, ofn, ifn) != 0) {
		rtrnd_uninit(&ctx);
		return 1;
	}

	if (ctx.io == NULL) {
		/* special case fallback: called with -l, without input file, so io is not detected by file format */
		if (rtrnd_io_set_by_name(&ctx, "tEDAx") != 0) {
			fprintf(stderr, ERROR "failed to find the tEDAx plugin, specify an io\n");
			rtrnd_uninit(&ctx);
			return 1;
		}
	}

	if (!list_conf && (ifn == NULL)) {
		fprintf(stderr, "route-rnd: the ringdove PCB autorouter requires arguments; see --help\n");
		return 1;
	}

	rtrnd_res_init(&ctx);

	if (list_conf)
		rtrnd_res_conf_all(&ctx, ctx.rt->conf);

	if ((ifn != NULL) && (ctx.verbose > 1)) {
		rtrnd_export(&ctx, "animator", "1load", NULL, &ctx.annots);
		rtrnd_export(&ctx, "svg", "1load", NULL, &ctx.annots);
	}

	if (ifn != NULL) {
		g2d_box_t gbb;
		rtrnd_netseg_map_board(ctx.board);
		rtrnd_board_bbox(&gbb, ctx.board);
		ctx.board->hdr.bbox.x1 = gbb.p1.x; ctx.board->hdr.bbox.y1 = gbb.p1.y;
		ctx.board->hdr.bbox.x2 = gbb.p2.x; ctx.board->hdr.bbox.y2 = gbb.p2.y;

		ctx.board->hdr.bbox.x1 -= 5;
		ctx.board->hdr.bbox.y1 -= 5;
		ctx.board->hdr.bbox.x2 += 5;
		ctx.board->hdr.bbox.y2 += 5;
#warning TODO: sanity checks and error reports
	}

	if ((ifn != NULL) && (ctx.rt != NULL) && (ctx.rt->route(&ctx) != 0)) {
		fprintf(stderr, ERROR "failed to perform routing request '%s'\n", ifn);
		res = 1;
	}

	if ((ifn != NULL) && (ctx.verbose > 1)) {
		rtrnd_export(&ctx, "animator", "3routed", NULL, &ctx.annots);
		rtrnd_export(&ctx, "svg", "3routed", NULL, &ctx.annots);
	}

	rtrnd_res_uninit(&ctx);
	fclose(ctx.resf);
	rtrnd_uninit(&ctx);
	return res;
}
