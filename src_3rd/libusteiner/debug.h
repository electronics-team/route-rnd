/*  libusteiner - simple approximation to minimal 2D Euclidean Steiner Tree
    Copyright (C) 2020  Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    Source code: svn://repo.hu/libusteiner/trunk
    Author: Tibor 'Igor2' Palinkas
    Contact: http://igor2.repo.hu/contact.html
*/

#ifndef LIBUSTEINER_DEBUG_H
#define LIBUSTEINER_DEBUG_H

#include <stdio.h>
#include "libusteiner.h"

/* Loads points from a plain text file, each line a par of decimal real x y;
   comments with # are allowed. Returns 0 on success, offending line number
   on error (-1 means can't open file) */
long ustn_fload_pts(FILE *f, ustn_tree_t *tree);
long ustn_load_pts(const char *fn, ustn_tree_t *tree);


void ustn_fsave_svg(FILE *f, ustn_tree_t *tree);
void ustn_save_svg(const char *fn, ustn_tree_t *tree);

#endif
