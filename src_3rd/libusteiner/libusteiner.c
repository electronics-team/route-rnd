/*  libusteiner - simple approximation to minimal 2D Euclidean Steiner Tree
    Copyright (C) 2020  Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    Source code: svn://repo.hu/libusteiner/trunk
    Author: Tibor 'Igor2' Palinkas
    Contact: http://igor2.repo.hu/contact.html
*/

#include <math.h>
#include <libusteiner/libusteiner.h>

/* terminals apply weak force only to let steiner points group more easily; this
   is the weakening multiplier, sort of a spring constant; 1 means this
   feature us turned off */
#define WEAK 1


/* Sort of a dissipation: step a bit smaller than we should. Decreases
   precision but helps convergence. Turned off because it messes up
   precision in trivial cases*/
/*#define DISSIPATE 0.97*/

/* Alternative: simple multiplier */
#define STEPPING 0.02

/* disjoint sets */

USTN_INLINE ustn_node_t *ustn_disjset_root(ustn_node_t *n)
{
	while(n->parent != NULL)
		n = n->parent;
	return n;
}

USTN_INLINE void ustn_disjset_union(ustn_node_t *x, ustn_node_t *y)
{
	ustn_node_t *xroot = ustn_disjset_root(x), *yroot = ustn_disjset_root(y);
	if (xroot->rank == yroot->rank) {
		yroot->parent = xroot;
		xroot->rank++;
	}
	else if (xroot->rank < yroot->rank)
		xroot->parent = yroot;
	else /* if (xroot->rank > yroot->rank) */
		yroot->parent = xroot;
}

USTN_INLINE int ustn_disjset_is_sameset(ustn_node_t *x, ustn_node_t *y)
{
	return ustn_disjset_root(x) == ustn_disjset_root(y);
}

/*** allocation ***/
USTN_INLINE ustn_edge_t *ustn_edge_new(ustn_tree_t *mst, ustn_node_t *p1, ustn_node_t *p2)
{
	ustn_edge_t *e;
	if (mst->free_edges.used > 0) {
		e = mst->free_edges.array[mst->free_edges.used-1];
		mst->free_edges.used--;
	}
	else
		e = malloc(sizeof(ustn_edge_t));

	memset(e, 0, sizeof(ustn_edge_t));
	e->p1 = p1; e->p2 = p2;
	return e;
}

ustn_edge_t *ustn_add_edge(ustn_tree_t *mst, ustn_node_t *p1, ustn_node_t *p2)
{
	ustn_edge_t *e = ustn_edge_new(mst, p1, p2);

	vtp0_append(&p1->edges, e);
	vtp0_append(&p2->edges, e);
	vtp0_append(&mst->edges, e);

	return e;
}

USTN_INLINE void ustn_edge_free(ustn_tree_t *mst, ustn_edge_t *e)
{
	vtp0_append(&mst->free_edges, e);
}


/*** Kruskal ***/

#define USTN_EDGES_MAX 10
typedef struct ustn_usable_edges_s {
	vtp0_t edges;
	ustn_node_t *node;
} ustn_usable_edges_t;

USTN_INLINE double ustn_edge_len2(const ustn_edge_t *src)
{
	double dx = src->p1->x - src->p2->x, dy = src->p1->y - src->p2->y;
	return dx*dx + dy*dy;
}

USTN_INLINE double ustn_edge_len2f(const ustn_edge_t *src)
{
	double l, dx = src->p1->x - src->p2->x, dy = src->p1->y - src->p2->y;
	l = dx*dx + dy*dy;
	if (src->fixed)
		l = l/100.0;
	return l;
}

USTN_INLINE int ustn_double_compare(double a, double b)
{
	if (a < b) return -1;
	if (a > b) return +1;
	return 0;
}

USTN_INLINE int ustn_edge_cmp(const void *A, const void *B)
{
	const ustn_edge_t *a = *(const ustn_edge_t **)A, *b = *(const ustn_edge_t **)B;
	return ustn_double_compare(ustn_edge_len2f(a), ustn_edge_len2f(b));
}

USTN_INLINE void ustn_edge_sort(vtp0_t *edges)
{
	qsort(edges->array, edges->used, sizeof(void *), ustn_edge_cmp);
}

USTN_INLINE int ustn_usable_edges_bottleneck(ustn_usable_edges_t *edges)
{
	int n, res = -1;
	double curr, best = -1;

	for(n = 0; n < edges->edges.used; n++) {
		ustn_edge_t *e = edges->edges.array[n];
		curr = ustn_edge_len2f(e);
		if (curr > best) {
			best = curr;
			res = n;
		}
	}
	return res;
}

USTN_INLINE void set_fixed(vtp0_t *fixed, ustn_edge_t *e)
{
	long n;
	/* apply fixed edges */
	for(n = 0; n < fixed->used; n++) {
		ustn_edge_t *fe = fixed->array[n];
		if (((e->p1 == fe->p1) || (e->p1 == fe->p2)) && ((e->p2 == fe->p1) || (e->p2 == fe->p2))) {
			e->fixed = 1;
			e->user_long = fe->user_long;
			e->user_ptr = fe->user_ptr;
		}
	}
}

USTN_INLINE void ustn_usable_edges_add_node(ustn_tree_t *mst, ustn_usable_edges_t *edges, ustn_node_t *node, vtp0_t *fixed)
{
	ustn_edge_t *e = ustn_edge_new(mst, edges->node, node);
	set_fixed(fixed, e);
	if (edges->edges.used >= USTN_EDGES_MAX) {
		int botidx = ustn_usable_edges_bottleneck(edges);
		ustn_edge_t *botneck = edges->edges.array[botidx];
		if(ustn_edge_len2f(e) < ustn_edge_len2f(botneck)) {
			vtp0_remove(&edges->edges, botidx, 1);
			vtp0_append(&edges->edges, e);
		}
	}
	else
		vtp0_append(&edges->edges, e);
}

USTN_INLINE void ustn_kruskal_all_beta(ustn_tree_t *mst, vtp0_t *dst_edges, vtp0_t *src_nodes, vtp0_t *fixed)
{
	long n, m;

	for(n = 0; n < src_nodes->used; n++) {
		ustn_node_t *node = src_nodes->array[n];
		ustn_usable_edges_t lst = {0};

		lst.node = node;
		for(m = 0; m < src_nodes->used; m++)
			ustn_usable_edges_add_node(mst, &lst, src_nodes->array[m], fixed);

		for(m = 0; m < lst.edges.used; m++)
			vtp0_append(dst_edges, lst.edges.array[m]);
	}
	ustn_edge_sort(dst_edges);
}


USTN_INLINE int ustn_kruskal_noloops(ustn_edge_t *edge) { return !ustn_disjset_is_sameset(edge->p1, edge->p2); }
USTN_INLINE void ustn_kruskal_union(ustn_edge_t *edge)  { ustn_disjset_union(edge->p1, edge->p2); }

USTN_INLINE void ustn_kruskal_construct(ustn_tree_t *mst)
{
	vtp0_t edges = {0}, fixed = {0};
	long n;

	/* save fixed edges */
	for(n = 0; n < mst->edges.used; n++) {
		ustn_edge_t *e = mst->edges.array[n];

		if (e->fixed)
			vtp0_append(&fixed, e);
	}

	mst->edges.used = 0;
	ustn_kruskal_all_beta(mst, &edges, &mst->nodes, &fixed);
	vtp0_uninit(&fixed);

	/* reset per node edges */
	for(n = 0; n < mst->nodes.used; n++) {
		ustn_node_t *nd = mst->nodes.array[n];
		nd->edges.used = 0;
	}


	for(n = 0; n < edges.used; n++) {
		ustn_edge_t *e = edges.array[n];

		if (ustn_kruskal_noloops(e)) {
			ustn_kruskal_union(e);
			vtp0_append(&mst->edges, e);
			vtp0_append(&e->p1->edges, e);
			vtp0_append(&e->p2->edges, e);
			if (mst->edges.used >= mst->nodes.used - 1)
				break;
		}
		else
			ustn_edge_free(mst, e);
	}

	vtp0_uninit(&edges);

	/* reset disjset */
	for(n = 0; n < mst->nodes.used; n++) {
		ustn_node_t *node = mst->nodes.array[n];
		node->parent = NULL;
		node->rank = 0;
	}
}

/*** solver: spring force iterator ***/
#define components(OLEN2, OLEN, ODX, ODY,   IDX, IDY) \
do { \
	double TLEN, TLEN2; \
	OLEN2 = TLEN2 = ((IDX) * (IDX)) + ((IDY) * (IDY)); \
	if (OLEN2 != 0) { \
		OLEN = TLEN = sqrt(TLEN2); \
		ODX = (IDX) / TLEN; \
		ODY = (IDY) / TLEN; \
	} \
	else { \
		OLEN = 0; \
		ODX = ODY = 0; \
	} \
} while(0)

USTN_INLINE void replace_edge_nd(ustn_edge_t *e, ustn_node_t *oldnd, ustn_node_t *newnd)
{
	vtp0_append(&newnd->edges, e);
	if (oldnd == newnd) return;
	if (e->p1 == oldnd) e->p1 = newnd;
	else e->p2 = newnd;
}

static int comp_ang(const void *A, const void *B)
{
	const ustn_edge_t **a = (const ustn_edge_t **)A, **b = (const ustn_edge_t **)B;
	return ((*a)->ang < (*b)->ang) ? -1 : +1;
}

void ustn_solve_iterate_pre(ustn_tree_t *mst)
{
	long n, m, end;
	vtp0_t sedges = {0}; /* sorted edges by angle */

	if (mst->pt_too_close2 == 0)
		mst->pt_too_close2 = 0.2*0.2;

	ustn_kruskal_construct(mst);

	end = mst->nodes.used; /* sanpshot so we are not re-iterating on newly added nodes */
	for(n = 0; n < end; n++) {
		ustn_node_t *nd = mst->nodes.array[n];
		int nd_used;

		if (nd->edges.used < 3) continue; /* ignore nodes with only 1 or 2 incoming nodes */

		sedges.used = 0; /* reset the ring temp storage, starting a new ring */
		nd_used = 0;

		/* move all edges to sedges for the edge-angle sorting, leaving nd empty */
		for(m = nd->edges.used-1; m >= 0; m--) {
			ustn_edge_t *e = nd->edges.array[m];
			ustn_node_t *here = (e->p1 == nd) ? e->p1 : e->p2;
			ustn_node_t *there = (e->p1 == nd) ? e->p2 : e->p1;

			vtp0_append(&sedges, e);
			e->ang = atan2(there->y - here->y, there->x - here->x);
			nd->edges.used--;
		}

/*printf("se1: %ld\n", sedges.used);*/

		qsort(sedges.array, sedges.used, sizeof(void *), comp_ang);
		while(sedges.used > 1) {
			long prev = sedges.used-1, besti1, besti2;
			double ang, best = 100;

			/* pick the smallest angle */
			for(m = 0; m < sedges.used; m++) {
				ustn_edge_t *e1 = sedges.array[prev], *e2 = sedges.array[m];
				ang = fmod(fabs(e1->ang - e2->ang), M_PI);
				if (ang < best) {
					best = ang;
					besti1 = prev;
					besti2 = m;
				}
				prev = m;
			}

			/* introduce a new steiner point at that angle */
			{
				ustn_edge_t *e1 = sedges.array[besti1], *e2 = sedges.array[besti2];
				double ndang = e1->ang;
				ustn_node_t *newnd;
/*				printf("  best angle: %f %f^%f nd: %f\n", best, e1->ang, e2->ang, ndang);*/

				if (!nd_used) { /* reuse the original steiner point */
					newnd = nd;
					nd_used = 1;
				}
				else
					newnd = ustn_add_node(mst, nd->ix, nd->iy); /* create a new steiner point */

				newnd->ix = nd->ix; newnd->iy = nd->iy;
				newnd->x = nd->ix + cos(ndang)*0.4; newnd->y = nd->iy + sin(ndang)*0.4;
				replace_edge_nd(e1, nd, newnd);
				replace_edge_nd(e2, nd, newnd);


				if (besti1 > besti2) {
					vtp0_remove(&sedges, besti1, 1);
					vtp0_remove(&sedges, besti2, 1);
				}
				else {
					vtp0_remove(&sedges, besti2, 1);
					vtp0_remove(&sedges, besti1, 1);
				}
			}
		}

		/* if there was an odd number of edges, the last one needs a steiner point too */
		if (sedges.used == 1) {
			ustn_edge_t *e = sedges.array[0];
			ustn_node_t *newnd = ustn_add_node(mst, nd->ix, nd->iy);
			replace_edge_nd(e, nd, newnd);
		}

/*printf("se2: %ld\n", sedges.used);*/

	}

	vtp0_uninit(&sedges);
}

/* calculate and cache edge lengths; return the total length of steiner edges */
USTN_INLINE double ustn_update_lengths(ustn_tree_t *mst)
{
	double totlen = 0;
	long n;

	for(n = 0; n < mst->edges.used; n++) {
		ustn_edge_t *e = mst->edges.array[n];
		components(e->len2, e->len, e->dx, e->dy,     e->p2->x - e->p1->x, e->p2->y - e->p1->y);
		totlen += e->len;
/*printf(" edge: %.3f %.3f;%.3f\n", e->len, e->dx, e->dy);*/
	}
	return totlen;
}

double ustn_tree_length(ustn_tree_t *mst)
{
	long n;
	double totlen = ustn_update_lengths(mst);

	for(n = 0; n < mst->nodes.used; n++) {
		double fx, fy, len2, len;
		ustn_node_t *nd = mst->nodes.array[n];
		if (nd->cloned) continue;
		components(len2, len, fx, fy,     nd->ix - nd->x, nd->iy - nd->y);
		(void)fx;(void)fy;(void)len2;
		totlen += len;
	}
	return totlen;
}


int ustn_solve_iterate(ustn_tree_t *mst)
{
	long n, m;
	double res = 0;

	ustn_update_lengths(mst);

	/* apply force on each node, calculating its new position, not updating current position yet */
	for(n = 0; n < mst->nodes.used; n++) {
		double fx, fy, len2, len, stepping;
		ustn_node_t *nd = mst->nodes.array[n];

		/* initial force: weak force to the shadowed terminal */
		components(len2, len, fx, fy,     nd->ix - nd->x, nd->iy - nd->y);
		(void)len;
		fx *= WEAK; fy *= WEAK;

/*printf("node for term %.3f;%.3f at %.3f;%.3f\n", nd->ix, nd->iy, nd->x, nd->y);
printf(" force . %.3f;%.3f\n", fx, fy);*/
		for(m = 0; m < nd->edges.used; m++) {
			ustn_edge_t *e = nd->edges.array[m];

			if (e->p1 == nd) {
				fx += e->dx;
				fy += e->dy;
/*printf(" force + %.3f;%.3f\n", e->dx, e->dy);*/
			}
			else {
				fx -= e->dx;
				fy -= e->dy;
/*printf(" force - %.3f;%.3f\n", e->dx, e->dy);*/
			}
		}

#ifdef DISSIPATE
		stepping = nd->stepping * DISSIPATE;
#else
		stepping = STEPPING;
#endif
		nd->nx = nd->x + fx * stepping;
		nd->ny = nd->y + fy * stepping;
		res += sqrt(fx * fx + fy * fy);

/*printf(" new: %.3f;%.3f\n", nd->nx, nd->ny);*/
	}


	/* apply new positions */
	for(n = 0; n < mst->nodes.used; n++) {
		ustn_node_t *nd = mst->nodes.array[n];
#ifdef DISSIPATE
		nd->stepping = sqrt((nd->nx - nd->x) * (nd->nx - nd->x) + (nd->ny - nd->y) * (nd->ny - nd->y));
#endif
		if (nd->fixed) continue;
		nd->x = nd->nx;
		nd->y = nd->ny;
	}

	/* decide if more iterations are needed */
	{
		mst->totlen = ustn_tree_length(mst);
		/* low pass filter: average net length */
		if (mst->itc++ == 0)
			mst->avg = mst->totlen;
		else
			mst->avg += 0.02*(mst->totlen - mst->avg);

		/* (avg - totlen) is whether the current solution is better (shorter)
		   than the low pass filtered average. Normally the optimization starts
		   with going up to positive, then slowly falling back to 0, but the
		   simulation is normally osciallating. So once we are past the
		   initial few iterations and then drop back to 0 first, we accept that
		   as the final solution - it could be slightly better but it's really
		   just oscillating around the best solution */
		if ((mst->itc > 10) && (mst->avg - mst->totlen) < 0.01) {
			if (mst->hit++ > 10)
				return 0;
		}
		mst->last = mst->totlen;
	}
	return 1;
}

void ustn_optimize(ustn_tree_t *mst)
{
	long n;

	/* if a node is too close to its input point, place it in the input point */
	for(n = 0; n < mst->nodes.used; n++) {
		ustn_node_t *nd = mst->nodes.array[n];
		double dx = nd->ix - nd->x, dy = nd->iy - nd->y;
		if (dx*dx + dy*dy < mst->pt_too_close2) {
			nd->x = nd->ix;
			nd->y = nd->iy;
			nd->cloned = 1;
		}
	}

	ustn_update_lengths(mst);
	for(n = 0; n < mst->edges.used; n++) {
		ustn_edge_t *e = mst->edges.array[n];
		if (e->len2 < mst->pt_too_close2) {
			/* TODO: merge the two nodes, but don't move input node */
		}
	}


	/* if a node is not in its input point, place an extra node and an edge */
	for(n = 0; n < mst->nodes.used; n++) {
		ustn_node_t *nd = mst->nodes.array[n];
		if ((nd->x != nd->ix) || (nd->y != nd->iy)) {
			ustn_node_t *newnd = ustn_add_node(mst, nd->ix, nd->iy);
			ustn_edge_t *e = ustn_edge_new(mst, nd, newnd);
			vtp0_append(&nd->edges, e);
			vtp0_append(&newnd->edges, e);
			vtp0_append(&mst->edges, e);
			nd->cloned = 1;
		}
	}

	for(n = 0; n < mst->nodes.used; n++) {
		ustn_node_t *nd = mst->nodes.array[n];
		if (nd->edges.used != 2) continue;
		/* TODO: check for 180 deg edges */
	}
}

/*** utility ***/
ustn_node_t *ustn_find_node(ustn_tree_t *tree, ustn_coord_t x, ustn_coord_t y, int require_fixed)
{
	long n;
	for(n = 0; n < tree->nodes.used; n++) {
		ustn_node_t *nd = tree->nodes.array[n];
		if ((nd->x == x) && (nd->y == y)) {
			if (require_fixed)
				return nd;
			if ((require_fixed < 0) && !nd->fixed)
				return nd;
			if ((require_fixed > 0) && nd->fixed)
				return nd;
		}
	}
	return NULL;
}
