/*  libusteiner - simple approximation to minimal 2D Euclidean Steiner Tree
    Copyright (C) 2020  Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    Source code: svn://repo.hu/libusteiner/trunk
    Author: Tibor 'Igor2' Palinkas
    Contact: http://igor2.repo.hu/contact.html
*/

#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <libusteiner/debug.h>

long ustn_fload_pts(FILE *f, ustn_tree_t *tree)
{
	double mult = 1;
	char *s, line[1024];
	long lineno = 0, datano = 0;

	while((s = fgets(line, sizeof(line), f)) != NULL) {
		int cols;
		double x, y, x1, y1, x2, y2;

		lineno++;
		while(isspace(*s)) s++;
		if ((*s == '#') || (*s == '\0'))
			continue;

		if (strncmp(s, "edge", 4) == 0) {
			ustn_node_t *n1, *n2;
			ustn_edge_t *e;

			cols = sscanf(s+5, "%lf %lf %lf %lf", &x1, &y1, &x2, &y2);
			if (cols != 4)
				return lineno;

			/* create two new fixed points to keep original points flexible */
			n1 = ustn_add_node(tree, x1*mult, y1*mult);
			n2 = ustn_add_node(tree, x2*mult, y2*mult);

			e = ustn_add_edge(tree, n1, n2);
			n1->fixed = 1;
			n2->fixed = 1;
			e->fixed = 1;

			continue;
		}

		datano++;
		cols = sscanf(s, "%lf %lf", &x, &y);
		if ((cols == 1) && (datano == 1)) {
			mult = 10;
			continue; /* ignore first data line if it's a sigle number: number of lines in stp */
		}
		if (cols != 2)
			return lineno;
		ustn_add_node(tree, x*mult, y*mult);
	}
	
	return 0;
}

long ustn_load_pts(const char *fn, ustn_tree_t *tree)
{
	long r;
	FILE *f = fopen(fn, "r");
	if (f == NULL) return -1;
	r = ustn_fload_pts(f, tree);
	fclose(f);
	return r;
}

void ustn_fsave_svg(FILE *f, ustn_tree_t *tree)
{
	double bx1 = HUGE_VAL, bx2 = -HUGE_VAL, by1 = HUGE_VAL, by2 = -HUGE_VAL, dx, dy;
	long n;

	for(n = 0; n < tree->nodes.used; n++) {
		ustn_node_t *node = tree->nodes.array[n];
		if (node->ix < bx1) bx1 = node->ix;
		if (node->iy < by1) by1 = node->iy;
		if (node->ix > bx2) bx2 = node->ix;
		if (node->iy > by2) by2 = node->iy;
	}

	dx = bx2 - bx1; dy = by2 - by1;
	bx1 -= dx / 10.0;
	bx2 += dx / 10.0;
	by1 -= dy / 10.0;
	by2 += dy / 10.0;

	fprintf(f, "<?xml version=\"1.0\"?>\n");
	fprintf(f, "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.0\" viewBox=\"%f %f %f %f\">\n", bx1, by1, bx2, by2);

	/* print nodes */

	fprintf(f, "<g id='points' opacity='0.8'>\n");
	for(n = 0; n < tree->nodes.used; n++) {
		ustn_node_t *node = tree->nodes.array[n];
		double r = 0.1 * (1+node->fixed);
		fprintf(f, "	<circle cx='%f' cy='%f' r='%f' stroke-width='0.025' stroke='#000000' fill='none'/>\n", node->x, node->y, r);
		fprintf(f, "	<circle cx='%f' cy='%f' r='%f' stroke-width='0' fill='#FF0000'/>\n", node->ix, node->iy, r);
	}
	fprintf(f, "</g>\n");

	fprintf(f, "<g id='edges' opacity='0.8'>\n");
	/* steiner -> input point stubs */
	for(n = 0; n < tree->nodes.used; n++) {
		ustn_node_t *node = tree->nodes.array[n];
		if ((node->ix != node->x) || (node->iy != node->y)) {
			fprintf(f, "	<line x1='%f' y1='%f' x2='%f' y2='%f' stroke-width='%f' stroke='#777777' stroke-linecap='round'/>\n",
				node->ix, node->iy, node->x, node->y, 0.1 * (1+node->fixed));
		}
	}
	/* steiner -> steiner edges (first fixed, then unfixed) */
	for(n = 0; n < tree->edges.used; n++) {
		ustn_edge_t *edge = tree->edges.array[n];
		if (!edge->fixed) continue;
		if ((edge->p1->x != edge->p2->x) || (edge->p1->y != edge->p2->y)) {
			fprintf(f, "	<line x1='%f' y1='%f' x2='%f' y2='%f' stroke-width='0.5' stroke='#33FF33' stroke-linecap='round'/>\n",
				edge->p1->x, edge->p1->y, edge->p2->x, edge->p2->y);
		}
	}
	for(n = 0; n < tree->edges.used; n++) {
		ustn_edge_t *edge = tree->edges.array[n];
		if (edge->fixed) continue;
		if ((edge->p1->x != edge->p2->x) || (edge->p1->y != edge->p2->y)) {
			fprintf(f, "	<line x1='%f' y1='%f' x2='%f' y2='%f' stroke-width='0.1' stroke='#000000' stroke-linecap='round'/>\n",
				edge->p1->x, edge->p1->y, edge->p2->x, edge->p2->y);
		}
	}


	fprintf(f, "</g>\n");

	fprintf(f, "</svg>\n");
}

void ustn_save_svg(const char *fn, ustn_tree_t *tree)
{
	FILE *f = fopen(fn, "w");
	if (f == NULL) return;
	ustn_fsave_svg(f, tree);
	fclose(f);
}
