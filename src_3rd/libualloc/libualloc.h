/*

libualloc - microlib for memory allocation with various strategies

Copyright (c) 2020 Tibor 'Igor2' Palinkas
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the Author nor the names of contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Source code: svn://svn.repo.hu/libualloc/trunk
Contact the author: http://igor2.repo.hu/contact.html

*/

#ifndef LIBUALLOC_LIBUALLOC_H
#define LIBUALLOC_LIBUALLOC_H

/* Changes only when the API changes */
#define UALL_APIVER_MAJOR 1
#define UALL_APIVER_MINOR 0
#define UALL_APIVER_PATCH 0
#define UALL_APIVER \
	((UALL_APIVER_MAJOR*100L) + (UALL_APIVER_MINOR*100L) + (UALL_APIVER_PATCH*100L))

#ifndef UALL_INLINE
#	define UALL_INLINE
#endif

#ifndef UALL_ALIGNMENT
#	define UALL_ALIGNMENT 8
#endif

#define UALL_ALIGNMASK (UALL_ALIGNMENT-1)
#define UALL_ALIGN(size)  \
	((((size) & UALL_ALIGNMASK) == 0) ? (size) : ((size) | UALL_ALIGNMASK)+1)

typedef struct uall_sysalloc_s uall_sysalloc_t;

struct uall_sysalloc_s {
	long page_size;
	void *(*alloc)(uall_sysalloc_t *ctx, long size);
	void (*free)(uall_sysalloc_t *ctx, void *block);

	/* initialize to all-zero */
	void *user_data;
	long user_cnt;
};

#ifdef LIBUALLOC_OVERRIDE
	extern int libualloc_override;
#	define UALL_OVERRIDE() libualloc_override
#else
#	define UALL_OVERRIDE() 0
#endif

/*** backends: low level sys allocators ***/

/* Use libc's malloc()/free() */
UALL_INLINE void *uall_stdlib_alloc(uall_sysalloc_t *ctx, long size);
UALL_INLINE void uall_stdlib_free(uall_sysalloc_t *ctx, void *block);

/* Return ctx->used_data (if size <= ctx->page_size) for the first
   allocation, subsequent allocations fail */
UALL_INLINE void *uall_static_alloc(uall_sysalloc_t *ctx, long size);
UALL_INLINE void uall_static_free(uall_sysalloc_t *ctx, void *block);

/* Return ctx->user_data (if size <= ctx->page_size) for the first
   allocation, subsequent allocations are done using malloc()/free() */
UALL_INLINE void *uall_static_stdlib_alloc(uall_sysalloc_t *ctx, long size);
UALL_INLINE void uall_static_stdlib_free(uall_sysalloc_t *ctx, void *block);


#endif
