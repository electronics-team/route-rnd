/*

libualloc - microlib for memory allocation with various strategies

Copyright (c) 2020 Tibor 'Igor2' Palinkas
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the Author nor the names of contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Source code: svn://svn.repo.hu/libualloc/trunk
Contact the author: http://igor2.repo.hu/contact.html

*/

#include <stdlib.h>
#include "libualloc.h"

#ifdef LIBUALLOC_OVERRIDE
int libualloc_override = 0;
#endif

UALL_INLINE void *uall_stdlib_alloc(uall_sysalloc_t *ctx, long size)
{
	return malloc(size);
}

UALL_INLINE void uall_stdlib_free(uall_sysalloc_t *ctx, void *block)
{
	free(block);
}

UALL_INLINE void *uall_static_alloc(uall_sysalloc_t *ctx, long size)
{
	if (ctx->user_cnt != 0) return NULL; /* already allocated */
	if (size > ctx->page_size) return NULL; /* not enough room */

	ctx->user_cnt = 1;
	return ctx->user_data;
}

UALL_INLINE void uall_static_free(uall_sysalloc_t *ctx, void *block)
{
	if (block == ctx->user_data)
		ctx->user_cnt = 0;
}


UALL_INLINE void *uall_static_stdlib_alloc(uall_sysalloc_t *ctx, long size)
{
	if ((ctx->user_cnt != 0) || (size > ctx->page_size))
		return malloc(size); /* static memory already used or too small*/

	ctx->user_cnt = 1;
	return ctx->user_data;
}

UALL_INLINE void uall_static_stdlib_free(uall_sysalloc_t *ctx, void *block)
{
	if (block == ctx->user_data)
		ctx->user_cnt = 0;
	else
		free(block);
}

