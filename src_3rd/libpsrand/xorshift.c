/* This file is put into public domain.
   Implementation made by Aron Barath in 2020 based on George Marsaglia's
   "xor128" algorithm (http://www.jstatsoft.org/v08/i14/paper).

   Adapted to libpsrand by Tibor 'Igor2' Palinkas in 2020.

   Project page: http://www.repo.hu/projects/libpsrand
   Project VCS: svn://svn.repo.hu/libpsrand/trunk
   Contact: http://igor2.repo.hu/contact.html
*/

#include "xorshift.h"

void psr_xorshift_init(psr_xorshift_t *rnd, psr_uint32_t seed)
{
	rnd->x = seed;
	rnd->y = seed >> 13;
	rnd->z = seed << 7;
	rnd->w = seed ^ 0xa7a7a7a7;
}

psr_uint32_t psr_xorshift_rand(psr_xorshift_t *rnd)
{
	psr_uint32_t t = rnd->x ^ (rnd->x << 11);
	rnd->x = rnd->y; rnd->y = rnd->z; rnd->z = rnd->w;
	return rnd->w = rnd->w ^ (rnd->w >> 19) ^ t ^ (t >> 8);
}
