/* libsprand - simple/portable pseudorandom generators.
   Public domain.

   Project page: http://www.repo.hu/projects/libpsrand
   Project VCS: svn://svn.repo.hu/libpsrand/trunk
   Contact: http://igor2.repo.hu/contact.html
*/

#ifndef PSR_MTW_H
#define PSR_MTW_H

#include "libpsrand/psr.h"

#define PSR_MTW_MAX (0xffffffffUL)

#define PSR_MTW_STVECT_LENGTH 624
#define PSR_MTW_STVECT_M      397 /* changes to PSR_MTW_STVECT_LENGTH also require changes to this */

typedef struct psr_mtw_s {
	psr_uint32_t mt[PSR_MTW_STVECT_LENGTH];
	int index;
} psr_mtw_t;

void psr_mtw_init(psr_mtw_t *state, psr_uint32_t seed);
psr_uint32_t psr_mtw_rand(psr_mtw_t *state);

#define psr_mtw_rand01(state) \
	((double)psr_mtw_rand(state) / (double)PSR_MTW_MAX)

#endif /* #ifndef PSR_MTW_H */
