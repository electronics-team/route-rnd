/* MISTD (16807 Lehmer RNG)
   Tibor 'Igor2' Palinkas in 2020.
   Public Domain.

   Idea and magic numbers from:
   https://en.wikipedia.org/wiki/Lehmer_random_number_generator
   https://titanwolf.org/Network/Articles/Article?AID=e141529f-5c98-457c-8207-31e6f5c51167#gsc.tab=0

   Project page: http://www.repo.hu/projects/libpsrand
   Project VCS: svn://svn.repo.hu/libpsrand/trunk
   Contact: http://igor2.repo.hu/contact.html
*/

#include "minstd.h"

void psr_minstd_init(psr_minstd_t *state, psr_uint32_t seed)
{
	*state = seed;
}

#define root 16807
psr_uint32_t psr_minstd_rand(psr_minstd_t *state)
{
	psr_uint32_t q = PSR_MINSTD_MAX / root, r = PSR_MINSTD_MAX % root;
	psr_int32_t tmp = root * ((*state) % q) - r * (psr_int32_t)((*state) / q);
	if (tmp < 0)
		tmp += PSR_MINSTD_MAX;
	*state = tmp;
	return tmp;
}
