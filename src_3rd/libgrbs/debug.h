#ifndef LIBGRBS_DEBUG_H
#define LIBGRBS_DEBUG_H

#include <stdio.h>
#include <libgrbs/grbs.h>

extern double grbs_draw_zoom;

void grbs_draw_begin(grbs_t *grbs, FILE *f);
void grbs_draw_end(grbs_t *grbs, FILE *f);

/* low level draw; fill has no stroke, wf is wireframe contour, cl is centerline 
   sr is "stroke radius" (half of trace width) */
void grbs_svg_fill_circle(FILE *f, double x, double y, double sr, const char *color);
void grbs_svg_wf_circle(FILE *f, double x, double y, double sr, const char *color);
void grbs_svg_fill_line(FILE *f, double x1, double y1, double x2, double y2, double sr, const char *color);
void grbs_svg_wf_line(FILE *f, double x1, double y1, double x2, double y2, double sr, const char *color);
void grbs_svg_fill_arc(FILE *f, double cx, double cy, double r, double sa, double da, double sr, const char *color);
void grbs_svg_wf_arc(FILE *f, double cx, double cy, double r, double sa, double da, double sr, const char *color);

/* high level draw: draw all objects of a kind */
void grbs_draw_points(grbs_t *grbs, FILE *f);
void grbs_dump_points(grbs_t *grbs, FILE *f);
void grbs_draw_wires(grbs_t *grbs, FILE *f);
void grbs_dump_wires(grbs_t *grbs, FILE *f);
void grbs_draw_2net(grbs_t *grbs, FILE *f, grbs_2net_t *tn);
void grbs_dump_2net(grbs_t *grbs, FILE *f, grbs_2net_t *tn);

/* for manual debugging of single objects */
void grbs_dump_point(grbs_point_t *p, FILE *f);


/* Return the number of unused sentinels left in a grbs; a non-zero
   value is usually an indication of a bug */
long grbs_count_unused_sentinel(grbs_t *grbs);
long grbs_count_new(grbs_t *grbs);

/* assert() if point's convex arcs are not within their sentinels */
void grbs_sentinel_check(grbs_point_t *pt);
void grbs_sentinel_check_all(grbs_t *grbs);

/* assert() if line's arc linking is broken */
void grbs_line_check(grbs_line_t *l);
void grbs_line_check_all(grbs_t *grbs);

/* assert() if arc is invalid (e.g. invalid convex) */
void grbs_arc_check(grbs_t *grbs, grbs_arc_t *arc);
void grbs_arc_check_all(grbs_t *grbs);


#endif
